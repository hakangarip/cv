<?php
session_start();
header('Content-Type: text/html; charset=utf-8');
// date_default_timezone_set('Asia/Istanbul');

ini_set('memory_limit', '100M');
// session_cache_limiter('public');
// ini_set('error_reporting', 1);
// error_reporting(E_ALL);
// ini_set('display_errors', 1);

//ini_set('error_reporting', 0);
 error_reporting(E_ALL);
ini_set('display_errors', 1);
// error_log ( 'error_reporting(-1)' , 3  ,  'test.log' );

define('SLASH'						, '/'										); // Site Genel SLASH
define('SITE_PATH'  				, 'http://newf/'							); // Site Url Path
define('SITE_FOLDER'   			    , '/newf/'		                            ); // Document Folder Örnek olarak xampta htdocsun altında bir klasörde çalışıyor iseniz
define('DR'   						, $_SERVER["DOCUMENT_ROOT"].SITE_FOLDER		); // Document Root
define('SITE_IP'     				, '127.0.0.1'			    			); // Site ip adresi, bazý upload dosyalarýnda sadece bu ipden ulaþýlmasýna izin veriliyor.
define('PRIVATE_FOLDER'   		    ,'Private'									); // Sistemdeki arka planda kullanýcý planýnda olmayan kodlar
define('PUBLIC_FOLDER'   			,'Public'  									); // Sistemin önünde, son kullanýcýya eriþim saðlanýr.
define('DEFAULT_LANGUAGE'  			, 'tr_TR'									); // sistem ilk açýldýðýndaki default language code
define('UPLOAD_FOLDER' 				, 'Upload'									); // Upload Klasörü
define('CKE' 				        , 'CKE'										); // Yonetim panelinde kullanýlan editörün klasör adý
define('MEMCACHE_PREFIX'			, 'belgecomtr'								); // mmecahceteki prefix.
define('Now'						, date('Y-m-d H:i:s')						); // tarih zaman 
define('NowDate'					, date('Y-m-d')								); // tarih
define('ASSETS'					 	, 'Assets'									); // javascript, css sitede kullanýlan jpg dizini
define('TEMPLATE_FOLDER_NAME'		, 'Template'								); // 
define('TEMPLATE_SUB_CLASS_NAME'	, 'Template'								); // 
define('SESS_ADMIN' 				, 'Administrator'							); // Session yonetim paneli ön eki
define('ASJ_FOLDER'					, DR.SLASH.PRIVATE_FOLDER.SLASH.'Settings/Asj/'			);
define('YONETIM_PATH'  				, SITE_PATH.PRIVATE_FOLDER.SLASH.'Yonetim/'	);
define('ADMIN_PANEL_DEFAULT_LANGUAGE', 'tr_TR'									); // Yonetim paneli default dili. 
define('MEMCACHE'                    , '0'									); 

// define('CacheDriver'				, 'Memcache'								); // cache drive 

include DR.SLASH.PRIVATE_FOLDER.SLASH.'Settings/global_settings.php';

/**
 * Mysql Db Config 
 */

define('ServerName' , '');
define('DatabaseName' , '');
define('DatabaseUser' , '');
define('DatabasePassword' , '');
define('DatabasePort' , '');
define('CharacterSet' , 'utf8');
define('Collation' , 'utf8_general_ci');
define('Prefix' , '');

/**
 * Sistemde yollanacak maillerin nasýl yollanacaðýný belirtir
 * Mandrill veya Default  seçilebilir
 * Default seçilirse php mail ile mail atýlýr. 
 */
define('Mail_Send_Type' , 'Mandrill');

/**
 * TRUE  : Memcached ten getiriyor. databaseden okuyor.
 * FALSE : Settings->Languages altýndaki dosyalardan okuyor.
 *
 */
define('TraslateCache' , TRUE);

/**
 * Sitenin gerçek ortamda yayýnda olmasý anlamýna geliyor.
 * Bazý yerlerde sadece Gerçek ortamda çalýþmasý gereken kodlar olmasý gerektiðinden.
 */
define('LIVE' , FALSE );

/**
 * Yüklenen resimlerin yollarý ve geniþlik , yükseklik ayarlarý
 */
define('IMAGES_BACKGROUND_COLOR', '#FFFFFF' );
define('IMAGES_ORIGINAL_PATH'	, 'Original' );
define('IMAGES_THUMBNAIL_PATH'	, 'Thumbnail' );
define('IMAGES_MIN_PATH'		, 'Min' );
define('IMAGES_MAX_PATH'		, 'Max' );

/**2014-08-04 15:48:36
 * HtmlLog
 * 0: loglamýyor
 * 1: loglayýp ekrana çýktý olarak yazýyor.
 * 2: logluyor çýktý vermiyor.
 */
define('HtmlLog'  				, '2'						);
define('COMMENT_BRACKETS'   	, '<br/>'					);

//############################################################################
// 					Html Log Output

	$htmlLog = new HtmlLog;

//############################################################################
// 				OTHER FUNCTIONS

	function prpl( $value='' )
	{
		ob_start();
	    print_r($value);
	    $result = ob_get_clean();
		global $htmlLog;
		$logClass = str_replace( '[*title*]', ' OTHERS ' , Config::HtmlLog('fileread') );
		$logClass = str_replace( '[*Name*]', $result  , $logClass );
		$htmlLog->Log( $logClass );

	}

	function prpe( $value='' )
	{
		echo '<pre>';
	    print_r($value);
	    echo '</pre>';
	    exit();

	}

	function prp( $value='' )
	{
		echo '<pre>';
	    print_r($value);
	    echo '</pre>';
	}

	function FileControl($file='')
	{
		if (! $file ) {
			exit(L10n::T('Pages Not Found'));
		}
	}

	function include_class($Class_Name='')
	{
		global $htmlLog;
		
		$htmlLog->In_Class( $Class_Name );
	}

	function close( $htmllog = TRUE)
	{
		if ($htmllog) {
			Config::HtmlLogOutPut();
		}
		exit();
	}
