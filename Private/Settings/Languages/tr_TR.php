<?php 

	
	// Yonetim Paneli
	$L10n['Tablo Pasif Durumdadır'] = 'Tablo Pasif Durumdadır';
	$L10n['update']                 = 'Güncelle';
	$L10n['new']                    = 'Kaydet';
	$L10n['Yüklenen Sınıf']         = 'Yüklenen Sınıf';
	$L10n['Hareketler']             = 'Hareketler';
	$L10n['Degistir']               = 'Değiştir';
	$L10n['Sil']                    = 'Sil';
	$L10n['Pages Not Found']        = 'Dosya Bulunamadı';
	$L10n['UPDATE_SUCCESS']        = 'Güncelleme Başarılı';
	$L10n['INSERT_SUCCESS']        = 'Ekleme Başarılı';
	$L10n['DELETE_SUCCESS']        = 'Silme Başarılı';


	// Site Top
	$L10n['MyAccount']              = 'Hesabım';
	$L10n['MyWishlist']             = 'Favorilerim';
	$L10n['MyCart']                 = 'Sepetim';
	$L10n['Checkout']               = 'Ödeme';
	$L10n['LogIn']                  = 'Giriş';
	$L10n['Arşiv Diyarı']           = 'Arşiv Diyarı';
	$L10n['Dil']                    = 'Dil';
	$L10n['Language']               = 'Türkçe';
	$L10n['Menu']                   = 'Menü';
	$L10n['Cart']                   = 'Sepetim';
	$L10n['Recently added item(s)'] = 'Son eklenen ürün/ürünler';
	$L10n['View Cart']              = 'Görüntüle';
	$L10n['Checkout']               = 'Alışverişi Tamamla';
	// Site SLAYT
	$L10n['MORE']                   = 'DEVAMI';
	
	$L10n['Konular']        = 'KONULAR';
	$L10n['AnaSayfa']       = 'ANA SAYFA';
	$L10n['Help']           = 'YARDIM';
	$L10n['SosyalAglar']    = 'SOSYAL AĞLAR';
	$L10n['Close']          = 'Kapat';
	$L10n['Call Us']        = 'Bizi Arayın';
	$L10n['Search here']    = 'Arama';
	$L10n['YeniEklenenler'] = 'Yeni Eklenenler';


	// Dokuman Enum
	$L10n['IMZALI']      = 'İmzalı';
	$L10n['IMZASIZ']     = 'İmzasız';
	$L10n['ORJINAL']     = 'Orjinal';
	$L10n['FOTOKOPI']    = 'Fotokopi';
	$L10n['TIPKI_BASIM'] = 'Tıpkı Basım';
	$L10n['ELEKTRONIK']  = 'Elektronik';
	$L10n['BASILI']      = 'Basılı';
	$L10n['EL_YAZISI']   = 'El Yazısı';
	$L10n['RENKLI']      = 'Renkli';
	$L10n['SIYAH_BEYAZ'] = 'Siyah & Beyaz';
	
	$L10n['Dokuman_Imza']         = 'İmza';
	$L10n['Dokuman_Kagit']        = 'Kağıt';
	$L10n['Dokuman_Ozellik']      = 'Kalite';
	$L10n['Dokuman_Renk']         = 'Renk';
	$L10n['Dokuman_Boyut']        = 'Boyut';
	$L10n['Dokuman_Sayfa_Sayisi'] = 'Sayfa Sayısı';
	$L10n['Dokuman_Kopya_Sayisi'] = 'Kopya Sayısı';
	$L10n['Advanced_Search']      = 'Gelişmiş  Arama';
	$L10n['Advanced_Search_Options'] = 'Gelişmiş  Arama Seçenekleri';

	// FOOTER
	$L10n['FollowUs']                  = 'Bizi Takip Edin !';
	$L10n['Follow_us_in_social_media'] = 'Sosyal medyada bizi takip edin';
	$L10n['Newsletter_Signup']         = 'Bülten Aboneliği';
	$L10n['Sign_up_for_newsletter']    = 'Bülteni için kaydolun';
	$L10n['Information']               = 'Bilgi Edinme';

	$L10n['About_us']         = 'Hakkımızda';
	$L10n['Privacy_Policy']   = 'Gizlilik Politikası';
	$L10n['Terms_Condotions'] = 'Şartlar Koşullar';
	$L10n['Secure_payment']   = 'Güvenli Ödeme';
	$L10n['My_account']       = 'Hesabım';
	$L10n['Order_History']    = 'Sipariş Geçmişi';
	$L10n['Wis_List']         = 'İstek Listesi';
	$L10n['Newsletter']       = 'Bülten';


	// DETAY
	$L10n['Description']     = 'Açıklama';
	$L10n['Reviews']         = 'Yorumlar';
	$L10n['Related_Product'] = 'İlgili Ürünler';
	$L10n['Add_your_review'] = 'Yorum Ekleyin';
	$L10n['Add_to_Cart']     = 'Sepete Ekleyin';

	// LOGIN
	$L10n['Login_or_Create_an_Account']     = 'Müşteri Girişi veya Yeni Kayıt';
	?>