<?php 
	
	
		
	// Yonetim Paneli
	$L10n['Tablo Pasif Durumdadır'] = 'Tablo Pasif Durumdadır';
	$L10n['update']                 = 'Güncelle';
	$L10n['new']                    = 'Kaydet';
	$L10n['Yüklenen Sınıf']         = 'Yüklenen Sınıf';
	$L10n['Hareketler']             = 'Hareketler';
	$L10n['Degistir']               = 'Değiştir';
	$L10n['Sil']                    = 'Sil';
	$L10n['Pages Not Found']        = 'Dosya Bulunamadı';
	$L10n['UPDATE_SUCCESS']        = 'Successful of renovation';
	$L10n['INSERT_SUCCESS']        = 'Successful insertion';
	$L10n['DELETE_SUCCESS']        = 'Successful deletion';
	
	// Site Top
	$L10n['MyAccount']              = 'My Account';
	$L10n['MyWishlist']             = 'My Wishlist';
	$L10n['MyCart']                 = 'My Cart';
	$L10n['Checkout']               = 'Checkout';
	$L10n['LogIn']                  = 'Log In';
	$L10n['Arşiv Diyarı']           = 'The Land Archive';
	$L10n['Dil']                    = 'Language';
	$L10n['Language']               = 'English';
	$L10n['Menu']                   = 'Menu';
	$L10n['Cart']                   = 'Cart';
	$L10n['Recently added item(s)'] = 'Recently added item(s)';
	$L10n['Checkout']               = 'Checkout';
	
	// Site SLAYT
	$L10n['MORE']           = 'MORE';
	$L10n['Konular']        = 'TOPICS';
	$L10n['AnaSayfa']       = 'HOME PAGE';
	$L10n['Help']           = 'HELP';
	$L10n['SosyalAglar']    = 'SOCIAL NETWORKS';
	$L10n['Close']          = 'Close';
	$L10n['Call Us']        = 'Call Us';
	$L10n['Search here']    = 'Search here';
	$L10n['YeniEklenenler'] = 'New Additions';

	// Dokuman Enum
	$L10n['IMZALI']        = 'Signed';
	$L10n['IMZASIZ']       = 'Unsigned';
	$L10n['ORJINAL']       = 'Original';
	$L10n['FOTOKOPI']      = 'Copying';
	$L10n['TIPKI_BASIM']   = 'Reproductions';
	$L10n['ELEKTRONIK']    = 'Electronics';
	$L10n['BASILI']        = 'Print';
	$L10n['EL_YAZISI']     = 'Handwriting';
	$L10n['RENKLI']        = 'Color';
	$L10n['SIYAH_BEYAZ']   = 'Black & White';

	$L10n['Dokuman_Imza']              = 'Sign';
	$L10n['Dokuman_Kagit']             = 'Paper';
	$L10n['Dokuman_Ozellik']           = 'Quality';
	$L10n['Dokuman_Renk']              = 'Color';
	$L10n['Dokuman_Boyut']             = 'Size';
	$L10n['Dokuman_Sayfa_Sayisi']      = 'Number of Pages';
	$L10n['Dokuman_Kopya_Sayisi']      = 'Number of Copies';
	$L10n['Advanced_Search']           = 'Advanced Search';
	$L10n['Advanced_Search_Options']   = 'Advanced Search Options';

	// FOOTER
	$L10n['FollowUs']                  = 'Follow Us';
	$L10n['Follow_us_in_social_media'] = 'Follow us in social media';
	$L10n['Newsletter_Signup']         = 'Newsletter Signup';
	$L10n['Sign_up_for_newsletter']    = 'Sign up for newsletter';
	$L10n['Information']               = 'Information';

	$L10n['About_us']         = 'About us';
	$L10n['Privacy_Policy']   = 'Privacy Policy';
	$L10n['Terms_Condotions'] = 'Terms Conditions';
	$L10n['Secure_payment']   = 'Secure Payment';
	$L10n['My_account']       = 'My Account';
	$L10n['Order_History']    = 'Order History';
	$L10n['Wis_List']         = 'Wish List';
	$L10n['Newsletter']       = 'Newsletter';

	// DETAY
	$L10n['Description']     = 'Description';
	$L10n['Reviews']         = 'Reviews';
	$L10n['Related_Product'] = 'Related Product';
	$L10n['Add_your_review'] = 'Add Your Review';
	$L10n['Add_to_Cart']     = 'Add to Cart';

	// LOGIN
	$L10n['Login_or_Create_an_Account']     = 'Login or Create an Account';
	

 ?>
