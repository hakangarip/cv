<?php 


//############################################################################
// 					Autoload Register

	spl_autoload_register('OtomatikSinifYukleme');
	function OtomatikSinifYukleme($ClassName) {
		global $htmlLog;

		$explodeClass = explode('_', $ClassName);
		$path = $ClassName;

		if (count($explodeClass) > 1) {
			$path='';
			foreach ($explodeClass as $key => $value) {
				 $path .=$value.SLASH;
			}
			$path=substr($path, 0, strlen($path)-1);
		}

		$Class_Folders = array('Class','MyClass');
		foreach ($Class_Folders as $key => $value) {
			
			$class = DR.SLASH.PRIVATE_FOLDER.SLASH.'Classes'.SLASH.$value.SLASH.$path.SLASH.$ClassName.'.php';
			if (is_file($class)) 
				break;
			
			$class='';
		}

		if ( $ClassName <>'HtmlLog')  
			$htmlLog->Set_Class( $ClassName );
		
		if(!empty($class)) 
			include_once $class;
	}

//############################################################################
// 					SITE RUN

	function RUN() {
		
		Config::Cache('LANGUAGES');
		$pager = new Pager;
		$model_return = $pager->model() ;
		$pager->view( $model_return);

		global $htmlLog;
		$htmlLog->Set_Session('UNIQUE_ID' ,  $_SERVER['UNIQUE_ID'] );
		$htmlLog->Set_Session('REMOTE_ADDR' ,  $_SERVER['REMOTE_ADDR'] );
		$htmlLog->Set_Session('REMOTE_PORT' ,  $_SERVER['REMOTE_PORT'] );
		$htmlLog->Set_Session('HTTP_REFERER' ,  @$_SERVER['HTTP_REFERER'] );		
		$htmlLog->Set_Session('HTTP_USER_AGENT' , $_SERVER['HTTP_USER_AGENT'] );		
		$htmlLog->Set_Session('REDIRECT_URL' ,  @$_SERVER['REDIRECT_URL'] );
		$htmlLog->Set_Session('REQUEST_URI' ,  @$_SERVER['REQUEST_URI'] );
		$htmlLog->Set_Session('REQUEST_TIME' ,  $_SERVER['REQUEST_TIME'] );
		$htmlLog->Set_Session('REQUEST_TIME' ,  $_SERVER['REQUEST_TIME'] );

	}
