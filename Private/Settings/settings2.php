<?php

//############################################################################
// 							DEFINES

define('SLASH'						, '/'										); // Site Genel SLASH
define('SITE_PATH'  				, 'http://belge/'							); // Site Url Path
define('DR'   						, $_SERVER["DOCUMENT_ROOT"]					); // Document Root
define('SITE_IP'     				, '212.58.19.101'			    			); // Site ip adresi, bazı upload dosyalarında sadece bu ipden ulaşılmasına izin veriliyor.
define('PRIVATE_FOLDER'   		    ,'Private'									); // Sistemdeki arka planda kullanıcı planında olmayan kodlar
define('PUBLIC_FOLDER'   			,'Public'  									); // Sistemin önünde, son kullanıcıya erişim sağlanır.
define('DEFAULT_LANGUAGE'  			, 'tr_TR'									); // sistem ilk açıldığındaki default language code
define('UPLOAD_FOLDER' 				, 'Upload'									); // Upload Klasörü
define('CKE' 				        , 'CKE'										); // Yonetim panelinde kullanılan editörün klasör adı
define('MEMCACHE_PREFIX'			, 'belgecomtr'								); // mmecahceteki prefix.
define('Now'						, date('Y-m-d H:i:s')						); // tarih zaman 
define('NowDate'					, date('Y-m-d')								); // tarih
define('ASJ_FOLDER'					, 'Settings/asj/'		); // asj dosyalarının bulunduğu dizin
define('ASSETS'					 	, 'Assets'									); // javascript, css sitede kullanılan jpg dizini
define('TEMPLATE_FOLDER_NAME'		, 'Template'								); // 
define('TEMPLATE_SUB_CLASS_NAME'	, 'Template'								); // 
define('SESS_ADMIN' 				, 'Administrator'							); // Session yonetim paneli ön eki
define('YONETIM_PATH'  				, SITE_PATH.PRIVATE_FOLDER.SLASH.'Yonetim/'	); // Yonetim paneli dizini
define('ADMIN_PANEL_DEFAULT_LANGUAGE', 'tr_TR'									); // Yonetim paneli default dili. 

// define('CacheDriver'				, 'Memcache'								); // cache drive 

include DR.SLASH.PRIVATE_FOLDER.SLASH.'Settings/global_settings.php';

/**
 * Mysql Db Config 
 */

define('ServerName' , '');
define('DatabaseName' , '');
define('DatabaseUser' , '');
define('DatabasePassword' , '');
define('DatabasePort' , '');
define('CharacterSet' , 'utf8');
define('Collation' , 'utf8_general_ci');
define('Prefix' , '');

/**
 * Sistemde yollanacak maillerin nasıl yollanacağını belirtir
 * Mandrill veya Default  seçilebilir
 * Default seçilirse php mail ile mail atılır. 
 */
define('Mail_Send_Type' , 'Mandrill');


/**
 * TRUE  : Memcached ten getiriyor. databaseden okuyor.
 * FALSE : Settings->Languages altındaki dosyalardan okuyor.
 *
 */
define('TraslateCache' , TRUE);

/**
 * Sitenin gerçek ortamda yayında olması anlamına geliyor.
 * Bazı yerlerde sadece Gerçek ortamda çalışması gereken kodlar olması gerektiğinden.
 */
define('LIVE' , FALSE );

/**
 * Yüklenen resimlerin yolları ve genişlik , yükseklik ayarları
 */
define('IMAGES_BACKGROUND_COLOR', '#FFFFFF' );
define('IMAGES_ORIGINAL_PATH'	, 'Original' );
define('IMAGES_THUMBNAIL_PATH'	, 'Thumbnail' );
define('IMAGES_MIN_PATH'		, 'Min' );
define('IMAGES_MAX_PATH'		, 'Max' );

/**2014-08-04 15:48:36
 * HtmlLog
 * 0: loglamıyor
 * 1: loglayıp ekrana çıktı olarak yazıyor.
 * 2: logluyor çıktı vermiyor.
 */
define('HtmlLog'  				, '0'						);
define('COMMENT_BRACKETS'   	, '<br/>'					);



//############################################################################
// 					Html Log Output

	$htmlLog = new HtmlLog;

//############################################################################
// 				OTHER FUNCTIONS

	function prp( $value='' )
	{
		ob_start();
	    print_r($value);
	    $result = ob_get_clean();
		global $htmlLog;
		$logClass = str_replace( '[*title*]', ' OTHERS ' , Config::HtmlLog('fileread') );
		$logClass = str_replace( '[*Name*]', $result  , $logClass );
		$htmlLog->Log( $logClass );

	}

	function prpe( $value='' )
	{
		echo '<pre>';
	    print_r($value);
	    echo '</pre>';
	}



	function FileControl($file='')
	{
		if (! $file ) {
			exit(L10n::T('Pages Not Found'));
		}
	}

	function include_class($Class_Name='')
	{
		global $htmlLog;
		
		$htmlLog->In_Class( $Class_Name );
	}

	function close( $htmllog = TRUE)
	{
		if ($htmllog) {
			Config::HtmlLogOutPut();
		}
		exit();
	}
