<?php

/**
*  Sitedeki Sepet Classı
*/
/*
	$sepet = new Sepet;
	echo $sepet->Add(12,10);
	echo $sepet->Update(13,10);
	echo $sepet->Remove(12);
	echo $sepet->Remove_All();
*/

class Sepet
{
	
	public $Db       = '';
	public $Sess     = '';
	public $Max_Adet = '';

	function __construct()
	{
		include_class( get_class() );
		$this->Db   = new Db;
		$this->Sess = new Sess;
		$this->Max_Adet();
	}

	/**
	 * Max_Adet 
	 * Sepetteki maxsimim adedi belirliyor. 
	 * @param integer $adet [description]
	 */
	public function Max_Adet( $adet=1 )
	{
		$this->Max_Adet = $adet;
	}

	/**
	 * Add
	 * Sepete eklenecek ürün
	 *
	 */
	public function Add( $urun_id = '' , $adet = 1 , $boyutu = '' )
	{
		if ( empty($urun_id) ) {
			return False;
		}

		$Sepet_Id = $this->Sepet_Id();
		$Sepet_Urunler = $this->Sess->get( Config::Session_Name('SEPET') );
		$Sepet_Urunler_Add = array();
		if ( isset($Sepet_Urunler['URUNLER']) AND count($Sepet_Urunler['URUNLER']) > 0 ) {
			if ( isset($Sepet_Urunler['URUNLER'][$urun_id][$boyutu]) ) {
				
				if ( $Sepet_Urunler['URUNLER'][$urun_id][$boyutu] <= $this->Max_Adet ) {
					return L10n::T('Tek seferde alınabilecek sayıyı aştınız.  ');
				}

				$this->Update($urun_id , $Sepet_Urunler['URUNLER'][$urun_id][$boyutu] + $adet ,$boyutu);
				return 'ADD';
			}
			foreach ($Sepet_Urunler['URUNLER'] as $k => $v) {
				$Sepet_Urunler_Add['URUNLER'][$k] =  $v;
			}
		}

		$data['Key'][]  = 'SEPET_ID';
		$data['Key'][]  = 'URUN_ID';
		$data['Key'][]  = 'SEPET_URUN_ADET';
		$data['Key'][]  = 'BOYUTU';
		$data['Key'][]  = 'SEPET_URUN_EKLENME_TARIHI';
		$data['Data'][] = $Sepet_Id ;
		$data['Data'][] = $urun_id ;
		$data['Data'][] = $adet ;
		$data['Data'][] = $boyutu ;
		$data['Data'][] = Now ;

		$this->Db->data( $data['Key'], $data['Data'] );
		$this->Db->table('SEPET_URUN');
		$this->Db->insert();
		$lastInsertId = $this->Db->lastInsertId();
		$Sepet_Urunler_Add['URUNLER'][$urun_id][$boyutu] = $adet;
		$this->Sess->set( Config::Session_Name('SEPET') , $Sepet_Urunler_Add );
		return 'ADD';

	}

	/**
	 * Remove
	 * Sepetten ürün çıkartma
	 *
	 */
	public function Remove( $urun_id = '',$boyutu )
	{
		$Sepet_Id = $this->Sepet_Id();
		$Sepet_Urunler = $this->Sess->get( Config::Session_Name('SEPET') );
		$Sepet_Urunler_Add = array();
		foreach ($Sepet_Urunler['URUNLER'] as $k => $v) {
			if ( $urun_id !=$k ) {
				$Sepet_Urunler_Add['URUNLER'][$k] =  $v;
			} else {
				foreach ($v as $key => $value) {
					if ($key != $boyutu) {
						$Sepet_Urunler_Add['URUNLER'][$k][$key] =  $value;
					}
				}
			}
		}
		// prpe($Sepet_Urunler_Add);
		if (count($Sepet_Urunler_Add)>0 ) {
			$this->Sess->set( Config::Session_Name('SEPET') , $Sepet_Urunler_Add );
		} else{

			$Sepet_Urunler_Add['ID'] = $Sepet_Id;
			$Sepet_Urunler_Add['URUNLER'] = '';
			$this->Sess->destroy( Config::Session_Name('SEPET') );
			$this->Sess->set( Config::Session_Name('SEPET') , $Sepet_Urunler_Add);
		}

		$this->Db->where( array('SEPET_ID','URUN_ID'), array($Sepet_Id , $urun_id) );
		$this->Db->table( 'SEPET_URUN' );
		$this->Db->delete();

	}

	/**
	 * Remove_All
	 * Sepetteki Tüm ürünleri silme
	 *
	 */
	public function Remove_All()
	{
		$Sepet_Id = $this->Sepet_Id();
		$this->Sess->destroy( Config::Session_Name('SEPET') );

		$this->Db->where( array('SEPET_ID'), array($Sepet_Id) );
		$this->Db->table('SEPET_URUN');
		$this->Db->delete();

		$this->Db->where( array('SEPET_ID'), array($Sepet_Id) );
		$this->Db->table('SEPET');
		$this->Db->delete();

	}

	/**
	 * Update
	 * Sepetteki Ürünleri Güncelleme
	 *
	 */
	public function Update( $urun_id = '', $adet = 1 ,$boyutu='')
	{
		$Sepet_Urunler  = $this->Sess->get( Config::Session_Name('SEPET') );
		foreach ($Sepet_Urunler['URUNLER'] as $k => $v) {
				$Sepet_Urunler_Add['URUNLER'][$k] =  $v;
		}
		
		if ( $Sepet_Urunler['URUNLER'][$urun_id][$boyutu] > $this->Max_Adet ) {
			return L10n::T('Tek seferde alınabilecek sayıyı aştınız.  ');
		}

		$Sepet_Id       = $this->Sepet_Id();
		$data['Key'][]  = 'SEPET_URUN_ADET';
		$data['Data'][] = $adet ;

		$this->Db->data( $data['Key'], $data['Data'] );
		$this->Db->where(array('SEPET_ID','URUN_ID'), array($Sepet_Id , $urun_id));
		$this->Db->table('SEPET_URUN');
		$this->Db->update();
		$Sepet_Urunler_Add['URUNLER'][$urun_id][$boyutu] = $adet;
		$this->Sess->set( Config::Session_Name('SEPET') , $Sepet_Urunler_Add );
		return 'TRUE';

	}

	
	/**
	 * Sepet_Id
	 * Sepete ai t uniq id geri dönülüyor.
	 * Bunu sessiondan alıyor.
	 * Eğer sessionda yok ise yeni bir tane yaratıyor.
	 */
	public function Sepet_Id()
	{


		$SEPET = $this->Sess->get( Config::Session_Name('SEPET') );
		if ( $SEPET ) {
			if (!empty($SEPET['ID'])) {
				return $SEPET['ID'];
			}
		}

		$Users = new Users;

		if ($Users->LoginValid() == 'TRUE') {
			$data['Key'][]  = 'USER_ID';
			$data['Data'][] = Config::UserSess('ID');
		}

		$data['Key'][]  = 'GUEST_ID';
		$data['Key'][]  = 'SEPET_OLUSTURULMA_TARIHI';
		$data['Data'][] = $this->Sess->Id();
		$data['Data'][] = Now;

		$this->Db->data( $data['Key'], $data['Data'] );
		$this->Db->table('SEPET');
		$this->Db->insert();
		$Sepet_Id['ID'] = $this->Db->lastInsertId();

		$this->Sess->set( Config::Session_Name('SEPET') , $Sepet_Id );
		return  $Sepet_Id['ID'];
	}

	/*
	 * lists
	 * eğer eklenmiş üsrünler var ise geri döndürülüyor.
	 * @return [type] [description]
	 */
	public function lists()
	{

		$SEPET = $this->Sess->get( Config::Session_Name('SEPET') );
		if ( $SEPET != 'FALSE') {
			return $SEPET['URUNLER'];
		}
		return false;

	}

	public function Login()
	{

		$SEPET = $this->Sess->get( Config::Session_Name('SEPET') );


		if ( $SEPET != FALSE ) {

			$this->Db->where( array('USER_ID' ,'SEPET_STATUS'),  array( Config::UserSess('ID') , 'TRUE')  );
			$this->Db->table('SEPET');
			$this->Db->select();
			$result = $this->Db->result();
			if (count($result) > 0) {
				foreach ($result as $k => $v) {
					$Db_Sepet_Id = $v['SEPET_ID'];

					$this->Db->where( array('SEPET_ID'), array($Db_Sepet_Id ) );
					$this->Db->table('SEPET_URUN');
					$this->Db->delete();

					$this->Db->where( array('SEPET_ID'), array($Db_Sepet_Id) );
					$this->Db->table('SEPET');
					$this->Db->delete();
				}

			}

			$this->Db->data(array('USER_ID'),  array(Config::UserSess('ID')) );
			$this->Db->where( array('SEPET_ID'), array( $this->Sepet_Id() ) );
			$this->Db->table('SEPET');
			$this->Db->update(); 
			return TRUE;
		}

		$this->Db->where( array('SEPET.USER_ID' ,'SEPET.SEPET_STATUS'),  array( Config::UserSess('ID') , 'TRUE')  );
		$this->Db->table('SEPET');
		$this->Db->join( 'SEPET_URUN' , 'SEPET_URUN.SEPET_ID=SEPET.SEPET_ID');
		$this->Db->select();
		$result = $this->Db->result();
		// prpe($this->Db->lastQuery());
		foreach ($result as $k => $v) {
			$Sepet_Urunler_Add['ID'] = $v['SEPET_ID'];

			$Sepet_Urunler_Add['URUNLER'][$v['URUN_ID']] =  $v['SEPET_URUN_ADET'];
		}
		$this->Sess->set( Config::Session_Name('SEPET') , $Sepet_Urunler_Add );



	}

	public function Logout()
	{
		$this->Sess->destroy( Config::Session_Name('SEPET') );
	}

}