<?php

/**
*
*/
class Admin_Create_Form
{
	public $Upload                    = 'TRUE';
	public $Comment                   = '';
	public $Languages                 = '';
	public $Property                  = '';
	public $Icons                     = '';
	public $HTML                      = '';
	public $HtmlAtt                   = '';
	public $Translate_Language        = '';
	public $DatasTranslate            = '';
	public $Translate                 = '';
	public $DatasUpload               = '';
	public $DatasSub                  = '';
	public $DatasMain                 = '';
	public $TableFieldsFields         = '';
	public $TableFieldsNames          = '';
	public $Id                        = '';
	public $R                         = '';
	public $AdminPanelDefaultLanguage = '';
	public $Request                   = '';
	public $category_sirlama;
	public $category_sira;
	public $category_secililer = array();
	public $d;
	public $de = true;

	function __construct()
	{

	}

	public function set($json = '' , $action = 'INSERT_FORM' )
	{
		$this->AdminPanelDefaultLanguage = Config::AdminPanelDefaultLanguage();
		$R             = new Request;
		$this->Request = $R;
		// prpe($R->allGet());
		$this->HtmlAtt = new HtmlAtt;
		$this->R       = $R;
		$TABLE_NAME    = strtoupper( $R->get('pages') );

		$json = json_decode( $json );
		$json = json_decode( json_encode( $json ), true);

		$this->Property( $json['Property'] );
		$this->Languages( $json['Languages'] );
		$this->Translate_Language( $json['Languages'] );
		$this->Icons( $json['Icons'] );

//########################################################################################################################
// INSERT OR UPDATE
		if ($action != 'TABLE' ) {

			if ( $action == 'ASJ') { return; }
			$action_db = 'Insert';
			if ( $action == "UPDATE_FORM" ) {
				$this->setDatas( $TABLE_NAME , $R->get('id') );
				$this->Id  = $R->get('id');
				$action_db = 'Update';
			}
			$this->Tables( $json['Tables'], $action );
			$formIcerik  = str_replace('[*value*]', L10n::T( $R->get('action') ),  Config::AdminCreate('submit_button') );
			$formIcerik .= $this->HtmlAtt->input('hidden', 'action',  $action_db );
			$formIcerik .= $this->HtmlAtt->input('hidden', 'pages',  $TABLE_NAME );

			if ( $action == "UPDATE_FORM" ) {
				$formIcerik .= $this->HtmlAtt->input('hidden', 'id',  $R->get('id') );
				$this->id    = $R->get('id');
			}
			$form = str_replace('[*baslik*]', $this->getTranslate( 'Caption', 'Data', $R->get('action') ) , Config::AdminCreate('form') );
			$form = str_replace('[*icon-class*]', $this->Icons[ $R->get('action') ], $form );

			$formUploadIcerik = $formUploadBaslik = '';

			if ($this->Upload == "TRUE" and  $action == "UPDATE_FORM") {

				$formUploadBaslik = str_replace('[*baslik*]', 'Image Upload', Config::AdminCreate('upload') );
				$formUploadBaslik = str_replace('[*icon-class*]', '',  $formUploadBaslik );
				$formUploadIcerik = str_replace('[*resimler*]', $this->FileUpload(), Config::AdminCreate('upload_form') );
				$formUploadIcerik = str_replace('[*pages*]',$TABLE_NAME , $formUploadIcerik );
				$formUploadIcerik = str_replace('[*id*]',$R->get('id') , $formUploadIcerik );
			}

			$form = str_replace('[*upload*]', $formUploadBaslik , $form );
			$form = str_replace('[*upload_form*]', $formUploadIcerik , $form );
			$form = str_replace('[*icerik*]' , $this->HTML . $formIcerik , $form);
			
			return $form;

		}

//########################################################################################################################
// TABLE LIST
		// $this->Property( $json['Property'] );
		
		$this->TableFields( $json );
		$formIcerik = str_replace('[*tablo_title*]', $json['Languages'][$this->AdminPanelDefaultLanguage]['Tables']['TableCaption'] ,  Config::AdminCreate('table') );
		$formIcerik = str_replace('[*fields_name*]', $this->TableFieldsNames ,  $formIcerik );
		$formIcerik = str_replace('[*fields_values*]', $this->setTableDatas( $TABLE_NAME ),  $formIcerik );
		return $this->Search().$formIcerik;

	}

	public function FileUpload($value='')
	{

		$return = '';
		foreach ($this->DatasUpload as $key => $value) {

			$Upload_Path_Pages = Config::SitePath().Config::UploadFolder().SLASH.strtoupper($this->R->get('pages') );
			$Upload_Path       = $Upload_Path_Pages.SLASH.Config::UploadFolder('Original');
			$orignalfilepath   = $Upload_Path.SLASH.$value['U_NAME'];
			$selected          = ($value['U_SELECTED']) ? 'checked=""' : '';

			$return .='<div class="detailimage">
	    					<img src="'.$orignalfilepath .'" alt=""  height="120">
	    					<input type="button" class="mws-button red small" value="Sil" onclick="tableDelete(\''.$this->R->get('pages').'\',\''.$value['U_ID'].'\',\'Upload_Delete_Files\')">
	    					<a href="'.$orignalfilepath.'" target="_blank"><input type="button" value="Görüntüle" class="mws-button green small"></a>
	    					<input type="checkbox" onclick="thumbssec(\''.$this->R->get('pages').'\',\''.$value['MAIN_ID'].'\',\''.$value['U_ID'].'\')" '.$selected.'><label class="selecclass">Seçili</label>
	    				</div>';
		}
		// $return .= '<div>';
		return $return;
	}

	public function TableFields($json='')
	{
		$this->Tables = $json['Tables'];

		$th =' <th style="width:17px">#</th>';

		foreach ($this->Tables as $key => $value) {

			foreach ($value as $k => $v) {

				if (isset($v['grid_status']) and $v['grid_status'] == 'true') {
					$v['name'] = str_replace( '[]' , '', $v['name']);
					$this->TableFieldsFields['FIELDS'][]                 = $v['name']; // Jsonden gelenler ile sqlden gelenlere in_array yapılıp kontrol ediliyor. Ona göre ekrana bastırılıyor.
					$this->TableFieldsFields['TABLE'][$v['name']]        = $key;
					$this->TableFieldsFields['TABLE_VALUES'][$v['name']] = $v;
					$th .= ' <th>'.$json['Languages'][$this->AdminPanelDefaultLanguage]['Tables'][$key][$k]['title'].'</th>';
				}

			}

		}

		$th .=' <th style="width:70px; text-align: center;">'.L10n::T('Hareketler').'</th>';
		$this->TableFieldsNames ='<tr>'.$th.'</tr>';
	}

	public function Property($Property='')
	{
		if ( strtoupper($Property['Status']) == 'FALSE' ) {
			exit(L10n::T('Tablo Pasif Durumdadır'));
		}

		if ( $Property['Uploads'] == 'FALSE' ) {
			$this->Upload = 'FALSE';
		}

		$this->Comment   = $Property['Comment'];
		$this->Translate = $Property['Table'];
		$this->Property  = $Property;

	}

	public function Languages($Languages='')
	{
		$this->Languages = $Languages[ Config::Language() ];
	}

	public function Translate_Language($Translate_Language='')
	{
		$this->Translate_Language = $Translate_Language;
	}

	public function Icons($Icons='')
	{
		$this->Icons = $Icons;
	}

	public function setTableDatas($table='' , $id = '')
	{

		$DEGISTIR = L10n::T('Degistir');
		$SIL      = L10n::T('Sil');
		$cache    = new Cache_Tables;
		$DB       = new Db;

		$cache->table('LANGUAGES');
		$where = array();
		$DB->table( $table );
		$Table_Translate = $table . '_TRANSLATE';
		if ( $this->Translate != 'Main' ) {
			foreach ($cache->get('LANGUAGES') as $key => $value) {
				if ( $this->AdminPanelDefaultLanguage == $value['LANGUAGE_CODE'] ) {
					$LANGUAGE_ID =  $value['ID'];
				}
			}
			// $where['KEY'][] = $table.'_TRANSLATE.LANGUAGE_ID';
			// $where['VAL'][] = $LANGUAGE_ID;
			$DB->join( $table.'_TRANSLATE' , $table.'_TRANSLATE.MAIN_ID = '.$table.'.ID');
		}

		foreach ($this->Request->allGet() as $key => $value) {

			if ($key != 'action' AND $key != 'pages' ) {
				if (!empty($value)) {

					if (isset($this->Tables['Main'][$key])) {

						$where['KEY'][] = $key;
						$where['VAL'][] = $value;
					} elseif (isset($this->Tables['Translate'][$key])) {

						$where['KEY'][] = $Table_Translate.'.'.$key;
						$where['VAL'][] = $value;

					} elseif (isset($this->Tables['MultiSelect'][$key]) and $this->Tables['MultiSelect'][$key]['type'] != 'category' ) {

						// Multi Select
						$Sub_Table = $this->Tables['MultiSelect'][$key]['insert_table'];
						$where_sub_query = '';
						foreach ($value as $k => $v) {
							if (!empty($v)) {
								$where_sub_query .= ' OR '.$Sub_Table.'.'.$key . ' = "'.$v.'" ';
							}
						}

						if (!empty($where_sub_query)) {
							$where_sub_query =substr($where_sub_query , 3, strlen($where_sub_query));
							$sub_query = ' SELECT MAIN_ID FROM '.$Sub_Table.' WHERE  ' . $where_sub_query;
							$DB->whereIn( $table.'.ID' , $sub_query);
						}

					}

				}
			}

		}
		if (!empty($where)) {
			$DB->where( $where['KEY'] , $where['VAL'] );
		}

		$DB->groupby($table.'.ID');
		$DB->select();

		$this->DatasMain = $DB->result();
		$tr = '';
		$i = 1;
		
		foreach ($this->DatasMain as $key => $value) {
			$td = '';
			$td .= '  <td>'.$i.'</td>';
			$i++;
			foreach ($this->TableFieldsFields['FIELDS'] as $k => $v) {

					switch ( $this->TableFieldsFields['TABLE_VALUES'][$v]['type'] ) {
							case 'enum':

									$select = $this->getTranslate('Tables', $this->TableFieldsFields['TABLE'][$v], $v, 'values' ,$value[$v]) ;
									$td .= '  <td>'.$select.'</td>';
								break;
							case 'select':
							

									$field_array = $this->TableFieldsFields['TABLE_VALUES'][$v]['db'];
									$TableName      = $field_array['table'];
									$TableTranslate = ($field_array['translate']=='true') ? 'true' : 'false' ;
									$TableFiled     = $field_array['field'];
									
									if ( !isset( $select_data_array[$TableName][$TableTranslate][$TableFiled] ) ){
										

										$DB->table($TableName);
										if ( $TableTranslate == 'true' ) {
											$DB->join( $TableName.'_TRANSLATE', $TableName.'_TRANSLATE.MAIN_ID = ' .$TableName.'.ID' );
										}
										
										$DB->where('ID'  , $value[$v]);
										$DB->select($TableFiled." , ID" );
										$select_data_array[$TableName][$TableTranslate][$TableFiled] = $DB->result();

									}

									$select = '';
									
									foreach ( $select_data_array[$TableName][$TableTranslate][$TableFiled] as $key_select_data => $value_select_data ) {
										$select.= ','. $value_select_data[$TableFiled] ;
									}

									$td .= '  <td>'.substr($select , 1 , strlen($select)).'</td>';

								break;
							case 'multiselect':

								$TableName      = $this->TableFieldsFields['TABLE_VALUES'][$v]['insert_table'];

								if ( !isset($Multi_Data_Array[$TableName]) ) {
									$Multi_Data_Array[$TableName] = array();
									$DB->table($TableName);
									$DB->select( 'MAIN_ID , '. $v );
									$Multi_Data_Array[$TableName] = $DB->result();

								}

								$Main_Ids=array();
								foreach ( $Multi_Data_Array[$TableName] as $key_multi => $value_multi) {
									if ($value_multi['MAIN_ID'] == $value['ID']) {
										$Main_Ids[]=$value_multi[$v];
									}
								}
								
								$field_array = $this->TableFieldsFields['TABLE_VALUES'][$v]['db'];
								$TableName      = $field_array['table'];
								$TableTranslate = ($field_array['translate']=='true') ? 'true' : 'false' ;
								$TableFiled     = $field_array['field'];
								
								if ( !isset( $Select_Multi_Data_Array[$TableName][$TableTranslate][$TableFiled] ) ){
									

									$DB->table($TableName);
									if ( $TableTranslate == 'true' ) {
										$DB->join( $TableName.'_TRANSLATE', $TableName.'_TRANSLATE.MAIN_ID = ' .$TableName.'.ID' );
									}
									$where['KEY'][] = $TableName.'_TRANSLATE.LANGUAGE_ID';
									$where['VAL'][] = $LANGUAGE_ID;
									$DB->where($where['KEY']  , $where['VAL']);
									$DB->select();
									$Select_Multi_Data_Array[$TableName][$TableTranslate][$TableFiled] = $DB->result();

								}
								
								$select = '';
								
								foreach ( $Select_Multi_Data_Array[$TableName][$TableTranslate][$TableFiled] as $key_select_data => $value_select_data ) {
									if (in_array( $value_select_data['ID']  , $Main_Ids)) {
										
										$select.= ','. $value_select_data[$TableFiled] ;
									}
								}

								$td .= '<td> '.substr($select , 1 , strlen($select)).' </td>';
							break;
							default:
								$td .= '  <td>'.$this->TableFieldsFoundValue( $v , $value ).'</td>';
							break;
					}
				
			}

			$UpdateLink = '?pages='. $table . '&action=Update_Form&id='.$value['ID'];
			$DeleteLink = '?pages='. $table . '&action=Delete_Form&id='.$value['ID'];

			$td .= '
						<td>
							<ul id="icons" class="ui-widget ui-helper-clearfix">
								'.$this->Mail_Send_Control($value['ID']).'
								<li class="ui-state-default ui-corner-all" title="'.$DEGISTIR.'"><a href="'. $UpdateLink .'"><span class="ui-icon ui-icon-pencil"></span></a></li>';
			if ( !isset($this->Property['Delete']) OR $this->Property['Delete']=='TRUE')  {

				$td .=' 			<li class="ui-state-default ui-corner-all" title="'.$SIL.'"><a onclick="tableDelete(\''.$table.'\',\''.$value['ID'].'\',\'DELETE\')"><span class="ui-icon ui-icon-trash"></span></a</li>';
			}

			$td .='			</ul>
						</td>
					';
			$tr .= '<tr class="">'.$td.'</tr>';
		}

		return $tr;
	}

	public function TableFieldsFoundValue($v = '' , $value='')
	{
		
		return $value[$v];
	}

	public function Mail_Send_Control($id)
	{
		if ( isset($this->Property['Mail_Control']) ) {
			if ($this->Property['Mail_Control'] == 'TRUE'){
				return '<li class="ui-state-default ui-corner-all" title="Çalıştır"><a onclick="mail_send(\''.$id.'\')"><span class="ui-icon ui-icon-play"></span></a></li>';
			}
		}
		return '';
	}

	public function setDatas($table='' , $id = '')
	{
		$DB = new Db;

		// MAIN DATAS SELECT
		if ( !empty($id) ) { $DB->where('ID' , $id); }
		$DB->table($table);
		$DB->select();
		$this->DatasMain = $DB->result();

		// TRANSLATES DATAS SELECT
		if ( !empty($id) ) { $DB->where('MAIN_ID' , $id); }
		$DB->table($table.'_TRANSLATE');
		$DB->select();
		$this->DatasTranslate = $DB->result();

		// UPLOADS DATAS SELECT
		if ( !empty($id) ) { $DB->where('MAIN_ID' , $id); }
		$DB->table($table.'_UPLOAD');
		$DB->select();
		$this->DatasUpload = $DB->result();

		// SUB CATEGORY 
		if ( !empty($id) ) { $DB->where('MAIN_ID' , $id); }
		$DB->table($table.'_SUB');
		$DB->select();
		$this->DatasSub = $DB->result();

		// prpe($this->DatasMain);
		// prp($this->DatasTranslate);
		// prp($this->DatasUpload);

	}

	public function getDataMain( $name='' )
	{
		return $this->DatasMain[0][$name];
	}

	public function getDataTranslate( $name='' , $LanguageId = '')
	{
		foreach ($this->DatasTranslate as $k => $v) {
			if( $v['LANGUAGE_ID'] ==$LanguageId ){
				return $v[$name];
			}
		}
	}

	public function Tables( $Tables='', $action)
	{
		$this->Tables = $Tables;
		if ( isset($this->Tables['Main']) ) {
			$this->Main( $this->Tables['Main'], $action );
		}

		if ( isset($this->Tables['Translate']) ) {
			$this->Translate( $this->Tables['Translate'], $action );
		}

		if ( isset($this->Tables['MultiSelect']) ) {
			$this->MultiSelect( $this->Tables['MultiSelect'], $action );
		}
	}

	public function Search()
	{
		$R = new Request;
		$cache = new Cache;
		$Search_Html = $formIcerikSearch = '';

		if ( isset($this->Property['Search']) ) {
			if ( $this->Property['Search'] == 'TRUE') {

				foreach ($this->Tables as $key => $value) {
					$this->TableName = $key;
					foreach ($value as $k => $v) {
						if (isset($v['Search'])) {
							if ( $v['Search'] == 'TRUE') {
								$Search_Html .= $this->setType($k , $v ,$R->get($k));
							}
						}
					}
				}

				$formIcerik  = str_replace('[*value*]', L10n::T('Arama'),  Config::AdminCreate('submit_button') );
				$formIcerik .= $this->HtmlAtt->input('hidden', 'action',  $R->get('action') );
				$formIcerik .= $this->HtmlAtt->input('hidden', 'pages',  $R->get('pages') );

				$formIcerikSearch = str_replace('[*tablo_title*]', L10n::T('Admin_Arama'),  Config::AdminCreate('search') );
				$formIcerikSearch = str_replace('[*icerik*]', $Search_Html.$formIcerik,  $formIcerikSearch );
				$formIcerikSearch = str_replace('class="clearfix"', '',  $formIcerikSearch );

			}
		}

		return $formIcerikSearch;
	}

	public function MultiSelect( $MultiSelect='', $action )
	{
		// prpe($MultiSelect);

		$this->TableName = 'MultiSelect';

		foreach ( $MultiSelect as $k => $v ) {
			
			if ( $v['type'] != 'category' ) {
			
				$selected = '';
				if ($action == 'UPDATE_FORM' ) {
					$DB = new Db;
					$SelectField = str_replace('[]', '', $v['name']);
					$DB->table( $v['insert_table']  ) ;
					$DB->where('MAIN_ID' , $this->Id );
					$DB->select( $SelectField );
					
					foreach ($DB->result() as $key => $value) {
						$selected[] = $value[$SelectField];
					}
					
				}

				$this->HTML .=$this->setType($k , $v , $selected);
			} else {
				$selected = '';
				if ($action == 'UPDATE_FORM' ) {
					$DB = new Db;
					$SelectField = str_replace('[]', '', $v['name']);
					$DB->table( $v['insert_table']  ) ;
					$DB->where('MAIN_ID' , $this->Id );
					$DB->select( $SelectField );
					foreach ($DB->result() as $key => $value) {
						$selected[$value[$SelectField]] = $value[$SelectField];
					}

				}

				$this->HTML .=$this->setType($k , $v , $selected);
			}

		}
	}


	public function Main( $MAIN='', $action )
	{
		$this->TableName = 'Main';
		foreach ( $MAIN as $k => $v ) {
			$selected = '';
			if ( $action == "UPDATE_FORM" ) {
				$selected = $this->getDataMain( $v['name'] );
			}

			$this->HTML .=$this->setType($k , $v , $selected);
		}
	}

	public function Translate($Translate='' , $action)
	{
		$cache = new Cache_Tables;
		$cache->table('LANGUAGES');
		$this->TableName = 'Translate';
		foreach ($Translate as $k => $v) {
			foreach ($cache->get('LANGUAGES') as $key => $value) {
				$selected = '';
				if ( $action == "UPDATE_FORM" ) {
					$selected = $this->getDataTranslate( $v['name'] , $value['ID'] );
				}
				$this->HTML .=$this->setType($k , $v, $selected , $value['LANGUAGE_CODE'] , $value['ID']);
			}
		}

	}

	public function getTranslate($main= '' , $sub='' , $name='' , $value='' ,$subvalue='')
	{

		$returnLan = $this->Languages;
		// prpe( $returnLan);
		// prpe($main.$sub.$name.$value.$subvalue);
		if ( isset($returnLan[$main]) ) {
			$returnLan = $returnLan[$main];
		}
		// if(isset($returnLan[$sub]))
		if ( isset($returnLan[$sub]) ) {
			$returnLan = $returnLan[$sub];
		}

		if ( isset($returnLan[$name]) ) {
			$returnLan = $returnLan[$name];
		}

		if ( isset($returnLan[$value]) ) {
			$returnLan = $returnLan[$value];
		}

		if ( isset($returnLan[$subvalue]) ) {
			$returnLan = $returnLan[$subvalue];
		}

		return  $returnLan  ;
	}

	public function getLanguageTitle( $value='' )
	{
		if ( isset( $this->Translate_Language[$value]['Title'] ) ) {
			return  $this->Translate_Language[$value]['Title']  ;
		}
		return '';
	}

	/**
	 * [setType description]
	 * @param string $k          [description]
	 * @param string $v          jsondaki degerler
	 * @param string $selected   [description]
	 * @param string $language   [description]
	 * @param string $languageId [description]
	 */
	public function setType($k ='' , $v = '', $selected = '', $language = '', $languageId = '')
	{

		$attr = $returnHtml ='';

		if ( isset($v['status']) ) {

			switch ( $v['status'] ) {
				case 'readonly':
					$attr = 'readonly="readonly"';
					break;
				case 'disabled':
					$attr = 'disabled="disabled"';
					break;
				case 'FALSE':
						return '';
					break;
				default:
					$attr = '';
					break;
			}
		}
		$TableSet = $this->TableName;
		if ( !empty( $language ) ) {
			$name = 'language_'.$languageId.'_'.$v['name'];
		} else {
			$name = $v['name'];
		}
		switch ( $v['type'] ) {
			case 'enum':

					foreach ( $v['values'] as $key => $value ) {
						$select[$key] = $this->getTranslate('Tables', $TableSet, $k, 'values' ,$key) ;
					}
					$returnHtml = Config::AdminCreate('input');
					$returnHtml = str_replace( '[*caption*]' , $this->TitleCreate( $language , $k ) , $returnHtml);
					$returnHtml = str_replace( '[*input*]' , $this->HtmlAtt->select( $name, $select , $selected, $attr . ' original-title="'.$this->getTranslate('Tables', $TableSet , $k, 'comment' ).'" ') , $returnHtml);

				break;
			case 'select':
			case 'multiselect':

					$select['']     ='';
					$TableName      = $v['db']['table'];
					$TableFiled     = $v['db']['field'];
					$TableTranslate = $v['db']['translate'];

					$db         = new Db;
					$db->table($TableName);
					if ( $TableTranslate == 'true' ) {
						$db->join( $TableName.'_TRANSLATE', $TableName.'_TRANSLATE.MAIN_ID = ' .$TableName.'.ID' );
					}
					$db->select($TableFiled." , ID" );

					foreach ( $db->result() as $key => $value ) {
						$select[$value['ID']] = $value[$TableFiled];
					}

					if ( $v['type'] == 'multiselect' ) {
						$attr =' multiple="multiple" size="5" ';
					}

					$returnHtml = Config::AdminCreate('input');
					$returnHtml = str_replace( '[*caption*]' , $this->TitleCreate( $language , $k ) , $returnHtml);
					$returnHtml = str_replace( '[*input*]' , $this->HtmlAtt->select( $name, $select , $selected, $attr . ' original-title="'.$this->getTranslate('Tables', $TableSet, $k, 'comment' ).'" ') , $returnHtml);
				break;
			case 'category':

				$Sess = new Sess;
				$db   = new Db;

				$TableName      = $v['db']['table'];
				$TableFiled     = $v['db']['field'];
				$TableTranslate = $v['db']['translate'];
				
				$LANGUAGE       = $Sess->get('ADMINISTRATOR');
				$LANGUAGE_ID    = $LANGUAGE['LANGUAGE_ID'];

				$db->table($TableName);

				if ( $TableTranslate == 'true' ) {
					$db->join( $TableName.'_TRANSLATE', $TableName.'_TRANSLATE.MAIN_ID = ' .$TableName.'.ID' );
				}

				$db->join( $TableName.'_SUB', $TableName.'_SUB.MAIN_ID = ' .$TableName.'.ID' , 'LEFT');
				$where['KEY'][] = $TableName.'_TRANSLATE.LANGUAGE_ID';
				$where['VAL'][] = $LANGUAGE_ID;

				$db->where($where['KEY']  , $where['VAL']);
				
				$db->orderby( $TableName.'.ID' , 'DESC');
				$db->select( $TableFiled." ,".$TableName.".ID , IFNULL(".$TableName."_SUB.".$TableName."_SUB_ID,0) AS ".$TableName."_SUB_ID " );

				foreach ( $db->result() as $key => $value ) {
					$select[$value[$TableName.'_SUB_ID']][$value['ID']] = $value;
					if ($value[$TableName.'_SUB_ID'] != 0 ) {		
						$select2[$value['ID']][] = $value[$TableName.'_SUB_ID'];
					}
				}

				$this->category_secililer[$this->Id] = array();
				$this->CategorySecililer($select2 , $this->Id,$this->Id );

				$json = $this->CategorySiralama($select , 0 , $TableName."_SUB_ID" , $TableFiled ,$selected, $this->category_secililer[$this->Id] );

				$a = '
						<script type="text/javascript">
						        $(function(){
						            $("#'.$v['name'].'").dynatree({
						                checkbox: true,
						                // Override class name for checkbox icon, so rasio buttons are displayed:
						                //classNames: {checkbox: "dynatree-radio"},
						                // Select mode 2: multi-hier
						                selectMode: 2,
						                children: [
						                   '.$json.'
						                ]
						            });
									$("form").submit(function(){
										var formData = $(this).serializeArray();
										var tree = $("#'.$v['name'].'").dynatree("getTree");
										formData = formData.concat(tree.serializeArray());
										// console.log(formData);
										$.each( tree.serializeArray(), function( key, value ) {
											// console.log(value.name);
											$(\'<input>\').attr({
											    type: \'hidden\',
											    id: value.name,
											    value:value.value,
											    name: value.name
											}).appendTo(\'form\');
										});
										// if(tree.getActiveNode()){
										// 	formData.push({name: "activeNode", value: tree.getActiveNode().data.key});
										// }
										return true;
									});
						           
						        });
						</script>
						<div id="'.$v['name'].'" name="'.$v['name'].'[]"></div>
					';
				$returnHtml = Config::AdminCreate('input');
				$returnHtml = str_replace( '[*caption*]' , $this->TitleCreate( $language , $k )  , $returnHtml);
				$returnHtml = str_replace( '[*input*]' , $a , $returnHtml);


			break;
			case 'pairing':

			break;
			case 'input':
					$returnHtml = Config::AdminCreate('input');
					$returnHtml = str_replace( '[*caption*]' , $this->TitleCreate( $language , $k )  , $returnHtml);
					$returnHtml = str_replace( '[*input*]' , $this->HtmlAtt->input('text', $name, $selected  , $attr . ' class="mws-textinput" ') , $returnHtml);

				break;
			case 'text':
					$returnHtml = Config::AdminCreate('input');
					$returnHtml = str_replace( '[*caption*]' , $this->TitleCreate( $language , $k ) , $returnHtml);
					$returnHtml = str_replace( '[*input*]' , $this->HtmlAtt->textarea($name, $selected  ,$attr . ' original-title="'.$this->getTranslate('Tables', $TableSet, $k, 'comment' ).'" ') , $returnHtml);

					break;
			case 'pass':
					$returnHtml = Config::AdminCreate('input');
					$returnHtml = str_replace( '[*caption*]' , $this->TitleCreate( $language , $k ) , $returnHtml);
					$returnHtml = str_replace( '[*input*]' , $this->HtmlAtt->input('password' ,$name, $selected  ,' original-title="'.$this->getTranslate('Tables', $TableSet, $k, 'comment' ).'" ') , $returnHtml);

					break;
			case 'editor':
					$returnHtml = Config::AdminCreate('input');
					$returnHtml = str_replace( '[*caption*]' , $this->TitleCreate( $language , $k ) , $returnHtml);
					$returnHtml = str_replace( '[*input*]' , $this->HtmlAtt->textarea($name, $selected  ,$attr . ' original-title="'.$this->getTranslate('Tables', $TableSet, $k, 'comment' ).'" ') , $returnHtml);
					$returnHtml .= '<script>CKEDITOR.replace(\''.$name.'\');</script>';

					break;
			case 'datetime':
					$returnHtml = Config::AdminCreate('input');
					$returnHtml = str_replace( '[*caption*]' , $this->TitleCreate( $language , $k )  , $returnHtml);
					$returnHtml = str_replace( '[*input*]' , $this->HtmlAtt->input('text', $name, $selected  , $attr . 'class="mws-textinput '.$name.'" ') , $returnHtml);
					$returnHtml .= '<script>$(".'.$name.'").datetimepicker({
						 dateFormat: "yy-mm-dd",
						 timeFormat: "hh:mm",
					});</script>';
				break;
			default:

				break;
		}

		$returnHtml.=$this->CommentCreate($language , $k);
		return $returnHtml;
	}

	
	public function CategorySecililer($array,$id,$id2=0)
	{
		if (isset($array[$id])) {
			
			foreach ($array[$id] as $key => $value) {
				$this->category_secililer[$id2][$value] = $value;
				if (isset($array[$value])) {
					 $this->CategorySecililer($array,$value,$id2);
				}
			}	
		} else {
			return;
		}

	}
	public function CategorySiralama($list='' , $id , $TableFiled , $Name , $selected = '' ,$select_expand)
	{
			
		foreach ($list[$id] as $key => $value) {
			$select = '';
			if ( $this->Id != $value['ID'] ) {
				$expand = '';
				if (isset($select_expand[$value['ID']])) {
					
					if ($select_expand[$value['ID']]) {
						$expand = ' expand: true, ';
					}
				}

				if ( isset($list[$value['ID']]) ) {

					if ( isset( $selected[$value['ID']] ) ) {
						$select = ' select: true ,';
					}
					
					// expand: true,
					$this->category_sirlama .= '{title: "' . $value[$Name] . '" ,'.$expand.' key :"' . $value['ID'].'", ' . $select ;
					$this->category_sirlama .= 'children: [ ';
					$this->CategorySiralama($list , $value['ID'] ,  $TableFiled , $Name, $selected ,$select_expand);
					$this->category_sirlama .= '] }, ';

				} else {
					if ( isset($selected[$value['ID']] ) ) {
						$select = '  select: true ';
					}
					$this->category_sirlama .= '{title: "' . $value[$Name] . '" ,'.$expand.' key :"' . $value['ID'].'" , '.$select.' },';
				}

			}
		}
		return $this->category_sirlama;

	}


	public function TitleCreate($language = ''  , $k )
	{

		if ( $language<>'' ) {

			$caption = $this->getTranslate('Tables',  $this->TableName, $k, 'title' ) . ' - ' .$this->getLanguageTitle( $language ) ;

			// Eğer Json da Databasedeki dil yok ise default olarak empty olarak geri dönülüyor.
			$getlanguage=$this->getLanguageTitle( $language ) ;
			if ( empty($getlanguage) ) {
 				$caption = $this->getTranslate('Tables', 'Translate', $k, 'title' ) . ' - Empty';
			}

		} else {
			$caption = $this->getTranslate('Tables',  $this->TableName, $k, 'title' ) ;
		}
		return  $caption;
	}

	public function CommentCreate($language = '' , $k='')
	{
		$returnHtml_Comment = Config::AdminCreate('comment');

		if ( $language<>'' ) {
			$comment = $this->getTranslate('Tables',  $this->TableName, $k, 'comment' ) ;
		} else {
			$comment = $this->getTranslate('Tables',  $this->TableName, $k, 'comment' ) ;
		}
		if (empty($comment)) {
			return '';
		}
		$returnHtml_Comment = str_replace( '[*comment*]' , $comment , $returnHtml_Comment);
		return $returnHtml_Comment;
	}

	public function c()
	{
		// prp($this->Comment);
		// prp($this->Languages);
		// prp($this->Upload);
		// prp($this->Tables);
		// prp($this->Icons);
	}

	public function clear()
	{
		$this->Comment      = '';
		$this->Upload       = TRUE;
		$this->Languages    = '';
		$this->Property     = '';
		$this->Icons        = '';
		$this->HTML         = '';
		$Translate_Language = '';
	}
}