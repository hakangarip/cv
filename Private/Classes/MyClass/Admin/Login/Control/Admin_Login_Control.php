<?php 

/**
* 
*/
class Admin_Login_Control
{
	public $Admin_Sess = array('UserName', 'Name','Email', 'Phone', 'CreationTime', 'EndTime', '' );
	public $Sess;

	function __construct()
	{
		$this->Sess = new Sess;
	}

	public function destroy()
	{
		$this->Sess->destroy('ADMINISTRATOR');
		
	}

	public function control()
	{
		$DB = new Db;
		$P  = new Post;
		
		$DB->table('YONETICILER');
		$DB->where( array('YONETICI_KULLANICI_ADI', 'YONETICI_SIFRE') , array($P->get('username'), Encrypt::Md5( $P->get('password' ) ) ) );
		$DB->select();
		$result = $DB->result();
		
		if ( empty($result) ) {
			return false;
		}
		
		$Admin_Sess['UserName']     = $result[0]['YONETICI_KULLANICI_ADI'];
		$Admin_Sess['Name']         = $result[0]['YONETICI_ADI'];
		$Admin_Sess['Email']        = $result[0]['YONETICI_MAIL_ADRESI'];
		$Admin_Sess['Phone']        = $result[0]['YONETICI_TELEFON_NUMARASI'];
		$Admin_Sess['CreationTime'] = Now;
		$Admin_Sess['EndTime']      = Now;
		$Admin_Sess['LANGUAGE']     = 'tr_TR';
		$Admin_Sess['LANGUAGE_ID']  = '10';

		$this->Sess->set('ADMINISTRATOR',$Admin_Sess);

	}

	public function logout()
	{
		 $this->Sess->destroy();
	}

	public function sess_control()
	{

		if( !$this->Sess->get( ('ADMINISTRATOR') ) ) {
			
			return false;
		}
		$AdminSess = $this->Sess->get( ('ADMINISTRATOR') );
		if (isset($AdminSess['UserName'])) {
			return true;
		}

	}


}
