<?php

/**
*
*
*/
class Admin_Database_Action
{
	// public $Db;
	public $P;

	function __construct()
	{
		$this->Db = new Db;
		$this->P  = new Post;
	}

	public function Asj_Read()
	{
		$ADTI = new Admin_Database_Tables_Information;
		$json = $ADTI->getJson( $this->P->get('pages') );
		$json = json_decode( $json );
		$json = json_decode( json_encode( $json ), true);
		return $json;
	}

	public function Ping_Control($Json='')
	{
		$Ping_Control = FALSE;
		if (isset($Json['Property']['Pingo'])) {
			$Ping_Control           = $Json['Property']['Pingo']['Status'];
		}
		return $Ping_Control;
	}

	public function Ping_Tables_Field_Name($Json='')
	{
		$Ping_Tables_Field_Name = '';
		if (isset($Json['Property']['Pingo'])) {
			$Ping_Tables_Field_Name = $Json['Property']['Pingo']['Tables_Field_Name'];
		}
		return $Ping_Tables_Field_Name;
	}


	public function insert()
	{
		$array          = array('action', 'pages', 'id');
		$dataEslestirme = $data  = $dataTranslate = array();
		$json           = $this->Asj_Read();

		// Ping Yapılacakmı diye bakılıyor. 
		$Ping_Control           = $this->Ping_Control($json);
		$Ping_Tables_Field_Name = $this->Ping_Tables_Field_Name($json);

		foreach ($this->P->allVariables as $key => $value) {

			if ( !in_array($key , $array) ) {


				if ('language' == substr($key, 0, 8) ) {	// Translate
					$valueExplode = explode('_' , $key);
					$newkey = '';
					foreach ($valueExplode as $k => $v) {

						if ( $k > 1 ) {
							$newkey .= $v.'_';
						}
					}
					$newkey = substr( $newkey, 0, strlen($newkey)-1 );
					switch ($json['Tables']['Main'][$key]['type'] ) {
						case 'editor':
							$value=$this->ckeditot_text($value);
						break;
						case 'pass':
							$value=Encrypt::Md5($value);
						break;
					}

					$dataTranslate[$valueExplode[1]][$newkey] = $value;

					if ( $Ping_Control ) {
						if ($Ping_Tables_Field_Name == $newkey) {
							$Ping_Url_Name[] = $value;
						}	
					}
						

				} else {	// Main ve Multi Select

					if (is_array($value)) {		// Multi Select Eşleştirme
						foreach ($value as $k => $v) {
							$dataEslestirme[$key]=$value;
							// $dataEslestirme[$key]['data'][]  = $v;
						}
					} else {
						
						switch ($json['Tables']['Main'][$key]['type'] ) {
							case 'editor':
								$value=$this->ckeditot_text($value);
							break;
							case 'pass':
								$value=Encrypt::Md5($value);
							break;
						}
						
						$data['key'][]  = $key;
						$data['data'][] = $value;
						
						if ( $Ping_Control ) {
							if ($Ping_Tables_Field_Name == $key) {
								$Ping_Url_Name[] = $value;
							}	
						}

					}


				} // if langıuage

			} // if in_array

		} // foreach
		
		
		$table = strtoupper($this->P->get('pages'));

		$data['key'][]  = 'ISER_ID_EKLEYEN';
		$data['data'][] = Config::UserId();
		$data['key'][]  = 'EKLENME_TARIHI';
		$data['data'][] = Config::SqlNow();

		$this->Db->table( $table );
		$this->Db->data($data['key'], $data['data']);
		$this->Db->insert();
		// prp($this->Db->lastQuery());
		$lastInsertId = $this->Db->lastInsertId();
		if ( !empty($dataTranslate) ) {

			foreach ($dataTranslate as $key => $value) {

				$dataT           = array();
				$dataT['key'][]  = 'MAIN_ID';
				$dataT['data'][] = $lastInsertId;
				$dataT['key'][]  = 'LANGUAGE_ID';
				$dataT['data'][] = $key;

				foreach ($value as $k => $v) {
					$dataT['key'][]  =  $k;
					$dataT['data'][] = $v;
				}

				$this->Db->table( $table.'_TRANSLATE' );
				$this->Db->data($dataT['key'], $dataT['data']);
				$this->Db->insert();
				// prp($this->Db);

			}
		}

		if ( !empty($dataEslestirme) ) { // EŞLEŞTİRME VARSA

			foreach ($dataEslestirme as $key => $value) {
				$db_insert_table = $json['Tables']['MultiSelect'][$key]['insert_table'];
				$dataT           = array();
				$this->Db->multi( true ); // MULTİ İNSERT UYAPIYOR.
				$dataT['key']  = array('MAIN_ID' , $key);
				foreach ($value as $k => $v) {

					$dataT['data'][] = array($lastInsertId , $v);

				}
				$this->Db->table( $db_insert_table );
				$this->Db->data($dataT['key'], $dataT['data']);
				$this->Db->insert();
				// prp( $this->Db );
				// BAKILACAK $this->Db->Execute();

			}
		}
		$this->CacheControl( $json );
		
		if ( $Ping_Control ) {
			$pingo = new Pingo;

			$Site_Url = $json['Property']['SiteUrl'];
			foreach ($Ping_Url_Name as $key => $value) {
				$url      = Config::UrlCreate($Site_Url , $lastInsertId , $value);
				$pingo->Data( $url , $value );
				$pingo->Ping();
				
				
			}
		}

		return $lastInsertId;
	}

	public function update()
	{
		$array          = array('action', 'pages', 'id');
		$dataEslestirme = $data  = $dataTranslate = array();
		$json           = $this->Asj_Read();
		// Ping Yapılacakmı diye bakılıyor. 
		$Ping_Control           = $this->Ping_Control($json);
		$Ping_Tables_Field_Name = $this->Ping_Tables_Field_Name($json);
		
		foreach ($this->P->allVariables as $key => $value) {

			if ( !in_array($key , $array) ) {

				// Translate
				if ('language' == substr($key, 0, 8) ) {
					$valueExplode = explode('_' , $key);
					$newkey = '';
					foreach ($valueExplode as $k => $v) {

						if ( $k > 1 ) {
							$newkey .= $v.'_';
						}
					}
					$newkey = substr( $newkey, 0, strlen($newkey)-1 );

					switch ($json['Tables']['Main'][$key]['type'] ) {
						case 'editor':
							$value=$this->ckeditot_text($value);
						break;
						case 'pass':
							$value=Encrypt::Md5($value);
						break;
					}

					if ( $Ping_Control ) {
						if ($Ping_Tables_Field_Name == $newkey) {
							$Ping_Url_Name[] = $value;
						}	
					}

					$dataTranslate[$valueExplode[1]][$newkey] = $value;

				} else { // Main

					if (is_array($value)) {		// Multi Select Eşleştirme
						foreach ($value as $k => $v) {
							$dataEslestirme[$key]=$value;
							// $dataEslestirme[$key]['data'][]  = $v;
						}
					} else {
						switch ($json['Tables']['Main'][$key]['type'] ) {
							case 'editor':
								$value=$this->ckeditot_text($value);
							break;
							case 'pass':
								$value=Encrypt::Md5($value);
							break;
						}
						$data['key'][]  = $key;
						$data['data'][] = $value;

						if ( $Ping_Control ) {
							if ($Ping_Tables_Field_Name == $key) {
								$Ping_Url_Name[] = $value;
							}	
						}
					}
				}
			}
		}

		$table = strtoupper($this->P->get('pages'));

		$data['key'][]  = 'USER_ID_GUNCELLEYEN';
		$data['data'][] = Config::UserId();
		$data['key'][]  = 'GUNCELLEME_TARIHI';
		$data['data'][] = Config::SqlNow();

		$this->Db->data($data['key'], $data['data']);
		$this->Db->where( 'ID' , $this->P->get('id') );
		$this->Db->table( $table );
		$this->Db->update();
	
		if ( !empty($dataTranslate) ) {

			foreach ($dataTranslate as $key => $value) {

				$dataT = array();

				foreach ($value as $k => $v) {
					$dataT['key'][]  =  $k;
					$dataT['data'][] = $v;
				}

				$this->Db->data($dataT['key'], $dataT['data']);
				$this->Db->where( array('MAIN_ID','LANGUAGE_ID' ), array($this->P->get('id'),  $key) );

				$this->Db->table( $table.'_TRANSLATE' );
				$this->Db->update();

			}
		}

		if ( !empty($dataEslestirme) ) { // EŞLEŞTİRME VARSA


			foreach ($dataEslestirme as $key => $value) {
				$db_insert_table = $json['Tables']['MultiSelect'][$key]['insert_table'];

				$this->Db->where( 'MAIN_ID' , $this->P->get('id') );
				$this->Db->table( $db_insert_table );
				$this->Db->delete();

				$dataT           = array();
				$this->Db->multi( true ); // MULTİ İNSERT UYAPIYOR.
				$dataT['key']  = array('MAIN_ID' , $key);

				foreach ($value as $k => $v) {

					$dataT['data'][] = array($this->P->get('id') , $v);

				}
				$this->Db->data($dataT['key'], $dataT['data']);
				$this->Db->table( $db_insert_table );
				$this->Db->insert();
				$this->Db->Execute();
			}
		}

		$this->CacheControl( $json );

		if ( $Ping_Control ) {
			$pingo = new Pingo;

			$Site_Url = $json['Property']['SiteUrl'];
			foreach ($Ping_Url_Name as $key => $value) {
				$url      = Config::UrlCreate($Site_Url , $this->P->get('id') , $value);
				$pingo->Data( $url , $value );
				$pingo->Ping();
				
				
			}
		}
		
	}

	public function CacheControl( $json = '' , $table_name = '' )
	{
		
		$table_name = empty($table_name) ? $this->P->get('pages') : $table_name;
		if ( isset($json['Property']['Cache']) ) {
			if ($json['Property']['Cache']) {
				$Cache_Type  = isset($json['Property']['Cache_Type']) ? $json['Property']['Cache_Type'] : 'Multi';				
				$cache_table = new Cache_Tables;
				
				if ($json['Property']['Table'] == 'Both' OR $json['Property']['Table'] == 'Translate') {
					$cache_table->translate( TRUE );
				}

				$cache_table->type( $Cache_Type );
				$cache_table->table( $table_name );
			}
		}
	}

	public function delete()
	{
		$table = strtoupper( $this->P->get('pages') );

		// Main Table
		$this->Db->table( $table );
		$this->Db->where('ID' , $this->P->get('id') );
		$this->Db->delete();

		// Translate Table
		$this->Db->table( $table.'_TRANSLATE' );
		$this->Db->where('MAIN_ID' , $this->P->get('id') );
		$this->Db->delete();

		// Upload Table
		$this->Db->table( $table.'_UPLOAD' );
		$this->Db->where('MAIN_ID' , $this->P->get('id') );
		$this->Db->delete();

		// Category Table
		$this->Db->table( $table.'_SUB' );
		$this->Db->where('MAIN_ID' , $this->P->get('id') );
		$this->Db->delete();

		// Cache Update
		$json = $this->Asj_Read();
		$this->CacheControl( $json );

	}
	function ckeditot_text($text)
	{

		$order		= array("\\r\\n", "\\n", "\\r", "<p>&nbsp;</p>");
		$replace	= array(" ", " ", " ", "");
		$text		= str_replace('\\','',$text);
		$text		= str_replace('"','\"',$text);
		
		$value		= str_replace($order, $replace, $text); 
		return $value;
		
	}

}