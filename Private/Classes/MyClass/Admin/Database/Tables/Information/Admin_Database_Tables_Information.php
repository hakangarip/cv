<?php

/**
*  Sistemdeki Database bilgileri buradan okunuyor.
*/
class Admin_Database_Tables_Information {


	public function tables()
	{
		$tables[] = array('Main'=>'LANGUAGES' , 'Translate' =>'');
		$tables[] = array('Main'=>'ETKINLIK_TURU' , 'Translate' =>'ETKINLIK_TURU_TRANSLATE');
		$tables[] = array('Main'=>'ETKINLIKLER' , 'Translate' =>'ETKINLIKLER_TRANSLATE');
		$tables[] = array('Main'=>'KISILER' , 'Translate' =>'');
		$tables[] = array('Main'=>'KONU' , 'Translate' =>'KONU_TRANSLATE');
		$tables[] = array('Main'=>'DOKUMAN' , 'Translate' =>'DOKUMAN_TRANSLATE');
		$tables[] = array('Main'=>'DOKUMAN_DIL' , 'Translate' =>'DOKUMAN_DIL_TRANSLATE');
		$tables[] = array('Main'=>'DOKUMAN_ETKINLIKLER' , 'Translate' =>'');
		$tables[] = array('Main'=>'DOKUMAN_GORSEL' , 'Translate' =>'DOKUMAN_GORSEL_TRANSLATE');
		$tables[] = array('Main'=>'DOKUMAN_KISILER' , 'Translate' =>'');
		$tables[] = array('Main'=>'DOKUMAN_KONU' , 'Translate' =>'');
		$tables[] = array('Main'=>'DOKUMAN_TUR' , 'Translate' =>'DOKUMAN_TUR_TRANSLATE');

		return $tables;
	}

	public function table( $tableName = '')
	{
		$tableName = strtoupper($tableName);
		$DIL                      = array('Dil_Id' => array('Type'=>'int','Values' => array()),'Dil_Name' => array('Type'=>'char','Values' => array()),'Dil_Code' => array('Type'=>'char','Values' => array()),'Dil_Direction' => array('Type'=>'char','Values' => array()),);
		$ETKINLIK_TURU            = array('E_Tur_Id' => array('Type'=>'int','Values' => array()),'E_Tur_User_Id_Ekleyen' => array('Type'=>'tinyint','Values' => array()),'E_Tur_User_Id_Guncelleyen' => array('Type'=>'tinyint','Values' => array()),'E_Tur_Eklenme_Tarihi' => array('Type'=>'datetime','Values' => array()),'E_Tur_Guncelleme_Tarihi' => array('Type'=>'datetime','Values' => array()),);
		$ETKINLIK_TURU_TRANSLATE  = array('E_T_T_Id' => array('Type'=>'int','Values' => array()),'E_T_T_Main_Id' => array('Type'=>'int','Values' => array()),'E_T_T_Dil_Id' => array('Type'=>'int','Values' => array()),'E_T_T_Adi' => array('Type'=>'varchar','Values' => array()),);
		$ETKINLIKLER              = array('Etkinlikler_Id' => array('Type'=>'int','Values' => array()),'Etkinlikler_User_Id_Ekleyen' => array('Type'=>'tinyint','Values' => array()),'Etkinlikler_User_Id_Guncelleyen' => array('Type'=>'tinyint','Values' => array()),'Etkinlikler_Eklenme_Tarihi' => array('Type'=>'datetime','Values' => array()),'Etkinlikler_Guncelleme_Tarihi' => array('Type'=>'datetime','Values' => array()),'Etkinlikler_Baslangic_Tarihi' => array('Type'=>'datetime','Values' => array()),'Etkinlikler_Bitis_Tarihi' => array('Type'=>'datetime','Values' => array()),'Etkinlikler_E_Turu_Id' => array('Type'=>'tinyint','Values' => array()),'Etkinlikler_Yeri' => array('Type'=>'varchar','Values' => array()),);
		$ETKINLIKLER_TRANSLATE    = array('E_T_Id' => array('Type'=>'int','Values' => array()),'E_T_Dil_Id' => array('Type'=>'int','Values' => array()),'E_T_Adi' => array('Type'=>'varchar','Values' => array()),'E_T_Aciklama' => array('Type'=>'text','Values' => array()),'E_T_Main_Id' => array('Type'=>'int','Values' => array()),);
		$KISILER                  = array('Kisiler_Id' => array('Type'=>'int','Values' => array()),'Kisiler_Adi' => array('Type'=>'varchar','Values' => array()),'Kisiler_User_Id_Ekleyen' => array('Type'=>'tinyint','Values' => array()),'Kisiler_User_Id_Guncelleyen' => array('Type'=>'tinyint','Values' => array()),'Kisiler_Eklenme_Tarihi' => array('Type'=>'datetime','Values' => array()),'Kisiler_Guncelleme_Tarihi' => array('Type'=>'datetime','Values' => array()),);
		$KONU                     = array('Konu_Id' => array('Type'=>'int','Values' => array()),'Konu_User_Id_Ekleyen' => array('Type'=>'tinyint','Values' => array()),'Konu_User_Id_Guncelleyen' => array('Type'=>'tinyint','Values' => array()),'Konu_Eklenme_Tarihi' => array('Type'=>'datetime','Values' => array()),'Konu_Guncelleme_Tarihi' => array('Type'=>'datetime','Values' => array()),'Konu_Sub_Id' => array('Type'=>'int','Values' => array()),);
		$KONU_TRANSLATE           = array('K_T_Id' => array('Type'=>'int','Values' => array()),'K_T_Dil_Id' => array('Type'=>'int','Values' => array()),'K_T_Adi' => array('Type'=>'varchar','Values' => array()),'K_T_Main_Id' => array('Type'=>'int','Values' => array()),);
		$MALZEME                  = array('Malzeme_Id' => array('Type'=>'int','Values' => array()),'Malzeme_Status' => array('Type'=>'enum','Values' => array('True','False')),'Malzeme_Imza' => array('Type'=>'enum','Values' => array('IMZALI','IMZASIZ')),'Malzeme_Orjinal' => array('Type'=>'enum','Values' => array('ORJINAL','FOTOKOPI','TIPKI_BASIM')),'Malzeme_Ozellik' => array('Type'=>'enum','Values' => array('ELEKTRONIK','BASILI','EL_YAZISI')),'Malzeme_Gorsel' => array('Type'=>'enum','Values' => array('VAR','YOK')),'Malzeme_Renk' => array('Type'=>'enum','Values' => array('RENKLI','SIYAH_BEYAZ')),'Malzeme_Boyut' => array('Type'=>'char','Values' => array()),'Malzeme_Sayfa_Sayisi' => array('Type'=>'smallint','Values' => array()),'Malzeme_Kopya_Sayisi' => array('Type'=>'smallint','Values' => array()),'Malzeme_Eklenme_Tarihi' => array('Type'=>'datetime','Values' => array()),'Malzeme_Guncelleme_Tarihi' => array('Type'=>'datetime','Values' => array()),'Malzeme_User_Id_Ekleyen' => array('Type'=>'tinyint','Values' => array()),'Malzeme_User_Id_Guncelleyen' => array('Type'=>'tinyint','Values' => array()),'Malzeme_Tur_Id' => array('Type'=>'tinyint','Values' => array()),'Malzeme_Mdil_Id' => array('Type'=>'tinyint','Values' => array()),);
		$MALZEME_TRANSLATE        = array('M_T_Id' => array('Type'=>'int','Values' => array()),'M_T_Dil_Id' => array('Type'=>'int','Values' => array()),'M_T_Adi' => array('Type'=>'varchar','Values' => array()),'M_T_Not' => array('Type'=>'varchar','Values' => array()),'M_T_Aciklama' => array('Type'=>'text','Values' => array()),'M_T_Kunye' => array('Type'=>'varchar','Values' => array()),'M_T_Main_Id' => array('Type'=>'int','Values' => array()),);
		$MALZEME_DIL              = array('M_Dil_Id' => array('Type'=>'tinyint','Values' => array()),'M_Dil_User_Id_Ekleyen' => array('Type'=>'tinyint','Values' => array()),'M_Dil_User_Id_Guncelleyen' => array('Type'=>'tinyint','Values' => array()),'M_Dil_Eklenme_Tarihi' => array('Type'=>'datetime','Values' => array()),'M_Dil_Guncelleme_Tarihi' => array('Type'=>'datetime','Values' => array()),);
		$MALZEME_DIL_TRANSLATE    = array('M_D_T_Id' => array('Type'=>'int','Values' => array()),'M_D_T_Dil_Id' => array('Type'=>'int','Values' => array()),'M_D_T_Adi' => array('Type'=>'varchar','Values' => array()),'M_D_T_Main_Id' => array('Type'=>'int','Values' => array()),);
		$MALZEME_ETKINLIKLER      = array('M_Etkinlikler_Id' => array('Type'=>'int','Values' => array()),'M_Etkinlikler_Malzeme_Id' => array('Type'=>'int','Values' => array()),'M_Etkinlikler_Etkinlikler_Id' => array('Type'=>'int','Values' => array()),);
		$MALZEME_GORSEL           = array('M_Gorsel_Id' => array('Type'=>'int','Values' => array()),'M_Malzeme_Id' => array('Type'=>'int','Values' => array()),'M_Gosel_Eklenme_Tarihi' => array('Type'=>'datetime','Values' => array()),'M_Gorsel_User_Id_Ekleyen' => array('Type'=>'tinyint','Values' => array()),'M_Gorsel_Turu' => array('Type'=>'char','Values' => array()),'M_Gorsel_Yolu' => array('Type'=>'char','Values' => array()),);
		$MALZEME_GORSEL_TRANSLATE = array('M_G_T_Id' => array('Type'=>'int','Values' => array()),'M_G_T_Dil_Id' => array('Type'=>'int','Values' => array()),'M_G_T_Adi' => array('Type'=>'varchar','Values' => array()),'M_G_T_Main_Id' => array('Type'=>'int','Values' => array()),);
		$MALZEME_KISILER          = array('M_Kisiler_Id' => array('Type'=>'int','Values' => array()),'M_Kisiler_Malzeme_Id' => array('Type'=>'int','Values' => array()),'M_Kisiler_Kisiler_Id' => array('Type'=>'int','Values' => array()),);
		$MALZEME_KONU             = array('M_Konu_Id' => array('Type'=>'int','Values' => array()),'M_Konu_Malzeme_Id' => array('Type'=>'int','Values' => array()),'M_Konu_Konu_Id' => array('Type'=>'int','Values' => array()),);
		$MALZEME_TUR              = array('M_Tur_Id' => array('Type'=>'int','Values' => array()),'M_Tur_User_Id_Ekleyen' => array('Type'=>'tinyint','Values' => array()),'M_Tur_User_Id_Guncelleyen' => array('Type'=>'tinyint','Values' => array()),'M_Tur_Eklenme_Tarihi' => array('Type'=>'datetime','Values' => array()),'M_Tur_Guncelleme_Tarihi' => array('Type'=>'datetime','Values' => array()),);
		$MALZEME_TUR_TRANSLATE    = array('M_T_T_Id' => array('Type'=>'int','Values' => array()),'M_T_T_Dil_Id' => array('Type'=>'int','Values' => array()),'M_T_T_Adi' => array('Type'=>'varchar','Values' => array()),'M_T_T_Main_Id' => array('Type'=>'int','Values' => array()),);

		return  $$tableName;

	}

	public function getJson($Name='')
	{
		$File = new File;

		$Name = strtoupper($Name).'.asj';
		$File->setPath( Config::Asj_Folder() ) ;
		$return = $File->read($Name);
		if ( !$return ) { // Dosya Okunamamışsa
			return false;
		}

		return $return;

	}

	/**
	 * tableCreate
	 * Database den Tablolardaki Alanların Tiplerini ve degerlerini
	 * Biz diziye aktarıyor.
	 * @param  string $tableName Eğer bir tablonun değerini istiyor isek.
	 * @return string
	 */
	public function tableCreate( $tableName='' )
	{
		$db = new Db;
		$field ='';
		$return = array();

		foreach ($this->tables() as $k => $v) {
			foreach ($v as $key => $value) {

				if ( !empty($tableName) ) {
					if ( $tableName !=  $value ) {
						continue;
					}
				}

				$field          .= '$'.$value. ' = array(';
				$db->Execute('SHOW FULL COLUMNS FROM '.$value.'');
				$return[$value] = $db->result();

				foreach ($db->result() as $k => $v) {
					$enum_deger = '';
					$type       =  $v['Type'];
					if ($v['Type'] <> 'datetime') {
						$type_Explode = explode('(' , $v['Type']) ;
						$type         = $type_Explode[0];

						if ( $type == 'enum' ) {
							$enum_deger = str_replace(')', '', $type_Explode[1]);
						}

					}
					$return['Tables'][$key][$v['Field']]['type'] = '';
					$return['Tables'][$key][$v['Field']]['grid_status'] = '';
					$return['Tables'][$key][$v['Field']]['status'] = '';
					$return['Tables'][$key][$v['Field']]['name'] = '';
					
					$field .= "'".$v['Field']."' => array('Type'=>'".$type."','Values' => array(".$enum_deger.")),";
				}

			}

			$field.= ');';
		}
		return  $field;
		return  $return;
	}

}
