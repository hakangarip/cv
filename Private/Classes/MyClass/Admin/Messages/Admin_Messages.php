<?php 

/**
* 
*/
class Admin_Messages
{
	
	public static function set( $type='' ,  $messages = '')
	{
		$Sess = new Sess;
		Admin_Messages::destroy();
		$Sess->set(Config::Sess('Admin'), array('Messages'=>array( $type => $messages)) );
	}	

	public static  function get()
	{
		$Sess = new Sess;
		$Messages= '';
		$Return_Message = $Sess->get(Config::Sess('Admin'));
		if (! isset($Return_Message['Messages']) ) {
			return false;
		}
		$Return_Message = $Return_Message['Messages'];
		foreach ($Return_Message as $key => $value) {
			$message = Config::FormMessages($key);

			$message = str_replace('[*MESSAGE*]', $value, $message);
			$Messages .= $message;
		}
		Admin_Messages::destroy();
		return $Messages;
	}

	public static function destroy()
	{
		$Sess = new Sess;
		$Sess->destroy(Config::Sess('Admin') , 'Messages');
	}
}