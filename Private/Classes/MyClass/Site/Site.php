<?php

/**
*
*
*/
class Site
{

	public $Db;
	public $Sess;
	public $TranslateControl = false;
	public $UploadControl    = false;
	public $StatusControl    = true;
	public $whereIn          = '';

	function __construct()
	{
		include_class( get_class() );
		$this->Db   = new Db;

		$this->setTranslate();
		$this->setUpload();
	}

	public function setTranslate($translate = false)
	{
		$this->TranslateControl = $translate;
	}

	public function setUpload($upload = false)
	{
		$this->UploadControl  = $upload;
	}

	public function setStatusControl( $status = true )
	{
		$this->StatusControl = $status;
	}

	public function setWhereIn($key='' , $value = '')
	{
		$arrayWhere['key'] = $key;
		$arrayWhere['val'] = $value;
		$this->whereIn = $arrayWhere;
	}

	public function get($table_name = '' , $select_field_name = '',  $select_value = '')
	{
		$arrayWhere = array();

		if ( empty($table_name) ) {
			return false;
		}

		$this->Db->table($table_name);

		if ( !empty($select_value) ) {
			if (is_array($select_field_name) and is_array($select_value)) {
				foreach ($select_field_name as $key => $value) {
					$arrayWhere['key'][] = $value;
					$arrayWhere['val'][] = $select_value[$key];
				}
			} else  {
				$arrayWhere['key'][] = $select_field_name;
				$arrayWhere['val'][] = $select_value;
			}
			// $this->Db->where($select_field_name , $select_value );
		}

		if ($this->StatusControl) {
			$arrayWhere['key'][] = $table_name.'.STATUS';
			$arrayWhere['val'][] = 'TRUE';
		}

		if ($this->TranslateControl) { // eğer dilli çekilecek ise
			$arrayWhere['key'][] = $table_name.'_TRANSLATE.LANGUAGE_ID';
			$arrayWhere['val'][] = Config::LanguageId();
			$this->Db->join( $table_name.'_TRANSLATE', $table_name.'_TRANSLATE.MAIN_ID = ' .$table_name.'.ID' );
		}

		if ($this->UploadControl) { // eğer dilli çekilecek ise
			$arrayWhere['key'][] = $table_name.'_UPLOAD.U_SELECTED';
			$arrayWhere['val'][] = '1';
			$this->Db->join( $table_name.'_UPLOAD', $table_name.'_UPLOAD.MAIN_ID = ' .$table_name.'.ID' ,'LEFT');
		}

		if (!empty($arrayWhere )) {
			$this->Db->where( $arrayWhere['key'] , $arrayWhere['val'] );
		}

		if ( !empty($this->whereIn) ) {
			$this->Db->whereIn('', $this->whereIn['key'] , $this->whereIn['val'] );
		}

		$this->Db->select();
		$return = $this->Db->result();
		// prpe($this->Db->lastQuery());
		$this->clear();
		return $return;
	}

	public function getUpload( $table_name = '' , $select_field_name = '',  $select_value = '' )
	{
		if ( empty($table_name) ) {
			return false;
		}

		$this->Db->table($table_name.'_UPLOAD');
		if ( !empty($select_value) ) {
			$this->Db->where($select_field_name , $select_value );
		}

		$this->Db->select();
		$return = $this->Db->result();
		// prpe($this->Db->lastQuery());
		return $return;
	}


	public function clear()
	{
		$this->setTranslate();
		$this->setUpload();
		$this->setStatusControl();
		$this->whereIn = '';
	}


}
