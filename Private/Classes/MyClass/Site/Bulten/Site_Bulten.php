<?php 

/**
* 
*/
class Site_Bulten
{
	function __construct()
	{
		include_class( get_class() );
	}
	
	public function Create($email = '' , $user_id = '')
	{
		$Valid = new Valid;
		if ( $Valid->email($email) ) {

			$DB   = new Db;
			$Sess = new Sess;
			$Sess->destroy('BULTEN_ERROR');

			$data['key'][]  = 'BULTEN_EMAIL';
			$data['data'][] = $email;
			$DB->where($data['key'], $data['data']);
			$DB->table( 'BULTEN' );
			$DB->select();
			$select = $DB->result();
			// prpe($select);
			// prpe($DB->lastQuery());
			if ( count($select) > 0 ) {
				$Sess->set('BULTEN_ERROR',array('ALERT' => L10n::T('KAYITLI BULTEN ADRESİ') ) );
				return FALSE;
			}
			$data = array();
			if ( !empty($user_id) ) {
				$data['key'][]  = 'USER_ID';
				$data['data'][] = $user_id;
				
			}
			$data['key'][]  = 'BULTEN_EMAIL';
			$data['data'][] = $email;
		
			$DB->data($data['key'], $data['data']);
			$DB->table( 'BULTEN' );
			$DB->insert();
			// prpe($DB->lastQuery());
			return true;

		} else{
			$Sess->set('BULTEN_ERROR',array('ALERT' => L10n::T('Geçerli Email Adresi Giriniz') ) );
		}
		return FALSE;
	}
	
}
