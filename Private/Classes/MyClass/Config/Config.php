<?php
/**
 *
 */
class Config
{
	public static function  LIVE()
	{
		return LIVE;
	}
	/**
	 * MYSQL CONFIG SETTINGS
	 */
	public static function Db()
	{
		$db_config                              = array();
	
		$db_config['Mysql']['ServerName']       = ServerName;
		$db_config['Mysql']['DatabaseName']     = DatabaseName;
		$db_config['Mysql']['DatabaseUser']     = DatabaseUser;
		$db_config['Mysql']['DatabasePassword'] = DatabasePassword;
		$db_config['Mysql']['DatabasePort']     = DatabasePort;
		$db_config['Mysql']['CharacterSet']     = CharacterSet;
		$db_config['Mysql']['Collation']        = Collation;
		$db_config['Mysql']['Prefix']           = Prefix;

		return $db_config;
	}

	public  static function DatabaseDriver()
	{
		return 'Mysql';
	}
	public static function TemplateFolderName()
	{
		return TEMPLATE_FOLDER_NAME;
	}
	public static function TemplateSubClassName()
	{
		return TEMPLATE_SUB_CLASS_NAME;
	}

	public static function Memcache_Prefix()
	{
		return MEMCACHE_PREFIX;
	}
 
	// public static function CacheDriver()
	// {
	// 	return CacheDriver;
	// }

	public static function Ping_Control()
	{
		if ( ! Config::LIVE() ) {
			return 'FALSE';
		}
		return Config::Cache('GENEL_AYARLAR','PING_CONTROL');

	}

	public static function Ftp( $select = '' ){

		$FtpServer   = 'ftp.arti-sanat.com';
		$FtpUser     = 'arti-sanat.com';
		$FtpPassword = 'Merve44255266';
		$FtpPort     = '21';
		$FtpPath     = '/httpdocs/Projects/Kitap/Upload';
		$FtpKip      = FTP_ASCII;

		return  $$select;
	}

	public static function UserIp()
	{
		return  $_SERVER['REMOTE_ADDR'];
	}

	public static function User_Kredi()
	{
		// İlk başka kredi şeklinde yapılmıştı sonra fikir değişitirilip normal kredi kartına dönünce iptal edildi. 
		if (!Config::UserId()) {
			return 0;
		}
		$db = new Db;
		$db->where('USER_ID' , Config::UserId());
		$db->table('USERS_KALAN_KREDI');
		$db->select();
		$return =  $db->result();
		if (empty($return)) {
			return 0;
		}
		return $return[0]['KALAN_KREDI'];
	}

	public static function DefaultPage()
	{
		return 'INDEX';
	}

	public static function Mandrill_Key()
	{
		return 'bs0ybo719PvLBf-OO2sBpg';

	}

	public static function Mail_Stype()
	{
		return Mail_Send_Type;
	}

	public static function EmptyImageName($value='')
	{
		$ImagesPath = array(
				'269' =>Config::SitePath('img').'269x269.png',
				'370' =>Config::SitePath('img').'370x370.png'
			);
		return $ImagesPath[$value];
	}

	public  static function RealImgPath($tableName = '',$FolderName='' , $imgName = '' , $empty_images_name = '' , $Session_Control = true )
	{
		$Users = new Users;
		$Folder_Ek= '';
		if ( $Users->LoginValid()=='TRUE' AND $Session_Control == true  ) {
			$Folder_Ek = '_NB';
		}

		if ( empty($imgName ) ) {
			return $empty_images_name;
		}
		$FolderName = Config::UploadFolder($FolderName);
		$ServerPath  = DR.SLASH. 'Upload' .SLASH. $tableName .SLASH. $FolderName .$Folder_Ek.SLASH. $imgName;
		if (!file_exists($ServerPath)) {
			return $empty_images_name;				
		}
		$images = Config::SitePath() . 'Upload' .SLASH. $tableName .SLASH. $FolderName .$Folder_Ek.SLASH. $imgName;
		return $images;
	}

	public static function OrjinalPath( $tableName='' ,$imgName ='' )
	{
		$FolderName = Config::UploadFolder('ORIGINAL');

		return DR . SLASH. 'Upload' .SLASH. $tableName .SLASH. $FolderName .SLASH. $imgName;	
	}

	public static function Sepet_Script_Create($type = '', $urun_id = '', $adet = '' ,$boyutu = 'ORJINAL')
	{
		$type= strtoupper($type) ;
		switch ($type) {
			case 'ADD':
				return "sepet('$urun_id','$adet','ADD','$boyutu')";
				break;
			case 'UPDATE':
				return "sepet('$urun_id','$adet','UPDATE','$boyutu')";
				
				break;
			case 'REMOVE':
				return "sepet_remove('$urun_id','reload','$boyutu')";
				break;
			case 'REMOVE_RELOAD':
				return "sepet_remove('$urun_id','reload','$boyutu')";
				break;
			case 'REMOVE_ALL';
				$sepet->Remove_All();
				return "REMOVE_ALL";

		}
	}

	public static function Favori_Script_Create($type = '', $urun_id = '')
	{
		$type= strtoupper($type) ;
		switch ($type) {
			case 'ADD':
				return "favori_islemleri('$urun_id','ADD')";
				break;
			case 'UPDATE':
				return "favori_islemleri('$urun_id','UPDATE')";
				
				break;
			case 'REMOVE':
				return "favori_islemleri('$urun_id')";
				break;
			case 'REMOVE_RELOAD':
				return "favori_islemleri('$urun_id','reload')";
				break;
			case 'REMOVE_ALL';
				$sepet->Remove_All();
				return "REMOVE_ALL";

		}
	}

	public static  function Preparing_To_Download_Image( $Image = '' , $newImage = '' , $ImageText = '' , $Steganography_Text = '')
	{
		// $NewImagePath = DR . SLASH. 'Upload' . SLASH . 'TMP' . SLASH . Config::UserSess('ID').'_'.Encrypt::Hash(Now). '.jpg';
		$NewImagePath = $newImage;
        list($waterX ,$waterY ,$Type) = getimagesize( $Image );
	 	$rs   = new Image_Resize;
        $forY = $waterY / 3;
        $forX = $waterX / 3;
        $i=1 ;
	  	// $x = 10;
	  	
	  	 if ($waterY > 2000) {
	  	 	$fontsize = $waterY / 45;
	  	 } elseif ($waterY > 1000 AND $waterY < 2001)  {
	  	 	$fontsize = $waterY / 55;
	  	 } elseif ($waterY > 500 AND $waterY < 1001)  {
	  	 		$fontsize = $waterY /50;
	  	 } else {
	  	 		$fontsize = $waterY / 30;
	  	 
	  	 }
	  	
	  	for ($x=0; $x < $waterX; $x = $x + $forX) {     
                for ($y=0; $y < $waterY+ $forY ; $y = $y + $forY) { 
                    $oldimagepath = ($i==1) ? $Image : $NewImagePath;
                    $rs->ImagePath($oldimagepath );	
                    $rs->NewImagePath($NewImagePath );
                    $rs->NewWidth($waterX);
            		$rs->NewHeight($waterY);
                    $rs->Quality(100);
                    $rs->WaterMarkText( $ImageText  , $x ,  $y);
                    $rs->Angle( 45 );
                    $rs->Alpha( 70 );
                    $rs->FontSize( $fontsize );
                    if ($i%3) {
                        $rs->color( '0','0','0' );
                    } else {
                        $rs->color( '255','255','255' );
                    }
                    $i++;
                    $rs->Resize();
                }
            }
        // $NewImagePathSteg = DR . SLASH. 'Upload' . SLASH . 'TMP' . SLASH . Config::UserSess('ID').'_'.'Hakan'. '.jpg';
        // $Stegan       = new StreamSteganography( $NewImagePath );
		// $Stegan->Write( 'HAKAN', $NewImagePathSteg );
        // unlink($NewImagePath);

	}

	public static function UploadSelectedSql($TableName='')
	{
		return '(SELECT '.$TableName.'_UPLOAD.U_NAME FROM '.$TableName.'_UPLOAD WHERE '.$TableName.'_UPLOAD.U_SELECTED = "1" AND '.$TableName.'_UPLOAD.MAIN_ID = '.$TableName.'.ID ) AS SELECTED_IMAGES';
	}

	public static function UploadFolder( $FolderName = '' )
	{

		switch ( strtoupper($FolderName) ) {
			case 'ORIGINAL':
				return IMAGES_ORIGINAL_PATH;
				break;
			case 'THUMBNAIL':
				return IMAGES_THUMBNAIL_PATH;
				break;
			case 'MIN':
				return IMAGES_MIN_PATH;
				break;
			case 'MAX':
				return IMAGES_MAX_PATH;
				break;
			case 'CKE':
				return CKE;
		}

		return UPLOAD_FOLDER;
	}

	public static function Sess($value='')
	{
		switch ($value) {
			case 'Admin':
					return SESS_ADMIN;
				break;
			case '':
			$Sess  = new Sess;
			$Sess->get(array('USER' , $value));
		}
	}

	public static function UserSess($value='')
	{
		
		$Sess  = new Sess;
		return $Sess->get(array('USER' , $value));
		
	}

	public static function Session_Name($value='')
	{
		switch ($value) {
			case 'SEPET':
					return 'SEPET';
				break;
			case '':
				return '';
		}
	}

	public static function SqlNow()
	{
		return Now;
	}

	public static function UserId($value='')
	{
		$Sess  = new Sess;
		$User = $Sess->get('USER');
		if (isset($User['ID'])) {

			return $User['ID'];
		}
		return FALSE;
	}

	public static function Rate($value='')
	{
		if (Config::Language() == 'tr_TR') {
			return  $value . ' TL';
		}
		$k = new Kur;
		return  '$ ' .  $k->exchange( $value , 'USD' );
	}

	public static function SitePath( $conf = '' )
	{

		switch ($conf) {
			case 'css':
				return SITE_PATH .ASSETS.'/css/';
				break;
			case 'js':
				return SITE_PATH .ASSETS. '/js/';
				break;
			case 'img':
				return SITE_PATH .ASSETS. '/img/';
				break;
			default:
				return SITE_PATH.'';
				break;
		}
	}

	public static function SiteIp()
	{
		return SITE_IP;
	}

	public static function SiteEmailName()
	{
		return 'Oto Analiz';
	}

	public static function SiteEmail()
	{
		return 'hgarip@otoanaliz.net';
	}

	public static function Cache( $Name = '', $return_name = '') {
		$Cache_Tables = new Cache_Tables;

		$RESULT_CACHE_TABLE = $Cache_Tables->get( $Name );
		

		if ( $RESULT_CACHE_TABLE  == false  OR empty($RESULT_CACHE_TABLE)) {
			
			$Cache_Tables->Create( $Name );
		}
		return $Cache_Tables->get($Name , $return_name);
	}

	public static function FiyatKontrol($key='' , $urun )
	{
		// foreach ($value as $k => $v) {
		// 	$key  = $k;
		// }
		
		switch ($key) {
			case 'MIN':
				$val = 'DOKUMAN_FIYATI_MIN';
				break;
			case 'MAX':
				$val = 'DOKUMAN_FIYATI_MAX';
				break;
			case 'ORJINAL':
				$val = 'DOKUMAN_FIYATI';
				break;
		}
		// prp($urun[$val]);
		
		$FIYATI = $urun[$val];
		
        // if ( !empty($value['DOKUMAN_ODEMEGRUBU_ID']) ){
        //   $FIYATI = Config::Cache('ODEMEGRUBU',$value['DOKUMAN_ODEMEGRUBU_ID']);
        //   $FIYATI = $FIYATI['ODEME'];
        // }
        
        return $FIYATI;
	}

	public static function Konu() {

		$Konu       = Config::Cache('KONU');
		
		$Konular    = $Konu[ Config::LanguageId() ];
		$Konu_Array = array();
		
		foreach ($Konular as $k => $v) {
		
			if ($v['KONU_SUB_ID'] == 0) {
				$Konu_Array[$v['ID']]['Name'] = $v['KT_ADI'];
			} else {
				$Konu_Array[$v['KONU_SUB_ID']]['SUB'][$v['ID']]['Name']   = $v['KT_ADI'];
				$Konu_Array[$v['KONU_SUB_ID']]['SUB'][$v['ID']]['TOP_ID'] = $v['KONU_SUB_ID'];
			}

		}
		return $Konu_Array;
	}

	public static function Yonetim( $conf = '' )
	{
		switch ($conf) {
			case 'css':
				return YONETIM_PATH . 'template/css/';
				break;
			case 'plugins':
				return YONETIM_PATH . 'template/plugins/';
				break;
			case 'js':
				return YONETIM_PATH . 'template/js/';
				break;
			case 'images':
				return YONETIM_PATH . 'template/images/';
				break;
			default:
				return YONETIM_PATH ;
				break;
		}
	}

	public static function Asj_Folder()
	{
		return ASJ_FOLDER;
	}

	public static function TraslateCache()
	{
		return TraslateCache;
	}
	public static function Language()
	{
		$Sess = new Sess;
		$LANGUAGE = $Sess->get('LANGUAGE') ; 
		if (!$LANGUAGE or empty($LANGUAGE) ) {
			$Cache_Table = new Cache_Tables;
			$Sess->set(array( 'LANGUAGE' => DEFAULT_LANGUAGE ) ) ;
		}
		return $Sess->get('LANGUAGE');
	}

	public static function LanguageId()
	{
		$Sess = new Sess;
		// prpe($Sess->allGet());
		$LANGUAGE_ID = $Sess->get('LANGUAGE_ID') ; 
		if (!$LANGUAGE_ID or empty($LANGUAGE_ID) ) {
			$Cache_Table = new Cache_Tables;
			$Sess->set(array( 'LANGUAGE_ID' => $Cache_Table->get('LANGUAGES' , 'LANGUAGE_CODE' , Config::Language() , 'ID') ) ) ;
		}
		return $Sess->get('LANGUAGE_ID');
	}

	public static function AdminPanelDefaultLanguage()
	{
		return ADMIN_PANEL_DEFAULT_LANGUAGE;
	}

	public static function HtmlLogControl()
	{
		return HtmlLog;
	}

	public static function UserSessEndTime()
	{
		return Now;
	}

	public static function HtmlLogOutPut()
	{
		global $htmlLog;
		switch ( Config::HtmlLogControl() ) {
        	case 1:
                echo $htmlLog->Output();
            break;
         	case 2:
                $htmlLog->Output();
            break;
   		}


	}

	public static function PagesHelp()
	{
		$Sayfa  = Config::Cache('SAYFA');
		$Sayfa  = $Sayfa[ Config::LanguageId() ];
		$return = '';
		foreach ($Sayfa as $key => $value) {
			$url = Config::UrlCreate('pages' , $value['ID'] , $value['ST_BASLIK']);
			$return.= ' <li><a href="'.$url.'">'. $value['ST_BASLIK'] .'</a></li>';
		}
		return $return;
	}

	public static function UrlCreate( $category=''  , $ID = ''  , $value = '' )
	{
		$name     = Config::UrlReplace($value);
		$location = Config::SitePath();

		if ( !empty($category) ) {
			$location .= $category;
		}
		if ( !empty($ID) ) {
			$location .= SLASH . $ID ;
		}

		if ( !empty($name) ) {
			$location .= SLASH . $name ;
		}
		return $location;

	}

	public static function Eslestirme($Table_Name = '' , $Main_Id = '' , $Select_Field = '')
	{
		$Db = new Db;
		$Db->table($Table_Name);
		$Db->where('Main_Id' , $Main_Id);
		$Db->select($Select_Field);
		return $Db->result();
	}

	/**
	 * Kullanılan Sayfalar
	 * SEARCH->View 
	 */
	
	public static function Pagination($Form_Name='' , $Pager_Name = '' , $Total_Record , $Page_Number,$Sayfadaki_Urun_Sayisi)
	{
		$SAYFALAMA_SAYFA_SAYISI  = 5 ;
		$SAYFALAMA_SAYFA_OTELEME = floor($SAYFALAMA_SAYFA_SAYISI / 2); 

		$return = '	
					  	<ul class="pagination">
					  		[#li_first#]
					  		[#li_prev#]
				  			[#li#]
				  			[#li_next#]
					        [#li_end#]
					  	</ul>
					  	';

		$Toplam_Sayfa = ceil( $Total_Record / $Sayfadaki_Urun_Sayisi );

		$li = '';
		$i_baslangic = 1;
		
		if ( $SAYFALAMA_SAYFA_SAYISI < $Toplam_Sayfa ) {
			$i_baslangic   = $Page_Number - $SAYFALAMA_SAYFA_OTELEME;
		} 

		if ( $Page_Number == $Toplam_Sayfa ) {
			$Page_Number = $Toplam_Sayfa ; 
			$i_baslangic = $Page_Number - $SAYFALAMA_SAYFA_SAYISI +1 ;
			$SAYFALAMA_I = $Page_Number  ;
		}
		

		$i_baslangic = ($i_baslangic < 1 ) ? 1 : $i_baslangic;
		$SAYFALAMA_I = $SAYFALAMA_SAYFA_SAYISI + $i_baslangic ;

		if ( $SAYFALAMA_I > $Toplam_Sayfa) {
			$SAYFALAMA_I =  $Toplam_Sayfa + 1 ;
		}
		if ( $Page_Number > $Toplam_Sayfa ) {
			$Page_Number = $Toplam_Sayfa + 1 ;
			$i_baslangic = $Page_Number - $SAYFALAMA_SAYFA_SAYISI  ;
			$SAYFALAMA_I = $Page_Number  ;
			$Page_Number = $Toplam_Sayfa;
		}

		for ($i=$i_baslangic ; $i < $SAYFALAMA_I ; $i++) { 
			
			$Onclick='';

			if ($i == $Page_Number) {
				$li .= '<li class="active"><span>'.$i.'</span></li>';
			} else {
				$li .= '<li ><a href="#" Onclick="Arama(\''.$Form_Name.'\' , \''.$Pager_Name.'\' ,\''.$i.'\' )">'.$i.'</a></li>';
			}

		}
		
		
		$page_prev = ( $Page_Number < 2 ) ? 1 : $Page_Number-1 ;
		$page_next = ( $Page_Number == $Toplam_Sayfa ) ? $Page_Number : $Page_Number+1 ;

		$return    = str_replace('[#li#]', $li, $return);
		$return    = str_replace('[#li_first#]', '<li><a href="#" Onclick="Arama(\''.$Form_Name.'\' , \''.$Pager_Name.'\' ,\'1\' )"><i class="fa fa-angle-double-left "></i></a></li>', $return);
		$return    = str_replace('[#li_prev#]', '<li><a href="#" Onclick="Arama(\''.$Form_Name.'\' , \''.$Pager_Name.'\' ,\''.$page_prev.'\' )"><i class="fa fa-angle-left "></i></a></li>', $return);
		$return    = str_replace('[#li_next#]', '<li><a href="#" Onclick="Arama(\''.$Form_Name.'\' , \''.$Pager_Name.'\' ,\''.$page_next.'\' )"><i class="fa fa-angle-right "></i></a></li>', $return);
		$return    = str_replace('[#li_end#]', '<li><a href="#" Onclick="Arama(\''.$Form_Name.'\' , \''.$Pager_Name.'\' ,\''.$Toplam_Sayfa.'\' )"><i class="fa fa-angle-double-right "></i></a></li>', $return);
		
		return $return;


	}

	public static function productDetailDateFormat($value='' , $start = 0 , $end = 10 , $replace = '.')
	{
		$bas_tar = substr($value,$start,$end) ;
		$bas_tar = str_replace('-', $replace, $bas_tar);
		return $bas_tar;
	}

	public static function User_Account_Dashboard_Menu($Select = '')
	{
		$my_account = $my_downloaded = $favorilerim = $my_orders = '';
		switch ($Select) {
			case 'my_account':
					$my_account  = 'active';
				break;
			case 'my_downloaded':
					$my_downloaded  = 'active';
				break;
			case 'favorilerim':
					$favorilerim  = 'active';
				break;
			case 'my_orders':
					$my_orders  = 'active';
				break;
			default:
				# code...
				break;
		}
		return '
				<li class="'.$my_account.'"> 
				<a href="'.Config::SitePath().'my_account' .'"><i class="fa fa-user item-icon"></i>'.L10n::T('Account Information') .'</a>
			  </li>
			  <li class="'.$my_downloaded.'">
				<a href="'.Config::SitePath().'my_downloaded' .'"><i class="fa fa-download item-icon"></i>'.L10n::T('İndirdiğim Dökümanlar') .'</a>
			  </li >
			   <li class="'.$favorilerim.'">
				<a href="'.Config::SitePath().'favorilerim' .'"><i class="fa fa-heart item-icon"></i>'.L10n::T('MyWishlist') .'</a>
			  </li >
			  <li class="'.$my_orders.'">
				<a href="'.Config::SitePath().'my_orders' .'"><i class="fa fa-shopping-cart item-icon"></i>'.L10n::T('My Orders') .'</a>
			  </li>
			  ';
	}

	public static function CategoryList( $value = '' )
	{
		$Sess  = new Sess;
		$SEPET = $Sess->get('SEPET');
		$SEPET_URUNLER = $SEPET['URUNLER'];
		$img        = Config::RealImgPath('DOKUMAN', 'Thumbnail' , $value['U_NAME'] , Config::EmptyImageName('269'));
		$url        = Config::UrlCreate('productview' , $value['ID'] , $value['DT_ADI']);
		// $FIYATI     = Config::FiyatKontrol($value);
		// $FIYATI     = Config::FiyatKontrol( $v, $value[$key] );
		$SepetEkle  = Config::Sepet_Script_Create('ADD',$value['ID'],1);
		$FavoriEkle = Config::Favori_Script_Create('ADD',$value['ID']);
		
		return '<div class="col-sm-3 col-md-3 product rotation">
	<div class="default">
		<div class="product-description">
			<div class="vertical">
				<h3 class="product-name">
				<a href=" ' . $url.'"> ' . $value['DT_ADI'] .'</a>
				</h3>
				
			</div>
		</div>
		<div style="height:270px;width:270px; margin-top:20px; padding:10px;" align="justify">
			' . $value['DT_ACIKLAMA'] . '
		</div>
	</div>
	
	<div class="product-hover">
		
		<a href=" ' . $url.'" class="product-image">
			<img src=" ' . $img.'" alt="" title="" >
		</a>
		<div class="actions">
			<a href="' . $url.'"   class="add-cart">
				<svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px" width="16px" height="16px" viewBox="0 0 16 16" enable-background="new 0 0 16 16" xml:space="preserve">
				<g>
				<path fill="#1e1e1e" d="M15.001,4h-0.57l-3.707-3.707c-0.391-0.391-1.023-0.391-1.414,0c-0.391,0.391-0.391,1.023,0,1.414L11.603,4
				H4.43l2.293-2.293c0.391-0.391,0.391-1.023,0-1.414s-1.023-0.391-1.414,0L1.602,4H1C0.448,4,0,4.448,0,5s0.448,1,1,1
				c0,2.69,0,7.23,0,8c0,1.104,0.896,2,2,2h10c1.104,0,2-0.896,2-2c0-0.77,0-5.31,0-8c0.553,0,1-0.448,1-1S15.554,4,15.001,4z
				M13.001,14H3V6h10V14z"></path>
				<path fill="#1e1e1e" d="M11.001,13c0.553,0,1-0.447,1-1V8c0-0.553-0.447-1-1-1s-1,0.447-1,1v4C10.001,12.553,10.448,13,11.001,13z"></path>
				<path fill="#1e1e1e" d="M8,13c0.553,0,1-0.447,1-1V8c0-0.553-0.448-1-1-1S7,7.447,7,8v4C7,12.553,7.448,13,8,13z"></path>
				<path fill="#1e1e1e" d="M5,13c0.553,0,1-0.447,1-1V8c0-0.553-0.447-1-1-1S4,7.447,4,8v4C4,12.553,4.448,13,5,13z"></path>
				</g>
				</svg>
			</a>
			<a href="#"  onclick="'.$FavoriEkle.'" class="add-wishlist">
				<svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px" width="16px" height="16px" viewBox="0 0 16 16" enable-background="new 0 0 16 16" xml:space="preserve">
				<path fill="#1e1e1e" d="M11.335,0C10.026,0,8.848,0.541,8,1.407C7.153,0.541,5.975,0,4.667,0C2.088,0,0,2.09,0,4.667C0,12,8,16,8,16
				s8-4,8-11.333C16.001,2.09,13.913,0,11.335,0z M8,13.684C6.134,12.49,2,9.321,2,4.667C2,3.196,3.197,2,4.667,2C6,2,8,4,8,4
				s2-2,3.334-2c1.47,0,2.666,1.196,2.666,2.667C14.001,9.321,9.867,12.49,8,13.684z"></path>
				</svg>
			</a>
			<a href=" ' . $url.'" class="add-compare">
				<svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px" width="16px" height="16px" viewBox="0 0 16 16" enable-background="new 0 0 16 16" xml:space="preserve">
				<path fill="#1e1e1e" d="M16,3.063L13,0v2H1C0.447,2,0,2.447,0,3s0.447,1,1,1h12v2L16,3.063z"></path>
				<path fill="#1e1e1e" d="M16,13.063L13,10v2H1c-0.553,0-1,0.447-1,1s0.447,1,1,1h12v2L16,13.063z"></path>
				<path fill="#1e1e1e" d="M15,7H3V5L0,7.938L3,11V9h12c0.553,0,1-0.447,1-1S15.553,7,15,7z"></path>
				</svg>
			</a>
		</div>
	</div>
</div>';
	}
	public static function UrlReplace( $link=NULL )
	{

		$turkce_harfler	= array('ı','ğ','ü','ş','ö','ç','İ','Ğ','Ü','Ş','Ö','Ç' );
		$latin_harfler	= array('i','g','u','s','o','c','I','G','U','S','O','C' );
		$text			= $link;
		$space			= preg_replace('/\s+/',' ',$text);
		$old			= array(' ','.','-','_',',');
		$new			= array('_','_','_','_','_');
		$change			= str_replace($old, $new, $space);
		$change			= str_replace($turkce_harfler, $latin_harfler, $change);
		$link			= strtolower(preg_replace('/[^a-z0-9_]/i','',$change));
		$trim			= trim($link, '_');
		return $trim;
	}


	public static function HtmlLog( $variable = '')
	{
		if (Config::HtmlLogControl() == 2) {
			switch ($variable) {
				case 'class':
					return '
[*title*] --------------------------- 
[*Name*]';
					break;
				case 'fileread':
					return '
[*title*] --------------------------- 
[*Name*]';
					break;
				case 'PageTime':
					return '
[*title*] --------------------------- 
[*Name*]';
					break;
				case 'Request':
					return '
[*title*] --------------------------- 
[*Name*]';
					break;

				default:
return '

======================================  '.Now.' ====================================== 
[*LogVariebles*]';
					break;
			}
		} else {

			switch ($variable) {
				case 'class':
					return '<span style="font-size: 12px; color:#B02642">[*title*] : </span>[*Name*]</br>';
					break;
				case 'fileread':
					return '<span style="font-size: 12px; color:#8E9E4E">[*title*] : </span>[*Name*]</br>';
					break;
				case 'PageTime':
					return '<span style="font-weight: bold;font-size: 13px; color:#F20000">[*title*] : </span><span style="font-weight: bold;font-size: 13px; ">[*Name*]</span></br>';
					break;
				case 'Request':
					return '<span style="font-size: 12px; color:#8E9E4E">[*title*] : </span>[*Name*]</br>';
					break;

				default:
					return '<div style="position: static;bottom: 0px;height: 300px;width: 100%;background-color: #F7F7F7;color:#23699C;border-top: 2px solid #23699C;overflow:scroll;">
								<span style="font-weight: bold;font-size: 13px;"> LOG OUTPUT</span></br>
								<div style="border-top: 1px dotted  #78B4E0;"></div>
								[*LogVariebles*]
							</div>
							';
					break;
			}
		}
	}

	public static function AdminCreate($v='')
	{
		switch ($v) {
			case 'input':
				return '
							<div class="mws-form-row">
								<label><strong>[*caption*]</strong></label>
								<div class="mws-form-item large">
									[*input*]
								</div>
							</div>
						';
				break;

			case 'comment':
				return  '
							<div class="mws-form-row">
								<label><strong></strong></label>
								<div class="mws-form-item comment">
									[*comment*]
								</div>
							</div>
						';
				break;
			case 'category':
				return '
							<div class="mws-form-row">
								<label><strong>[*caption*]</strong></label>
								<div class="mws-form-item large">
									[*input*]
								</div>
							</div>
						';
			break;

			case 'table_eski':
				return ' <div id="mws-container" class="clearfix">
							<div class="mws-panel grid_8">
			                	<div class="mws-panel-header">
			                    	<span class="mws-i-24 i-table-1">[*tablo_title*]</span>
			                    </div>
			                    [*MESSAGE*]
			                    <div class="dataTables_filter"><label>Search: <input type="text"></label></div>
			                    <div class="mws-panel-body">
			                       	<table class=" mws-table">
			                            <thead>
			                                <tr>
			                                    [*fields_name*]
			                                    <!-- <th> test </th> -->
			                                </tr>
			                            </thead>
			                            <tbody>
			                                [*fields_values*]
			                            </tbody>
			                        </table>
			                    </div>
			                </div>
			            </div>
                        ';
				break;
			case 'table':
				return '
					<div id="mws-container" class="clearfix">
						<div class="mws-panel grid_8">
						    <div class="mws-panel-header">
						        <span class="mws-i-24 i-table-1">[*tablo_title*]</span>
						    </div>
						    [*MESSAGE*]
						    <div class="mws-panel-body">
						        <table class="mws-datatable-fn mws-table">
						            <thead>
						                <tr>
						                    [*fields_name*]
						                </tr>
						            </thead>
						            <tbody>
						           		[*fields_values*]
						            </tbody>
						        </table>
						    </div>
						</div>
					</div>
				';
				break;
			case 'search':
				return ' 
						<div id="mws-container" class="clearfix">
							<div class="container">
								<div class="mws-panel grid_8  mws-collapsible mws-collapsed" style="margin-bottom:0px">
									<div class="mws-panel-header">
											<span class="mws-i-24 i-magnifying-glass-2">[*tablo_title*]</span>
									</div>
									<div class="mws-panel-body">
                    					<div class="mws-panel-content">
											<form action="?" class="mws-form"  method="post" accept-charset="utf-8" id="search" target="_parent" name="search"  enctype="multipart/form-data">
												<div class="mws-form-inline">
													[*icerik*]
												</div>
											</form>
										</div>
									</div>
								</div>
							</div>
						</div>
                        ';
				break;

			case 'upload':
				return '
							<li class="ui-state-default ui-corner-top" >
				               	<a href="#tab-2" class="mws-ic ic-box-down">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<strong>[*baslik*]</strong></a>
				            </li>';
				break;
			case 'upload_form':
				return '
							<div id="tab-2" class="ui-tabs-panel ui-widget-content ui-corner-bottom ui-tabs-hide">

									<div class="mws-form-inline">
										<input type="button" class="mws-tooltip-s mws-button blue" value="Dosya Ekle" id="newimage" original-title="Dosya Ekle" onclick="yeni_dosya_ekle(\'[*pages*]\',\'[*id*]\',\'sonuc\',\'crop\')">
										<div class="detailsimages">[*resimler*]</div><div class="clear"></div>
										<div id="sonuc"></div>
									</div>

							</div>';
				break;
			case 'form':
				return '
							<div id="mws-container" class="clearfix">
								<div class="mws-panel grid_8">
									<div class="mws-tabs ui-tabs ui-widget ui-widget-content ui-corner-all">
										<ul class="ui-tabs-nav ui-helper-reset ui-helper-clearfix ui-widget-header ui-corner-all">
				                            <li class="ui-state-default ui-corner-top ui-tabs-selected ui-state-active" >
				                            	<a href="#tab-1" class="[*icon-class*]">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<strong>[*baslik*]</strong></a>
				                            </li>
				                            [*upload*]
				                        </ul>
				                        [*MESSAGE*]
										<div id="tab-1" class="ui-tabs-panel ui-widget-content ui-corner-bottom">
											<form action="?" class="mws-form"  method="post" accept-charset="utf-8" id="form1" target="_parent" name="form1"  enctype="multipart/form-data">
												
												<div class="mws-form-inline">
													[*icerik*]
												</div>
											</form>
										</div>
										[*upload_form*]
									</div>
								</div>
							</div>
						';
				break;
			case 'form_image':
				return '<div class="mws-panel grid_8"><div class="mws-tabs"><ul><li><a href="#tab-1">[*baslik*]</a></li><li><a href="#tab-2">[*imagesbaslik*]</a></li> </ul><div id="tab-1">[*icerik*]</div><div id="tab-2">[*imagesupload*]</div></div><div class="clear"></div></div>';
				break;
			case 'form_upload':
			return '
			<style>
				.jrac_viewport {
					  width : [*style_crop_width*]px;
					  height: [*style_crop_height*]px;
				}
			</style>

					<link rel="stylesheet" type="text/css" href="'.Config::Yonetim('css').'upload.css" media="screen" />
					<script src="'.Config::Yonetim('plugins').'ajax_form/form.js"></script>
					<script  src="'.Config::Yonetim('js').'form_crop.js"  /></script>

					<!-- jrac - jQuery Resize And Crop -->
    				<link rel="stylesheet" type="text/css" href="'.Config::Yonetim('plugins').'/image_crop/style.jrac.css" />
    				<script type="text/javascript" src="'.Config::Yonetim('plugins').'/image_crop/jquery.jrac.js"></script>

					<link rel="stylesheet" type="text/css" href="'.Config::Yonetim('css').'upload.css" media="screen" />
					<script src="'.Config::Yonetim('plugins').'ajax_form/form.js"></script>
					<script  src="'.Config::Yonetim('js').'form_upload.js"  /></script>

					<div id="uploadd">
						<form action="'.Config::Yonetim().'" class="mws-form"  method="post" accept-charset="utf-8" id="formupload" target="_parent" name="formupload"  enctype="multipart/form-data" >
							<div class="mws-form-row" id="imageuploadfile">
									<div class="mws-form-item large" style="margin-left: 0px;">
										<label>Dosya Adı : </label> <input type="text" class="default" name="name"><br>
										<label>Dosya Seçiniz : </label> <input type="file" class="default" name="file">
									</div>
									<input type="hidden" name="action" value="file_uploading">
									<input type="hidden" name="pages" value="[*pages*]">
									<input type="hidden" name="id" value="[*id*]">
									<input type="hidden" name="crop" value="true">
									<input type="submit"  name="" value="Dosya Yükle" class="mws-tooltip-nw mws-button orange">
							</div>
						</form>
						 <div id="progress">
					        <div id="bar"></div>
					        <div id="percent">0%</div >
						</div>
						<br/>
					</div>
					<div id="message_upload"></div>';
			case 'form_upload_crop_watermark':

			return '

					<link rel="stylesheet" type="text/css" href="'.Config::Yonetim('css').'upload.css" media="screen" />
					<script src="'.Config::Yonetim('plugins').'ajax_form/form.js"></script>
					<script  src="'.Config::Yonetim('js').'form_crop.js"  /></script>
				
					<!-- jrac - jQuery Resize And Crop -->
    				<link rel="stylesheet" type="text/css" href="'.Config::Yonetim('plugins').'/image_crop/style.jrac.css" />
    				<script type="text/javascript" src="'.Config::Yonetim('plugins').'/image_crop/jquery.jrac.js"></script>
					<style>
						
						.jrac_viewport {
							  width : [*style_crop_width*]px;
							  height: [*style_crop_height*]px;
						}

					</style>
					 <script type="text/javascript">
					      <!--//--><![CDATA[//><!--
					      $(document).ready(function(){
					        // Apply jrac on some image.
					        $(\'.[*class_name*] img\').jrac({
					          \'crop_width\': [*crop_width*],
					          \'crop_height\': [*crop_height*],
					          \'crop_x\': 10,
					          \'crop_y\': 10,
					          \'image_width\': [*image_width*],
					          \'viewport_onload\': function() {
					            var $viewport = this;
					            var inputs = $viewport.$container.parent(\'.[*class_name*]\').find(\'.coords input:hidden\');
					            var events = [\'jrac_crop_x\',\'jrac_crop_y\',\'jrac_crop_width\',\'jrac_crop_height\',\'jrac_image_width\',\'jrac_image_height\'];
					            for (var i = 0; i < events.length; i++) {
					              var event_name = events[i];
					              // Register an event with an element.
					              $viewport.observator.register(event_name, inputs.eq(i));
					              // Attach a handler to that event for the element.
					              inputs.eq(i).bind(event_name, function(event, $viewport, value) {
					                $(this).val(value);
					              })
					              // Attach a handler for the built-in jQuery change event, handler
					              // which read user input and apply it to relevent viewport object.
					              .change(event_name, function(event) {
					                var event_name = event.data;
					                $viewport.$image.scale_proportion_locked = $viewport.$container.parent(\'.[*class_name*]\').find(\'.coords input:checkbox\').is(\':checked\');
					                $viewport.observator.set_property(event_name,$(this).val());
					              });
					            }
					          }
					        })
					        // React on all viewport events.
					        .bind(\'jrac_events\', function(event, $viewport) {
					          var inputs = $(this).parents(\'.[*class_name*]\').find(\'.coords input\');
					          inputs.css(\'background-color\',($viewport.observator.crop_consistent())?\'chartreuse\':\'salmon\');
					        });
					      });
					      //--><!]]>
				    </script>
					<form action="'.Config::Yonetim().'" class="mws-form"  method="post" accept-charset="utf-8" id="formcrop" target="_parent" name="formcrop"  enctype="multipart/form-data" >
						<input type="hidden" name="action" value="[*action*]">
						<input type="hidden" name="pages" value="[*pages*]">
						<input type="hidden" name="id" value="[*id*]">
						<input type="hidden" name="[*cropping*]" value="true">
						<input type="hidden" name="upload_id" value="[*upload_id*]">
						<div><span  class="Crop_Title">[*Crop_Title*]</span></div>
							
							<div class="mws-form-inline">
								<div class="mws-form-row">
									<label><strong>Filigran Yazı</strong></label>
									<input type="input" name="watermark_text" value="[*watermark_text*]" class="mws-textinput small">
								</div>
								<div class="mws-form-row">
									<label><strong>Filigran Yazı Açı</strong></label>
									<input type="input" name="watermark_angle" value="[*watermark_angle*]"class="mws-textinput">
								</div>
								<div class="mws-form-row">
									<label><strong>Filigran Font Boyutu </strong></label>
									<input type="input" name="watermark_fontsize" value="[*watermark_fontsize*]"class="mws-textinput">
								</div>
								<div class="mws-form-row">
									<label><strong>Filigran Şeffaflık</strong></label>
									<input type="input" name="watermark_alpha" value="[*watermark_alpha*]"class="mws-textinput">
								</div>
								<div class="mws-form-row">
									<label><strong>Filigran X & Y</strong></label>
									<input type="input" name="watermark_x" value="[*watermark_x*]"class="mws-textinput"> | <input type="input" name="watermark_y" value="[*watermark_y*]"class="mws-textinput">
								</div>
							</div>
						
						<div class="[*class_name*] clearfix">
						    <img src="[*image_path*]"/>
					      	<div class="coords">
						        <input type="hidden" name="crop_x"/>
						        <input type="hidden" name="crop_y"/>
						        <input type="hidden" name="crop_w"/>
						        <input type="hidden" name="crop_h"/>
						        <input type="hidden" name="image_w"/>
						        <input type="hidden" name="image_h"/>
					      	 	<input type="submit" value="Resmi Kırp" class="mws-button red">
					      	</div>
				    	</div>
					</form>
					<div id="newImage"></div>
					';
			case 'form_upload_crop':

			return '

					<link rel="stylesheet" type="text/css" href="'.Config::Yonetim('css').'upload.css" media="screen" />
					<script src="'.Config::Yonetim('plugins').'ajax_form/form.js"></script>
					<script  src="'.Config::Yonetim('js').'form_crop.js"  /></script>
				
					<!-- jrac - jQuery Resize And Crop -->
    				<link rel="stylesheet" type="text/css" href="'.Config::Yonetim('plugins').'/image_crop/style.jrac.css" />
    				<script type="text/javascript" src="'.Config::Yonetim('plugins').'/image_crop/jquery.jrac.js"></script>
					<style>
						
						.jrac_viewport {
							  width : [*style_crop_width*]px;
							  height: [*style_crop_height*]px;
						}

					</style>
					 <script type="text/javascript">
					      <!--//--><![CDATA[//><!--
					      $(document).ready(function(){
					        // Apply jrac on some image.
					        $(\'.[*class_name*] img\').jrac({
					          \'crop_width\': [*crop_width*],
					          \'crop_height\': [*crop_height*],
					          \'crop_x\': 10,
					          \'crop_y\': 10,
					          \'image_width\': [*image_width*],
					          \'viewport_onload\': function() {
					            var $viewport = this;
					            var inputs = $viewport.$container.parent(\'.[*class_name*]\').find(\'.coords input:hidden\');
					            var events = [\'jrac_crop_x\',\'jrac_crop_y\',\'jrac_crop_width\',\'jrac_crop_height\',\'jrac_image_width\',\'jrac_image_height\'];
					            for (var i = 0; i < events.length; i++) {
					              var event_name = events[i];
					              // Register an event with an element.
					              $viewport.observator.register(event_name, inputs.eq(i));
					              // Attach a handler to that event for the element.
					              inputs.eq(i).bind(event_name, function(event, $viewport, value) {
					                $(this).val(value);
					              })
					              // Attach a handler for the built-in jQuery change event, handler
					              // which read user input and apply it to relevent viewport object.
					              .change(event_name, function(event) {
					                var event_name = event.data;
					                $viewport.$image.scale_proportion_locked = $viewport.$container.parent(\'.[*class_name*]\').find(\'.coords input:checkbox\').is(\':checked\');
					                $viewport.observator.set_property(event_name,$(this).val());
					              });
					            }
					          }
					        })
					        // React on all viewport events.
					        .bind(\'jrac_events\', function(event, $viewport) {
					          var inputs = $(this).parents(\'.[*class_name*]\').find(\'.coords input\');
					          inputs.css(\'background-color\',($viewport.observator.crop_consistent())?\'chartreuse\':\'salmon\');
					        });
					      });
					      //--><!]]>
				    </script>
					<form action="'.Config::Yonetim().'" class="mws-form"  method="post" accept-charset="utf-8" id="formcrop" target="_parent" name="formcrop"  enctype="multipart/form-data" >
						<input type="hidden" name="action" value="[*action*]">
						<input type="hidden" name="pages" value="[*pages*]">
						<input type="hidden" name="id" value="[*id*]">
						<input type="hidden" name="[*cropping*]" value="true">
						<input type="hidden" name="upload_id" value="[*upload_id*]">
						<div><span  class="Crop_Title">[*Crop_Title*]</span></div>
						
						<div class="[*class_name*] clearfix">
						    <img src="[*image_path*]"/>
					      	<div class="coords">
						        <input type="hidden" name="crop_x"/>
						        <input type="hidden" name="crop_y"/>
						        <input type="hidden" name="crop_w"/>
						        <input type="hidden" name="crop_h"/>
						        <input type="hidden" name="image_w"/>
						        <input type="hidden" name="image_h"/>
					      	 	<input type="submit" value="Resmi Kırp" class="mws-button red">
					      	</div>
				    	</div>
					</form>
					<div id="newImage"></div>
					';

			case 'submit_button':
				return '<div class="mws-button-row"><input type="submit" value="[*value*]" class="mws-button red"></div>';
				break;


			default:
				return '';
				break;
		}
	}

	/**
	 * FormMessages
	 * Ekleme, Çıkarma veya Güncelleme yapıldığında ekrana basılacak 
	 * uyarı tipleri ve templateleri
	 * 
	 * @param string $type  mesaj tipi
	 */
	public static function FormMessages( $type='' )
	{
		$type = strtoupper($type);
		switch ($type) {
			case 'ERROR':
					return '<div class="mws-form-message error">[*MESSAGE*]</div>';
				break;
			case 'SUCCESS':
					return '<div class="mws-form-message success">[*MESSAGE*]</div>';
				break;
			case 'INFO':
					return '<div class="mws-form-message info">[*MESSAGE*]</div>';
				break;
			case 'WARNING':
					return '<div class="mws-form-message warning">[*MESSAGE*]</div>';
				break;
		}
	}

	public  static function SiteAlert( $type='' )
	{
		$type = strtoupper($type);
		switch ($type) {
			case 'BLACK':
					return '<div class="alert alert-black"><div class="fa fa-circle-o alert-icon"></div><button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>[*MESSAGE*]</div>';
				break;
			case 'SUCCESS':
					return '<div class="alert alert-success"><div class="fa fa-check alert-icon"></div><button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>[*MESSAGE*]</div>';
				break;
			case 'INFO':
					return '<div class="alert alert-info"><div class="fa fa-info alert-icon"></div><button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>[*MESSAGE*]</div>';
				break;
			case 'WARNING':
					return '<div class="alert alert-warning"><div class="fa fa-exclamation-triangle alert-icon"></div><button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>[*MESSAGE*]</div>';
				break;
			case 'DANGER':
				return '<div class="alert alert-danger"><div class="fa fa-times alert-icon"></div><button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>[*MESSAGE*]</div>';
		}
	}

}