<?php

/**
*
*/
class Kur
{
	public $Kur = '';

	function __construct()
	{

	}

	public function get($return_rate = 'USD')
	{
		$Cache = new Cache;
		$KURLAR = $Cache->get( 'KURLAR' );

		if ( isset($KURLAR) AND count($KURLAR) > 0) {
			if (isset( $KURLAR[$return_rate]['RATE'] ) AND $KURLAR[$return_rate]['DATE'] == NowDate ) {
				return $KURLAR[$return_rate]['RATE'];
			}
		}

		$c = new MultiCurl;

		$site[1]  ='http://www.tcmb.gov.tr/kurlar/today.xml';
		
		$c->setPages($site);
		$c->exec();
		$result = $c->getData();
		$doc    = new DOMDocument();
		$doc->loadXML($result[1]);

		$xpath    = new DOMXpath($doc);
		$elements = $xpath->query("//Currency[@CurrencyCode='$return_rate']/ForexSelling");

		foreach ($elements as $key => $value) {
			$Kur = ($value->nodeValue);
		}

		$kur_sess[$return_rate]['RATE'] = $Kur;
		$kur_sess[$return_rate]['DATE'] = NowDate;
		$Cache->set( 'KURLAR' , $kur_sess );
		return $Kur;
	}

	public function exchange( $price='' , $rate = 'USD' )
	{
		$return = $price / $this->get($rate);
    	return number_format(round($return,5),2);
	}

}
