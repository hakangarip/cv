<?php

/**
*
*/
class Cache_Tables
{
	public $Cache;
	public $Translate  = FALSE;
	public $Cache_Type = 'Multi';

	function __construct()
	{
		$this->Cache = new Cache;
	}

	/**
	 * Type
	 * Eğer Saklanacak olan değerler sadece tek kayıt içerecek ise;
	 * Örnek olarak Genel Ayarlar
	 * 
	 * @param  string $type 
	 * @return  void
	 */
	function type( $type='Multi' )
	{
		$this->Cache_Type = $type;
	}

	/**
	 * Translate
	 * tabloda translate var ise true yapılıyor.
	 * @param  string $translate 
	 * @return [type]            [description]
	 */
	public function translate( $translate='FALSE' )
	{
		$this->Translate = $translate;
	}

	public function table( $table='' )
	{
		$table = strtoupper($table);
		$this->set($table);
		$this->type();
	}

	public function setTranslate( $table='' )
	{
		$Db        = new Db;
		$LANGUAGES = $this->Cache->get('LANGUAGES');

		if (!$LANGUAGES) {
			$this->Create('LANGUAGES');
		}
		$LANGUAGES = $this->Cache->get('LANGUAGES');
		$result    = array();

		foreach ($LANGUAGES as $k => $v) {

			$LANGUAGES_ID = $v['ID'];
			
			$Db->table($table);
			$Db->join( $table.'_TRANSLATE', $table.'_TRANSLATE.MAIN_ID = ' .$table.'.ID' );		
			$Db->where( array($table.'_TRANSLATE.LANGUAGE_ID',$table.'.STATUS'), array($LANGUAGES_ID, 'TRUE') );
			$Db->select();

			$resultData = $Db->result();
			
			foreach ($resultData as $key => $value) {
				if ($this->Cache_Type == 'Single') {
					$result[$LANGUAGES_ID]= $value;	
				} else {
					// Translate kelimelerin çekildiği tablo gelmiş ise ife giriyor. 
					if ( $table == 'TRANSLATE_KEYWORDS' ) {
						$result[$LANGUAGES_ID][$value['KEYWORD_KEY']] = $value['TKT_MEAN'];
					} else {
						$result[$LANGUAGES_ID][$value['ID']] = $value;
					}
				}
				
			}

		} // LANGUAGES END 
		// prp($result);
		$this->Cache->set($table , $result);
	}

	public function setMain( $table='' )
	{
		$Db = new Db;
		$Db->table($table);
		$Db->where($table.'.STATUS' , 'TRUE');
		$Db->select();
		$resultData = $Db->result();
		$result= array();
		// prpe($table);
		// prpe($resultData);
		if ($this->Cache_Type == 'Single') {
			$result = $resultData[0];	
		} else {

			foreach ($resultData as $key => $value) {
				$result[$value['ID']] = $value;
			}
		}
	
		$this->Cache->set($table , $result);
	}

	public function set( $table )
	{
		$table = strtoupper($table);

		if ( $this->Translate ) {
			$this->setTranslate( $table );
		} else {
			$this->setMain( $table );
		}
		$this->Translate();

	}

	public function get( $table  , $fieldName = '' , $fieldValue = '' , $return = '')
	{
		$table = strtoupper($table);
		if ( empty($fieldName) and empty($fieldValue) and empty($return) )  {
			
			return $this->Cache->get($table);
		}
		
		if ( !$this->Cache->get($table) ) {
			$this->set($table);
			$this->get($table  , $fieldName = '' , $fieldValue = '' , $return = '');
		}
		// Eğer 3 ü de dolu gelmişse
		if ( !empty($fieldName) and !empty($fieldValue) and !empty($return) )  {
			foreach ($this->Cache->get($table) as $key => $value) {
				if ($value[$fieldName] == $fieldValue) {
					return $value[$return];
				}
			}
		} elseif ( !empty($fieldName) and empty($fieldValue) and empty($return) )  {
			$returnCache = $this->Cache->get($table);
			if ( $returnCache ) {
				foreach ($returnCache as $key => $value) {
					if ($key == $fieldName) {
						return $value;
					}
				}
			} 
		}
		return FALSE;
	}

	public function Create( $tableName )
	{
		$ADTI       = new Admin_Database_Tables_Information;
		$asj        = $ADTI->getJson( $tableName );
    	FileControl($asj);
    	$asj = json_decode( $asj );
		$asj = json_decode( json_encode( $asj ), true);
		
		$ADA = new Admin_Database_Action ;
		$ADA->CacheControl($asj,$tableName);
	}

}