<?php 

/**
* 
*/
class Cache
{
	public $m    = '';
	public $type = '';

	function __construct()
	{
		include_class( get_class() );
		// $this->m  = new Memcached();
		// $this->type = 'memcached';
		// $this->m->addServer('localhost', 11211);
		if (!MEMCACHE) {
		 return false;
		}
		$this->m = memcache_connect ('localhost', 11211);
		$this->type = 'memcache';
		if ( ! $this->m ) {
			echo "Connection to memcached failed";
			return false;
		}
	}

	
	public function set($key, $var )
	{
		if (!MEMCACHE) {
		 return false;
		}
		$key = Config::Memcache_Prefix().'_'.$key;
		$this->m->set($key, $var);
		$GLOBALS['CACHE_DB'][$key] = $var;
	}

	public function get( $key='' )
	{
		if (!MEMCACHE) {
		 return false;
		}
		$key        = Config::Memcache_Prefix().'_'.$key;
		if ( isset( $GLOBALS['CACHE_DB'][$key] ) ) {
			return 	$GLOBALS['CACHE_DB'][$key];
		}

		// $getallkeys = $this->getAllKeys();
		// $getallkeys = $this->getMemcacheKeys();
		// // prpe($getallkeys);
		// // exit();
		// if (is_array($getallkeys)) {
		// 	foreach ($getallkeys as $k => $v) {
		// 		if ($v == $key ) {
		// 			$GLOBALS[$key] = $this->m->get($key);
		// 			return $GLOBALS[$key];
		// 		}
		// 	}
		// }
		// prpe('$GLOBALS[]' .$key  );
		$returnvalue = $this->m->get($key);
		// prpe($returnvalue);
		// exit;
		if ( !empty($returnvalue) ) {
			$GLOBALS['CACHE_DB'][$key] = $this->m->get($key);
			return  $GLOBALS['CACHE_DB'][$key];
		}

		return false;
	}

	public function flush()
	{	
		$this->m->flush();
	}

	public function getAllKeys()
	{
		if ($this->type == 'memcached') {
			return $this->m->getAllKeys();
		}
		return $this->getMemcacheKeys();
	  
	}

	public function SetAllCahce()
	{
		$arrVal = $this->getAllKeys();
		foreach($arrVal AS $v=> $k) {                   
        	if ( substr($k, 0,strlen(Config::Memcache_Prefix()))  == Config::Memcache_Prefix() )  {
        		// prpe($k);
        		$GLOBALS['CACHE_DB'][$k] = $this->m->get($k);
            	$return[] = $k;
            }
        }
       	return $return;
	}
	function getMemcacheKeys() {

    // $this->m = new Memcache;
    // $this->m->connect('127.0.0.1', 11211) or die ("Could not connect to memcache server");
		$return   = array();
		$list     = array();
		$allSlabs = $this->m->getExtendedStats('slabs');
		$items    = $this->m->getExtendedStats('items');
	    foreach($allSlabs as $server => $slabs) {
	    	// prpe($slabs);
	        foreach($slabs AS $slabId => $slabMeta) {
	            $cdump =@$this->m->getExtendedStats('cachedump',(int)$slabId);
	            // prpe($cdump );
	            foreach($cdump AS $keys => $arrVal) {
	                if (!is_array($arrVal)) continue;
	                foreach($arrVal AS $k => $v) {                   
	                	if ( substr($k, 0,strlen(Config::Memcache_Prefix()))  == Config::Memcache_Prefix() )  {
	                		$GLOBALS['CACHE_DB'][$k] = $this->m->get($k);
	                    	$return[] = $k;
	                    }
	                }
	           }
	        }
	    }   
	    return $return;
	}
}