<?php 

/**
* 
*/
class Pager
	{
	public $Page       = '';
	public $FolderName = 'Pages';
	public $SubName    = 'MODEL';
	public $Values     = '';

	function __construct()
	{
		include_class( get_class() );
		$return = false;
		if ( $_GET ) {
			$get = $_GET;
			// prpe($get);
			if ( ! empty($get) ) {

				$get_explode = explode('/' , $get['get']);
				$pager=$get_explode [0];
				// prpe($get_explode);
			}
		}

		if ( ! isset($pager) ) {
			$pager = Config::DefaultPage();
		}
		if (isset($get_explode[0])) {
			unset($get_explode[0]);
			$this->Values = $get_explode;
		}
		$this->Page   = $pager;
	}

	public function setFolder($folderName)
	{
		$this->FolderName =  $folderName;
	}

	public function setPage( $includeFolder = '' )
	{
		$this->Page =  $includeFolder;
	}

	public function setSubName( $SubName='' )
	{
		$this->SubName = $SubName;
	}

	public function Values($value='')
	{
		$this->Values = $value;
	}

	public function model( $value = '' )
	{	
		$this->Page = strtoupper($this->Page);
		include DR.SLASH.PUBLIC_FOLDER.SLASH.$this->FolderName.'/'.$this->Page.'/model.php';
		$ModelName = $this->SubName.'_'.$this->Page;
		$return = new $ModelName;
		return $return->set($this->Values , $value);
	}

	public function view( $returnValue = '' )
	{	

		if ( !empty($returnValue) ) {
			foreach ($returnValue as $key => $value) {
				$GLOBALS[$key] = $value;
				$$key          = $value;
			}
		}

		include DR.SLASH.PUBLIC_FOLDER.SLASH.$this->FolderName.'/'.$this->Page.'/view.php';
	}

}
