<?php 

/**
* 
*/
class Pager_Site
{
	public static function get($fileName='' , $values = '')
	{
		include_class( get_class() );
		$pager = new Pager;

		$pager->setFolder(Config::TemplateFolderName());
		$pager->setSubName(Config::TemplateSubClassName());
		$pager->setPage( strtoupper($fileName) );
		return $pager->view( $pager->model( $values ) );
	}
}
