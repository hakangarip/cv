<?php 

/**
* 
*/
class HtmlLog
{
	public $starttime;
	public $totaltime;
	public $ClassInclude   = '';
	public $OtherFunctions = '';
	public $Class          = '';
	public $In_Class       = '';
	public $Get_Db         = '';
	public $Set_Session    = '';

	function __construct()
	{
		// include_class( get_class() );
		$this->start();
	}

	public function start()
	{
		$mtime = microtime(); 
	   	$mtime = explode(" ",$mtime); 
	   	$mtime = $mtime[1] + $mtime[0]; 
	   	$this->starttime = $mtime; 
	   	// echo "string".	$this->starttime;
	}

	public function end()
	{
		$mtime = microtime(); 
		$mtime = explode(" ",$mtime); 
		$mtime = $mtime[1] + $mtime[0]; 
		$endtime = $mtime; 
		$this->totaltime = ($endtime - 	$this->starttime); 
			
		// echo "string".	$this->totaltime;
	}

	public function CacheSize()
	{
		return '
Page Size :' . realpath_cache_size()  ;
	}
	public function PageTime()
	{
		$this->end();
		return '
Page Download Time :' . $this->totaltime  ;
	}

	public function Time()
	{
		return 'Log Time :' .  date('Y-m-d H:i:s') ;
	}

	public function Log($value = '')
	{
		$this->OtherFunctions .=$value;
	}


	public function Set_Db($value='')
	{
		$this->Get_Db[]=$value;
	}
	
	public function Get_Db()
	{
		if( empty($this->Get) ) return '';
		$outlog='';
		foreach ($this->Get_Db as $key => $value) {
			$outlog .= $value.'
';
		}
		$logClass = str_replace( '[*title*]', 'SQL QUERYS' , Config::HtmlLog('class') );
		$logClass = str_replace( '[*Name*]', $outlog  , $logClass );
		return $logClass;
	}

	public function Set_Class($value='')
	{
		$this->Class[]=$value;
	}
	public function Get_Class()
	{
		if( empty($this->Class) ) return '';
		$outlog='';
		foreach ($this->Class as $key => $value) {
			$outlog .= $value.'
';
		}
		$logClass = str_replace( '[*title*]', 'INCLUDE CLASS' , Config::HtmlLog('class') );
		$logClass = str_replace( '[*Name*]', $outlog  , $logClass );
		return $logClass;
	}


	public function In_Class( $Class_Name='' )
	{
		if (isset($this->In_Class[$Class_Name])) {
			$this->In_Class[$Class_Name] = $this->In_Class[$Class_Name] + 1 ;
			return ;
		} 
		$this->In_Class[$Class_Name] = 1;
	}

	public function get_In_Class()
	{
		if( empty($this->In_Class) ) return '';
		$outlog='';
		foreach ($this->In_Class as $key => $value) {
			$outlog .= $key.' = ' . $value . '
';
		}
		$logClass = str_replace( '[*title*]', 'CALL CLASS' , Config::HtmlLog('class') );
		$logClass = str_replace( '[*Name*]', $outlog  , $logClass );
		return $logClass;
	}

	public function Page_Opt()
	{
		$output = $this->Time().$this->PageTime().$this->CacheSize();
		$logClass = str_replace( '[*title*]', 'PAGE PROPERTY' , Config::HtmlLog('class') );
		$logClass = str_replace( '[*Name*]', $output  , $logClass );
		return $logClass;
	}

	public function Set_Session($title = '' , $value='')
	{
		$this->Set_Session[$title]=$value;
	}

	public function Get_Session()
	{
		if( empty($this->Set_Session) ) return '';
		$outlog='';
		foreach ($this->Set_Session as $key => $value) {
			$outlog .=$key .' = ' . $value.'
';
		}
		$logClass = str_replace( '[*title*]', 'SESSION PROPERTY' , Config::HtmlLog('class') );
		$logClass = str_replace( '[*Name*]', $outlog  , $logClass );
		return $logClass;
	}

	public function OutputFunctions()
	{

		return 	$this->Get_Class().
				$this->get_In_Class().
				$this->Get_Db().
				$this->OtherFunctions.
				$this->Get_Session().
				$this->Page_Opt()	
			;
	}

	public function Output()
	{
		// $File   = new File;
		$return = str_replace( '[*LogVariebles*]', $this->OutputFunctions(), Config::HtmlLog() );
		// $File->setPath(DR.SLASH.'Settings'.SLASH.'Log');
		error_log ( $return , 3  ,  DR.SLASH.PRIVATE_FOLDER . SLASH . 'Settings'.SLASH.'Log'.SLASH.date('Y-m-d').'.log' );
		// $File->write( date('Y-m-d').'.log' , $return , 'add');
		return $return;

	}


}