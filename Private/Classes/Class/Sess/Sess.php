<?php 

/**
* 
*/
class Sess
{
	public $array_keys = array();
	public $time = 0 ;
	function __construct()
	{
		include_class( get_class() );
	}

	public function setSessLifeTime($key='')
	{
		if ($this->time == 0 ) {
			$STOP_TIME = 0;
		} else {

			$date  = date('Y-m-d H:i:s');
			$date2 = strtotime($date) + $this->time;
			$STOP_TIME = date('Y-m-d H:i:s',$date2);
		}
		$_SESSION['SESSIONS_SETTINGS']['LIFE_TIME'][$key]['STOP_TIME'] = $STOP_TIME;
	}

	public function sessLifeTimeControl($key)
	{
		// $_SESSION['SESSIONS_SETTINGS']['LIFE_TIME'][$key];
		if(isset($_SESSION['SESSIONS_SETTINGS']['LIFE_TIME'][$key]) ) {
			$date  = date('Y-m-d H:i:s');
			$Session_Stop_Time = $_SESSION['SESSIONS_SETTINGS']['LIFE_TIME'][$key]['STOP_TIME'];
			if ( $Session_Stop_Time < $date AND $Session_Stop_Time != 0) {
				$this->destroy($key);
				$this->destroy('SESSIONS_SETTINGS','LIFE_TIME',$key);
				return false;
			}
		}
		return true;
	}

	public function setTime($time='0')
	{
		$this->time =  $time;
	}
	/**
	 * Set Session
	 * Key array yollanırsa key içinde direk işlemn yapıyor. 
	 * @param string $key   [description]
	 * @param string $value [description]
	 */
	public function set($key='' , $value = '')
	{
		if (empty($value)) {
			foreach ($key as $k => $v) {
				$_SESSION[$k] = $v;
				$this->setSessLifeTime($k);
			}
			$this->setTime(0);
			return true;
		}
		
		foreach ($value as $k => $v) {
			$_SESSION[$key][$k] = $v;
		}
		$this->setSessLifeTime($key);
		$this->setTime(0);
		return true;
	
	}

	/**
	 * [get description]
	 *
	 * Örnek Olarak 
	 * $s->get(array(Config::Sess('Admin') , 'UserName'));
	 * $s->get(Config::Sess('Admin'));
	 * @param  string $key [description]
	 * @return [type]      [description]
	 */
	public function get($key='')
	{

		if (is_array($key)) {
			$Sess = $_SESSION;
			foreach ($key as $k => $v) {
				if ( !isset( $Sess[$v]) ) {
					return false;
				}
				if (!$this->sessLifeTimeControl($v)){
					return false;
				}
				$Sess = $Sess[$v];
			}
			return $Sess;
		}

		if (isset( $_SESSION[$key])) {
			if (!$this->sessLifeTimeControl($key)){
				return false;
			}
			return  $_SESSION[$key];
		}

		return false;
	}

	public function allGet()
	{
		return $_SESSION;
	}

	/**
	 * [destroy description]
	 * $s->destroy('TT', 'TEST' ,'ADI');
	 * @param  string $key1 [description]
	 * @param  string $key2 [description]
	 * @param  string $key3 [description]
	 * @return [type]       [description]
	 */
	public function destroy( $key1=''  , $key2 = '' , $key3 = '')
	{
		
		if ( !empty($key1) AND  !empty($key2) AND !empty($key3) ) {
			unset($_SESSION[$key1][$key2][$key3]);	
		} elseif ( !empty($key1) AND  !empty($key2) AND empty($key3) ) {
			unset($_SESSION[$key1][$key2]);	
		} elseif ( !empty($key1) AND  empty($key2) AND empty($key3) ){
			unset($_SESSION[$key1]);	
		} else {
			$_SESSION = array();
			session_destroy();
		}
		return;

		// if (is_array($key)) {
		// 	$array = array();
		// 	foreach ($key as $k => $v) {
		// 		// prpe($k) ; prpe($v);
		// 		$array[$k][$v] = true;
		// 	}	
		// }
		// prpe($array);
		// unset($_SESSION[array('TT' => array('TEST'))]);
	}

	public function setKey($keys='')
	{

		if (is_array($keys)) {
			foreach ($keys as $k => $v) {
				
				$this->array_keys[$k] = $v;
				
				if (is_array($v) ){
					$this->setKey($v);
				}

			}	

			return;
		}
		$this->array_keys = $keys; 

	}

	public function Id()
	{
		return  session_id();
	}
}
