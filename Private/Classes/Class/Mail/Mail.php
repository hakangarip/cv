<?php 


/**
*
* 
*/
class Mail
{

	public $mail_type   = '';
	public $html        = '';
	public $subject     = '';
	public $from_name   = '';
	public $from_email  = '';
	public $headers     = '';
	public $to          = array();

	function __construct()
	{
		include_class( get_class() );
	}

	/**
	 * set_Type
	 * mail tip isetleniyor. 
	 * Mandrill veya default olarak php mail olarak
	 * @param string $mail_type [description]
	 */
	public function set_Type( $mail_type = '' )
	{
		if (empty($mail_type)) {
			$this->mail_type = Config::Mail_Stype();
		} else{
			$this->mail_type = $mail_type;
		}

	}

	public function get_Type()
	{
		return $this->mail_type;
	}

	public function html($html='')
	{
		$this->html = $html;
	}

	public function subject($subject='')
	{
		$this->subject = $subject;
	}

	public function from_name($from_name='')
	{
		$this->from_name = $from_name;
	}

	public function from_email($from_email='')
	{
		$this->from_email = $from_email;
	}

	public function headers($header='')
	{
		$this->headers = $header;
	}

	public function to( $to='' )
	{
		$this->to[] = $to;
	}

	public function send()
	{
		$this->set_Type();
		if ($this->get_Type() == 'Mandrill') {
			return $this->mandrill_send();
		} else {
			return $this->php_send();
		}
		
	}

	public function mandrill_send()
	{
		// $to = array();
		try {

		    $mandrill = new Mandrill( Config::Mandrill_Key() );
			
			// foreach ($this->to as $key => $value) {
			// 	$to[] =array( 
			// 			'email' => $value['email'],
			// 			'name'  => $value['name'],
			// 			'type'  => 'to'
		 //            );

			// }
		    $message = array(
					'html'       => $this->html,
					'subject'    => $this->subject,
					'from_email' => $this->from_email,
					'from_name'  => $this->from_name,
					'to'         => $this->to,
					'headers'    => $this->headers,  
		    );
		
		    $async = false;
		    $ip_pool = '';
		    $send_at = '';
		    $result = $mandrill->messages->send($message, $async, $ip_pool, $send_at);
		    $this->Clear();
		    return $result;
		    
		} catch(Mandrill_Error $e) {
		    // Mandrill errors are thrown as exceptions
		    echo 'A mandrill error occurred: ' . get_class($e) . ' - ' . $e->getMessage();
		    // A mandrill error occurred: Mandrill_Unknown_Subaccount - No subaccount exists with the id 'customer-123'
		    throw $e;
		    exit();
		}
	
	}

	public function Clear()
	{
		$this->set_Type();
		$this->html       = '';
		$this->subject    = '';
		$this->from_name  = '';
		$this->from_email = '';
		$this->headers    = '';
		$this->to         =  array();
	}


}
