<?php 
/**
* 
*/
class Curl 
{
	public  $Agent            = 'Googlebot/2.1 (http://www.googlebot.com/bot.html)';
	public  $UserAgent        = '';
	public  $Pages            = '';
	public  $Return           = array();
	public  $MultiCurlControl = FALSE;
	
	function __construct()
	{
		include_class( get_class() );
		$this->UserAgent = $this->Agent;
	}

	/**
	 * userAgent 
	 * Curl yapılırken hangi agent üzerinden okunacağını belirtiyor. 
	 * Default olarak google bot geliyor. 
	 * @param  string $agent [description]
	 * @return [type]        [description]
	 */
	public function userAgent( $agent='' )
	{
		$this->UserAgent = ( $agent == '' ) ? $this->Agent : $agent;
	}

	/**
	 * [setPages description]
	 * @param string $pages [description]
	 */
	public function setPages($pages='')
	{
		$this->Pages = $pages;
	}

	public function setMulti( $multi = FALSE )
	{
		$this->MultiCurlControl =  $multi;
	}

	public function getMulti()
	{
		return $this->MultiCurlControl;
	}

	public function MultiCurl()
	{
		$mh            = curl_multi_init(); 
		$curl_array    = array(); 
		
		foreach($this->Pages as $i => $url) { 

            $curl_array[$i]	= curl_init($url); 
            curl_setopt($curl_array[$i], CURLOPT_USERAGENT, $this->UserAgent);           
			curl_setopt($curl_array[$i], CURLOPT_FOLLOWLOCATION, 1);
            curl_setopt($curl_array[$i], CURLOPT_RETURNTRANSFER, true); 
            curl_setopt($curl_array[$i], CURLOPT_ENCODING , "gzip");
            // curl_setopt($curl_array[$i], CURLOPT_SSL_VERIFYPEER , 0); 
            curl_setopt($curl_array[$i], CURLOPT_POST , 1); 
			curl_setopt($curl_array[$i], CURLOPT_POSTFIELDS, $i);
            curl_multi_add_handle($mh, $curl_array[$i]); 
        }

        $running = NULL; 
        do { 
            curl_multi_exec($mh,$running); 
        } while($running > 0); 

        $result = array(); 
        foreach($this->Pages as $i => $url) { 
            $result[$i]	= curl_multi_getcontent($curl_array[$i]); 
            $result['status'][$i]= curl_getinfo($curl_array[$i]);
            curl_multi_remove_handle($mh, $curl_array[$i]); 
        } 
        
        curl_multi_close($mh); 
        $this->Return = $result;
		return $this->Return;
	}

	/**
	 * [exec description]
	 * @return [type] [description]
	 */
	public function exec()
	{ 
		if ( $this->getMulti() ) {
			 return $this->MultiCurl();
		} 
		
		if ( ! is_array($this->Pages) ) {
			
			$pages[1] = $this->Pages;
			$this->setPages($pages);
			$result = $this->MultiCurl();
			return  $result[1];
		}
	}

	public function getData()
	{
		return $this->Return;
	}

	public function clear()
	{
			$this->UserAgent = $this->Agent;	
			$this->Pages     = '';
			$this->Return     = array();
	}
	public function returntest()
	{
		return $this;
	}
}