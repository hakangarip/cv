<?php

/**
*
*/
class Get
{
	public $get;
	function __construct()
	{
		include_class( get_class() );
		$return = false;
		if ( $_GET ) {
			$get = $_GET;

			if ( !is_array($get) ) {
				if ( ! empty($get) ) {

					$get_explode = explode('/' , $get['get']);
					
					if ( count($get_explode) == 1 ) { // Burada sadece iletisim.html gibi geliyor Değişken Gelmiyor.

						$pager = Config::DefaultPage();

					} else {

						foreach ($get_explode as $key => $value) {
							if ( !empty($value) ) {
								$return[$key] = $value;
							}
						}
					}
				}
			} else {
				foreach ($get as $key => $value) {
					if ( !empty($value) ) {
						$return[$key] = $value;
					}
				}
			}
			global $htmlLog;
			$logClass = str_replace( '[*title*]', 'Gets' , Config::HtmlLog('Request') );
			$logClass = str_replace( '[*Name*]', prpl($return)  , $logClass );
			$htmlLog->Log( $logClass );
		}
		
		$this->get  = $return;
	}

	public function get( $key = '' )
	{
		$key    = (string)$key;
		$return = false;
		if ( $key == '' ) {
			$return =  $this->get;
		} else {
			if (isset( $this->get[$key] )) {
				$return = $this->get[$key];
			}
		}
		return  $return;
	}

}