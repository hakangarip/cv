<?php 
/**
* TABLE CLASS
* Otoanalize ait tablo yap�sn� otomatik olu�turup ��kt� al�yor. 
* 
* @author Hakan GAR�P | hgarip@otoanaliz.net
* 
* Olu�turma Tarihi : 03.07.2014
* 
*/
class Table
{
	public $imgPath 	 = "/images/";
	public $name         = '';
	public $width        = '';
	public $border       = '';
	public $cellspacing  = '';
	public $cellpadding  = '';
	public $bgcolor      = '';
	public $color        = '';
	public $className    = '';
	public $tdClassName  = '';
	public $idName       = '';
	public $align        = 'left';
	public $rowspan      = '';
	public $nowrap       = '';
	public $colspan      = '';
	public $valign       = '';
	public $scope        = '';
	public $trKontrol    = false;
	public $trOTOKontrol = true;
	public $tdColor      = 'acik';
	public $TABLE        = '';

	function __construct()
	{
		include_class( get_class() );
		$this->Temizle();
	}

	public function StandartDegerler($value='')
	{
		$this->width       = '100%';
		$this->border      = '0';
		$this->cellspacing = '1';
		$this->cellpadding = '0';
		$this->bgcolor     = '#FFFFFF';
		$this->cName       = '';
		$this->iName       = '';
		$this->align       = '';
		$this->rowspan     = '';
		$this->nowrap      = '';
		$this->colspan     = '';
		$this->valign      = '';
		$this->scope       = '';
	}

	public function OtoStandartTd()
	{	
		$this->tdColor = ($this->tdColor == 'koyu') ? 'acik' : 'koyu'; 
		$this->tdcName($this->tdColor);
		return;
	}
	public function Clear()
	{
		$this->width       = '';
		$this->border      = '';
		$this->cellspacing = '';
		$this->cellpadding = '';
		$this->bgcolor     = '';
		$this->color       = '';
		$this->className   = '';
		$this->idName      = '';
		$this->align('left');
		$this->rowspan     = '';
		$this->nowrap      = '';
		$this->colspan     = '';
		$this->valign      = '';
		$this->scope       = '';
		$this->TABLE       = '';
	}

	public function Temizle()
	{
		$this->width       = '';
		$this->border      = '';
		$this->cellspacing = '';
		$this->cellpadding = '';
		$this->bgcolor     = '';
		$this->color       = '';
		$this->className   = '';
		$this->idName      = '';
		$this->align('left');
		$this->rowspan     = '';
		$this->nowrap      = '';
		$this->colspan     = '';
		$this->valign      = '';
		$this->scope       = '';
	}

//###########################################################################
//
//
	//width="100%" border="0" cellspacing="1" cellpadding="0" bgcolor="#FFFFFF"
	//class="koyu" align="left" nowrap
 	public function width($width='')
	{
		$this->width = ' width="'.$width.'" ';
		return $this;	
	}

	public function height($height='')
	{
		$this->height = ' height="'.$height.'" ';
		return $this;	
	}
	public function border($border='')
	{
		$this->border =  ' border="'.$border.'" ';
		return $this;	
	}

	public function cellspacing($cellspacing='')
	{
		$this->cellspacing = ' cellspacing="'.$cellspacing.'" ';
		return $this;	
	}

	public function cellpadding($cellpadding='')
	{
		$this->cellpadding = ' cellpadding="'.$cellpadding.'" ';
		return $this;	
	}

	public function bgcolor($bgcolor='')
	{
		$this->bgcolor = ' bgcolor="'.$bgcolor.'" ';
		return $this;	
	}
	public function color($color='')
	{
		$this->color = ' style="color:'.$color.'" ';
		return $this;	
	}

	public function cName($name='')
	{
		$this->className =  ' class="'.$name.'" ';
		return $this;	
	} 
	
	public function tdcName($name='')
	{
		$this->tdClassName =  ' class="'.$name.'" ';
		return $this;	
	} 

	public function thcName($name='')
	{
		return  ' class="'.$name.'" ';
	} 

	public function iName($name='')
	{
		$this->idName = ' id="'.$name.'" ';
		return $this;	
	}

	/**
	 * [align description]
	 * <td align="left|right|center|justify|char"> 
	 * @param  string $align [description]
	 * @return [type]        [description]
	 */
	public function align($align='')
	{
		$this->align = ' align="'.$align.'" ';
		return $this;	
	}
	
	/**
	 * rowspan
	 * <td rowspan="number"> 
	 * @param  string $value [description]
	 * @return [type]        [description]
	 */
	public function rowspan($rowspan='')
	{
		$this->rowspan = 'rowspan="'.$rowspan.'" ';
		return $this;	
	}

	/**
	 * [nowrap description]
	 * <td nowrap>
	 * @param  string $nowrap [description]
	 * @return [type]         [description]
	 */
	public function nowrap($nowrap='')
	{
		$this->nowrap = $nowrap == true ? ' nowrap ' : '';
		return $this;	
	}

	/**
	 * [colspan description]
	 * <td colspan="number"> 
	 * @param  string $value [description]
	 * @return [type]        [description]
	 */
	public function colspan($colspan='')
	{
		$this->colspan = ' colspan="'.$colspan.'" ';
		return $this;	
	}

	/**
	 * [valign description]
	 * <td valign="top|middle|bottom|baseline"> 
	 * @param  string $valign [description]
	 * @return [type]         [description]
	 */
	public function valign($valign='')
	{
		$this->valign =  ' valign="'.$valign.'" ';
		return $this;	
	}

	/**
	 * scope
	 * <td scope="col|row|colgroup|rowgroup"> 
	 * @param  string $value [description]
	 * @return [type]        [description]
	 */
	public function scope($scope='')
	{
		$this->scope = ' scope="'.$scope.'" ';
		return $this;	
	}

	public function tdCNameKontrol()
	{
		$this->className = (!empty($this->className)) ? $this->className : $this->tdClassName;
	}

	public function getAtt()
	{
		$attr = $this->width       .
				$this->border      .
				$this->cellspacing .
				$this->cellpadding .
				$this->bgcolor     .
				$this->color       .
				$this->className   .
				$this->idName      .
				$this->align       .
				$this->rowspan     .
				$this->nowrap      .
				$this->colspan     .
				$this->valign      .
				$this->scope       ;
		return $attr;
	}
//###########################################################################


	public function trOTOKontrol($value='')
	{
		if ($value == true) {
			if ( $this->trOTOKontrol ) {
				$this->OtoStandartTd();
			}
			return;
		}
		$this->className    = '';
		$this->trOTOKontrol = $value;
	}

	public function thCNameKontrol()
	{
		$this->className = (!empty($this->className)) ? $this->className : $this->thcName('koyubaslik');
	}

	public function thOTOKontrol($value='')
	{
	
		if ( $this->trOTOKontrol ) {
			$this->thCNameKontrol();
			return;
		}
		
		$this->className    = '';
		$this->trOTOKontrol = $value;
	}


	public function table($title = '')
	{
		$returnBaslik = '';
		if (!empty($title)) {
			$returnBaslik = $this->baslik($title);
		}
		$this->TABLE = $returnBaslik.'<table'.$this->getAtt().'>'. $this->TABLE .'</table>';
		
	}

	public function trKontrol()
	{

		if ($this->trKontrol == true) {
			$this->TABLE     .= '</tr>';
			$this->className = '';
			$this->trKontrol = false;
		}
	}

	public function tr($tr = '')
	{
		$this->trKontrol();
		$this->TABLE     .= '<tr'.$this->getAtt().'>';
		$this->trOTOKontrol(true);
		$this->trKontrol = true;
		$this->Temizle();

	}

	public function th($th='')
	{
		// $this->tdCNameKontrol();
		$this->thOTOKontrol();
		$this->TABLE .= '<th '.$this->getAtt().'>'.$th.'</th>';
		$this->Temizle();
	}

	public function td($td='')
	{
		$this->tdCNameKontrol();
		$this->TABLE     .= '<td '.$this->getAtt().'>'.$td.'</td>';
		$this->Temizle();
	}

	public function create($title = '')
	{
		$this->trKontrol();
		$this->table($title);
		return $this->TABLE;
	}

	public function baslik($title)
	{
		return '	<table'.$this->getAtt().'>
						<tr>
	                        <td>
								<table border="0" cellspacing="0" cellpadding="0" bgcolor="#FFFFFF">
									<tr align="left">
				                        <td width="27"><img width="27" height="23" src="'.$this->imgPath.'baslik_img03.gif"></td>
				                        <td background="'.$this->imgPath.'baslik_img02.gif" align="center" class="tb">&nbsp;'.$title.'&nbsp;</td>
				                        <td width="43"><img width="43" height="23" src="'.$this->imgPath.'baslik_img01.gif"></td>
			                      </tr>
			                    </table>
			                </td>
			            </tr>
	                </table>
                ';
	}


// #######################################################
// 
	public function input($type = '' ,$name ='' , $value = '' , $attr = '') 
	{
		return '<input type="'.$type.'" value="'.$value.'" name="'.$name.'" ' .$attr. '>';
	}

	public function ahref($name = '', $value = '', $link ='' ,  $attr = '')
	{
		//<a href="javascript:degistir('.$Cekicikler->ID.')" class="">'.dil("De�i�tir").'</a>
		return '<a href="'.$link.'" name="'.$name.'" ' .$attr. '>'. $value .'</a>';
	}
	
	public function select($name = '', $value = array(), $selected ='' ,  $attr = '')
	{

		$return = '<select name="'.$name.'" '.$attr.' >';
		foreach ($value as $key => $value) {
			$select = ($key == $selected) ? 'selected="select"' : '';
			
			$return.='<option value="'.$key.'" '.$select.'>'.$value.'</option>';

		}
		$return .= '</select>'; 
		return $return;
	}
	
	public function textarea($name ='' , $value = '' , $attr = '') 
	{
		return '<textarea name="'.$name.'"  '.$attr.'>'.$value.'</textarea>';
	}
	
}
