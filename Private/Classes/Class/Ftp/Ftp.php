<?php 
/**
* 
*/
class Ftp {
	
	public $ftpServer   = '';
	public $ftpUser     = '';
	public $ftpPassword = '';
	public $ftpPort     = '';
	public $ftpPath     = '';
	public $Error 		= array();
	public $Kip 		= '';
	public $conn 		= '';

	function __construct()
	{
		include_class( get_class() );
		$this->setServer( Config::Ftp('FtpServer') );
		$this->setUser( Config::Ftp('FtpUser') );
		$this->setPassword( Config::Ftp('FtpPassword') );
		$this->setPort( Config::Ftp('FtpPort') );
		$this->setPath( Config::Ftp('FtpPath') );
		$this->setKip( Config::Ftp('FtpKip') );
		$returnConnection = $this->Connection();
		
		if ($this->Connection() == false) {
			return $this->getError();
		}

		return true;
	}

// -----------------------  SETS  functions  -----------------------//

	public function setServer($server='')
	{
		$this->ftpServer = $server;
	}
	public function setUser($user='')
	{
		$this->ftpUser = $user;
	}
	public function setPassword($password='')
	{
		$this->ftpPassword = $password;
	}
	public function setPort($port='')
	{
		$this->ftpPort = $port;
	}
	public function setPath($path='')
	{
		$this->ftpPath = $path;
	}
	public function setError($error='')
	{
		$this->Error[] = $error;
	}
	public function setKip($kip='')
	{
		$this->Kip = $kip;
	}
	public function setConn($conn='')
	{
		$this->conn = $conn;
	}

// -----------------------  GETS  functions  -----------------------//

	public function getServer()
	{
		return $this->ftpServer;
	}
	public function getUser()
	{
		return $this->ftpUser;
	}
	public function getPassword()
	{
		return $this->ftpPassword;
	}
	public function getPort()
	{
		return $this->ftpPort;
	}
	public function getPath()
	{
		return $this->ftpPath;
	}
	public function getError()
	{
		return $this->Error;
	}
	public function getKip()
	{
		return $this->Kip;
	}
	public function getConn()
	{
		return $this->conn;
	}

// -----------------------  OTHER  functions  -----------------------//
// 

	public function Connection()
	{
		$conn = ftp_connect($this->getServer(), $this->getPort());
		if (!$conn) {
			$this->setError('Ftp Server Connection Failed');
			return false;
		}
		if (@ftp_login($conn, $this->getUser(), $this->getPassword())) {
			$this->setConn($conn);
		    return true;
		} else {
			$this->setError('Incorrect username or password');
		    return false;
		}
	}

	public function put( $localPath = '', $newfileName = '' )
	{
		if ($localPath == '') {
			$this->setError('Yuklenecek Dosya Yok');
			return false;
		}

		if (empty($newfileName)) {
			$explodeLocalFile = explode('/', $localPath);
			$celf = count($explodeLocalFile);
			if ($celf > 1) {
				$newfileName = $explodeLocalFile[$celf-1];
			} else {
				$newfileName = $localPath;
			}
		}
		
		$fp = fopen($localPath, 'r');
		if (ftp_fput($this->getConn(), $this->getPath() . $newfileName , $fp, $this->getKip())) {
			$return = true;
		} else {
			$this->setError('Dosya Yüklenemedi');
			$return = false;
		}

		fclose($fp);
		return  $return;
	}

	public function get( $localPath = '', $newfileName = '' )
	{
		if ($localPath == '') {
			$this->setError('Yuklenecek Dosya Yok');
			return false;
		}

		if (empty($newfileName)) {
			$explodeLocalFile = explode('/', $localPath);
			$celf = count($explodeLocalFile);
			if ($celf > 1) {
				$newfileName = $explodeLocalFile[$celf-1];
			} else {
				$newfileName = $localPath;
			}
		}
		
		$fp = fopen($localPath, 'w');
		if (ftp_fget( $this->getConn(), $fp,  $this->getPath() . $newfileName, $this->getKip(), 0 )) {
			$return = true;
		} else {
			$this->setError('Dosya Alınamadı');
			$return = false;
		}

		fclose($fp);
		return  $return;
	}


	public function del( $fileName = '' )
	{
		if ( ftp_delete($this->getConn(),  $this->getPath() . $fileName) ) {
			return true;
		} 
		$this->setError($fileName. ' . Dosya Silinemedi');
		return false;
	}

	public function delTree( $folderName = '' )
	{
		$dir = ftp_nlist( $this->getConn(), $this->getPath() . $folderName );
		foreach ($dir as $key => $value) {
			if( substr( $value, 0, 1) != "." ) {
				 $this->del( $value );
			}
		}
		return true;
	}
	
	public function delDir( $remoteFolder='' )
	{
		if (ftp_rmdir($this->getConn(), $remotePath . $remoteFolder)) {
		    return true;
		} 
		$this->setError($remoteFolder . ' . Directory could not be deleted.');
		return false;
	}

	/**
	 * dName
	 * Dizin Adını Geri Döndürüyor
	 * @return boolean
	 */
	public function dName()
	{
		return ftp_pwd( $this->getConn() ); 
	}

	public function setDir( $remotePath='' )
	{
		if ( ftp_chdir( $this->getConn() ,  $remotePath ) ) {
		    return true;
		} 
		$this->setError($remotePath . ' . Directories can not be found. ');
		return false;
	}

	public function dir( $remotePath='' ,  $detail = false)
	{
		if ( $detail ) {
			return ftp_rawlist( $this->getConn(), $this->getPath() . $remotePath );
		}
		return ftp_nlist( $this->getConn(), $this->getPath() . $remotePath );
	}

	public function rename($newName = '' , $fileName = '')
	{
		if (ftp_rename( $this->getConn(), $this->getPath() . $fileName, $this->getPath() . $newName) ) {
			return true;
		} 
		$this->setError($fileName . ' . Name could not be changed. ');
		return false;
	}



}
