<?php 

/**
* 
*/
class HtmlAtt
{
	function __construct()
	{
		include_class( get_class() );
	}

	public function input($type = '' ,$name ='' , $value = '' , $attr = '') 
	{
		return '<input type="'.$type.'" value="'.$value.'" name="'.$name.'" ' .$attr. '>';
	}

	public function ahref($name = '', $value = '', $link ='' ,  $attr = '')
	{
		//<a href="javascript:degistir('.$Cekicikler->ID.')" class="">'.dil("Değiştir").'</a>
		return '<a href="'.$link.'" name="'.$name.'" ' .$attr. '>'. $value .'</a>';
	}
	
	public function select($name = '', $value = array(), $selected ='' ,  $attr = '')
	{
		$return = '<select name="'.$name.'" '.$attr.' >';
		foreach ($value as $key => $value) {
			$select ='';
			if (is_array($selected)) {
				if ( in_array($key , $selected) ) {
					$select = 'selected="select"';
				}
			} else{
				$select = ($key == $selected) ? 'selected="select"' : '';
			}
			$return.='<option value="'.$key.'" '.$select.'>'.$value.'</option>';

		}
		$return .= '</select>'; 
		return $return;
	}
	
	public function textarea($name ='' , $value = '' , $attr = '') 
	{
		return '<textarea name="'.$name.'" id="'.$name.'" '.$attr.'>'.$value.'</textarea>';
	}

	

}
