<?php

/**
*
*/
/*
		$pingo = new Pingo;
		$pingo->Data( 'http://belge' , 'BELGE');
		$pingo->Ping(); 
		OR 
		$pingo->Urls(array('http://blogsearch.google.com/ping/RPC2' , 'http://ping.bloggers.jp/rpc/','http://blogsearch.google.tw/ping/RPC2'));

		
 */
class Pingo
{
	public $xml = '';
	// public $method = array( 'weblogUpdates.ping' , 'weblogUpdates.extendedPing' );

	public $ping         ='';
	public $extendedPing ='';
	public $Ping_Data    = '';

	function __construct()
	{
		include_class( get_class() );
		$this->xml  = '
	        <?xml version="1.0"?>
		        <methodCall>
			        <methodName>[##METHOD##]</methodName>
			        <params>
				        <param><value><string>[##SITE##]</string></value></param>
				        <param><value><string>[##URL##]</string></value></param>
			        </params>
		        </methodCall>
	    ';
	}

	/**
	 * Data
	 * @param string $url
	 * @param string $site
	 */
	public function Data($url='' , $site = '')
	{
		$xml = $this->xml;
		$xml = str_replace( '[##METHOD##]', 'weblogUpdates.ping', $xml);
		$xml = str_replace( '[##SITE##]', $site, $xml);
		$this->ping = str_replace( '[##URL##]', $url, $xml);

		$xml = $this->xml;
		$xml = str_replace( '[##METHOD##]', 'weblogUpdates.extendedPing', $xml);
		$xml = str_replace( '[##SITE##]', $site, $xml);
		$this->extendedPing = str_replace( '[##URL##]', $url, $xml);

	}

	public function Urls( $urls = '' )
	{
		$i = 1;
		if (is_array($urls)) {
			foreach ($urls as $key => $value) {
				$Ping_Data['URL'][$i]  = $value ;
				$Ping_Data['DATA'][$i] =  $this->ping;
				$i++;
				$Ping_Data['URL'][$i]  =  $value ;
				$Ping_Data['DATA'][$i] =  $this->extendedPing;
				$i++;

			}
		} else {
			$Ping_Data['URL'][$i]  = $urls ;
			$Ping_Data['DATA'][$i] =  $this->ping;
			$i++;
			$Ping_Data['URL'][$i]  = $urls ;
			$Ping_Data['DATA'][$i] =  $this->extendedPing;
		}
		$this->Ping_Data = $Ping_Data;

		$this->exec();
	}

	public function Ping()
	{
		$db = new Db;
		$db->where('hatakodu', 'false');
		$db->table('PING');
		$db->select();
		$result = $db->result();
		if (empty($result)) {
			return false;
		}
		foreach ($result as $key => $value) {
			$urls[] = $value['ping'];
		}
		$this->Urls($urls);
		return true;
	}

	public function exec()
	{
		if ( Config::Ping_Control() == 'TRUE') {
			$mc        = new MultiCurl;
			$db        = new Db;
			$mc->clear();
			$mc->setPost($this->Ping_Data['DATA']);
			$mc->setPages($this->Ping_Data['URL']);
			$mc->setStatus(TRUE);
			$mc->exec();
			$get = $mc->getData() ;
			$status = $get['status'];

			foreach ($status as $key => $value) {
				if (substr($value['http_code'],0,1) != '2' ){

					$db->where( 'PING' , $value['url'] );
					$db->data( 'hatakodu' , 'true' );
					$db->table('PING');
					$db->update();

				}
			}
		}
	}

}
