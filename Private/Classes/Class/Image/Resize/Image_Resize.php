<?php

/**
* Hakan GARİP | i@hakangarip.com.tr
* 20-08-2014
*/

/*
	------ KULLANIMI  ------

	$rs = new Image_Resize;
    $rs->ImagePath('../Upload/dokuman/Original/12_qqq.jpeg');
    $rs->NewWidth('800');
    $rs->NewHeight('600');
    $rs->Crop_XY('100' , '200');
    $rs->NewImagePath('../Upload/dokuman/Original/15.jpg');

	------ WATERMARK KULLANIMI  ------
    $rs->WaterMarkText( 'Hakan Garip' , '190' , '150');
    $rs->Angle( '45' );
    $rs->Alpha( '100' );
	$rs->FontSize('25');
    $rs->Resize();
	
*/
class Image_Resize
{
	public $Width;
	public $Height;
	public $NewWidth;
	public $NewHeight;
	public $ImagePath;
	public $NewImagePath;
	public $Quality       = 100;
	public $Crop_x        = 0;
	public $Crop_y        = 0;
	public $Crop          = FALSE;
	public $WaterMarkText = '';
	public $FontSize      = '12';
	public $Font          = 'arial.ttf';
	public $Angle         = '0';
	public $ColorRed      = '255';
	public $ColorGreen    = '255';
	public $ColorBlue     = '255';
	public $WaterMarkX    = '0';
	public $WaterMarkY    = '0';
	public $Alpha         = 0;
	public $Filter        =FALSE;
	public $Repeat        = FALSE;
	
	function __construct()
	{
		include_class( get_class() );
	}


	/**
	 * Resize
	 * Daha önceden setlenmiş olan değerlere göre yeni resim oluşturulup kayıt ediliyor.
	 */
	public function Resize()
	{

		if ( $this->Crop ) {
			$this->NewWidth( $this->NewWidth );
	    	$this->NewHeight( $this->NewHeight );
		}

		$File     = new File;
		// echo 'a';
		// prpe($this->ImagePath );
		$mime = mime_content_type($this->ImagePath);
		// prpe($mime);
		$FileType = $File->getType( $mime );
		// echo 'F:' . $FileType ;
		switch ( $FileType )
		{
			case 'gif' :	$img = @imagecreatefromgif( $this->ImagePath ); break;
			case 'jpeg':	$img = @imagecreatefromjpeg( $this->ImagePath ); break;
			case 'jpg':		$img = @imagecreatefromjpeg( $this->ImagePath ); break;
			case 'png' :	$img = @imagecreatefrompng( $this->ImagePath ); break;
		}

		$newImageCreate	= imagecreatetruecolor( $this->NewWidth, $this->NewHeight );
		$background		= imagecolorallocate($newImageCreate,255,255,255);
		imagefill($newImageCreate,0,0,$background);

		$Width     = $this->Width;
		$Height    = $this->Height;
		$NewWidth  = $this->NewWidth;
		$NewHeight = $this->NewHeight;

		if ( $this->Crop ) {
			$Width     = $this->NewWidth;
			$Height    = $this->NewHeight;
			$NewWidth  = $this->NewWidth;
			$NewHeight = $this->NewHeight;
		}

		$crop_x = $this->Crop_x;
		$crop_y = $this->Crop_y;
		
		// prpe($newImageCreate);
		// prpe($img);
		// prpe($crop_x);
		// prpe($crop_y);
		// prpe($this->NewWidth);
		// prpe($this->NewHeight);
		// prpe($Width);
		// prpe($Height);

		imagecopyresampled(
				$newImageCreate,
				$img,
				0, 0,
				$crop_x, $crop_y,
				$this->NewWidth, $this->NewHeight,
				$Width, $Height
			);

		if ( $this->Filter ) {
			
			for ($i=0; $i < 40 ; $i++) { 
				imagefilter($newImageCreate, IMG_FILTER_GAUSSIAN_BLUR);
			}	
			imagefilter($newImageCreate, IMG_FILTER_SELECTIVE_BLUR);
			imagefilter($newImageCreate, IMG_FILTER_PIXELATE,5 , true);	
		}
		
		if ( !empty($this->WaterMarkText) ) {
			 imagettftext($newImageCreate, $this->FontSize, $this->Angle, $this->WaterMarkX, $this->WaterMarkY, $this->WaterMarkTextColor($newImageCreate), $this->getFont(), $this->WaterMarkText);
			 // $a = imagettfbbox($this->FontSize, $this->Angle , $this->getFont(), $this->WaterMarkText);
			 // prpe($a);
		}

		switch ( $FileType ) {
			case 'gif' :	imagegif($newImageCreate,$this->NewImagePath); break;
			case 'jpeg':	imagejpeg($newImageCreate,$this->NewImagePath,$this->Quality);break;
			case 'png' :	imagepng($newImageCreate,$this->NewImagePath); break;
		}
		imagedestroy($img);
		imagedestroy($newImageCreate);
		$this->clear();

	}

	/**
	 * Width
	 * Kaynak Resmin Genişliği Ayarlanıyor
	 * @param string veya integer $Width Genişlik Değeri
	 */
	public function Width($Width = '')
	{
		$this->Width = $Width;
	}

	/**
	 * Height
 	 * Kaynak Resmin Yüksekliği Ayarlanıyor
	 * @param string veya integer  $Height Yukseklik değeri
	 */
	public function Height($Height = '')
	{
		$this->Height = $Height;
	}

	/**
	 * NewWidth
	 * Oluşturulacak resmin genişliği
	 * Eğer değer belirtilmemişse kaynbak resmin genişliği alınıyor.
	 * @param string veya integer $NewWidth Genişlik Değeri
	 */
	public function NewWidth($NewWidth = '')
	{
		if (empty($NewWidth)) {
			$this->NewWidth = $this->Width;
			return;
		}
		$this->NewWidth = $NewWidth;
	}

	/**
	 * NewHeight
	 * Oluşturulacak resmin Yüksekliği
	 * Eğer değer belirtilmemişse kaynbak resmin yükseliği alınıyor.
	 * @param string veya integer $NewHeight
	 */
	public function NewHeight($NewHeight = '')
	{
		if (empty($NewHeight)) {
			$this->NewHeight = $this->Height;
			return;
		}
		$this->NewHeight = $NewHeight;
	}

	/**
	 * ImagePath
	 * Kaynak Resmin Yolu ve ismi
	 * @param string $ImagePath
	 */
	public function ImagePath( $ImagePath='' )
	{
		// echo $ImagePath;
		list($Width,$Height,$Type) = getimagesize( $ImagePath );
		$this->Width($Width);
		$this->Height($Height);
		$this->ImagePath = $ImagePath;

	}

	/**
	 * NewImagePath
	 * Oluşturulacak Resmin Yolu ve ismi
	 * @param string $NewImagePath
	 */
	public function NewImagePath( $NewImagePath='' )
	{
		$this->NewImagePath = $NewImagePath;
	}

	/**
	 * Quality
	 * Oluşturulacak resmin genel resim kalitesi değeri
	 * @param string $Quality
	 */
	public function Quality( $Quality='100' )
	{
		$this->Quality = $Quality;
	}

	/**
	 * Crop_XY
	 * Kaynak resimden kesilecek olan kısmın
	 * başlangıç x ve y koordinatları
	 * @param string $crop_x X Koordinatı
	 * @param string $crop_y Y Koordinatı
	 */
	public function Crop_XY( $crop_x='', $crop_y='' )
	{
		$this->Crop   = TRUE;
		$this->Crop_x = $crop_x;
		$this->Crop_y = $crop_y;
	}

	/**
	 * setImageLength
	 * Oluşturulacak olan resmin genişlik ve yüksekliğine göre oranı alınıyor.
	 *
	 */
	public function setImageLength()
	{
		$width  = $this->Width;
		$height = $this->Height;
		$newW   = $this->NewWidth;
		$newH   = $this->NewHeight;

		/// ORAN HESAPLANIYOR -----------------------
		$oran1			= $width/$height;
		$oran_yuk_gen	= $newW/$newH;
		if($oran_yuk_gen>$oran1)
		{
			if( $height < $newH )	{
				$yeniyukseklik	= $height;
				$yenigenislik	= $width;
			} else {
				$yeniyukseklik	= $newH;
				$yenigenislik	= ($newH*$width)/$height;
			}

		} else {
			if($width<$newW)	 {
				$yeniyukseklik	= $height;
				$yenigenislik	= $width;
			} else {
				$yenigenislik	= $newW;
				$yeniyukseklik	= ($newW*$height)/$width;
			}
		}
		$this->NewWidth  = $yenigenislik;
		$this->NewHeight = $yeniyukseklik;
	}

	/**
	 * WaterMarkText
	 * @param string $text Yazı
	 * @param string $x Sol üst köşesinden x uzaklığı 
	 * @param string $y Sol üst köşesinden y uzaklığı 
	 * 
	 */
	public function WaterMarkText( $text=''  , $x = 0 , $y = 0)
	{
		$this->WaterMarkText = $text;
		$this->WaterMarkX    = $x;
		$this->WaterMarkY    = $y;
	}



	/**
	 * FontSize
	 * Watermark font size
	 * @param string $fontsize
	 */
	public function FontSize($fontsize='12')
	{
		$this->FontSize = $fontsize;
	}

	/**
	 * Font
	 * Watermark font
	 * @param string $font 
	 */
	public function Font($font='arial.ttf')
	{
		$this->Font = $font;
	}

	public function getFont()
	{
		return  DR . SLASH .ASSETS.SLASH. 'fonts' . SLASH . $this->Font;
	}
	/**
	 * Angle
	 * Yazının açısını ayarlıyoruz.
	 * @param string $angle 
	 */
	public function Angle($angle='')
	{
		$this->Angle = $angle;
	}

	public function Alpha($alpha='0')
	{
		$this->Alpha = $alpha;
	}

	public function Repeat($repeat=false)
	{
		$this->Repeat = $repeat;
	}

	/**
	 * Color
	 * yazının rengini berlitirip  image ekliyoruz
	 *
	 * @param string $red   
	 * @param string $green 
	 * @param string $blue  
	 */
	public function Color( $red='255', $green='255', $blue ='255' )
	{
		$this->ColorRed   = $red;
		$this->ColorGreen = $green;
		$this->ColorBlue  = $blue;
	}

	public function WaterMarkTextColor($image)
	{
		return imagecolorallocatealpha($image, $this->ColorRed , $this->ColorGreen, $this->ColorBlue , $this->Alpha);
		// return imagecolorallocate($image, $this->ColorRed , $this->ColorGreen, $this->ColorBlue);
	}


	/**
	 * WaterMarkImage
	 * @param string $path resim yolu
	 */
	public function WaterMarkImage( $path='' )
	{

	}

	public function Filter($filter  = FALSE)
	{
		$this->Filter = $filter;
	}
	public function clear()
	{
		$this->Quality       = 100;
		$this->Crop_x        = 0;
		$this->Crop_y        = 0;
		$this->Crop          = FALSE;
		$this->WaterMarkText = '';
		$this->FontSize      = '12';
		$this->Font          = 'arial.ttf';
		$this->Angle         = '0';
		$this->ColorRed      = '255';
		$this->ColorGreen    = '255';
		$this->ColorBlue     = '255';
		$this->WaterMarkX    = '0';
		$this->WaterMarkY    = '0';
		$this->Alpha         = '0';
		$this->Filter        = FALSE;
		$this->Repeat        = FALSE;

	}
}
