<?php

/**
*
*/
class Users
{

	function __construct()
	{
		include_class( get_class() );
	}

	public function LoginPageValid()
	{

		if( $this->LoginValid() == 'TRUE' ) {
			$location = Config::SitePath();
			header('Location: '. $location);
			exit();
		}

	}

	public function UserLoginValid( $AutoReferer = 'TRUE' )
	{

		if( $this->LoginValid() != 'TRUE' ) {
			if ($AutoReferer == 'FALSE') {
				return 'FALSE';
			}
			$location = Config::SitePath();
			header('Location: '. $location);
			exit();
		}
		return 'TRUE';
	}

	/**
	 * UserLoginImageDownloadValid
	 * Dosya indirmeye yetkisi varmı diye bakılıyor
	 */
	public function UserLoginImageDownloadValid()
	{

		if( $this->LoginValid() != 'TRUE' ) {
			exit(L10n::T('İndirme Yetkiniz Bulunmamaktadır.'));
		}

	}

	/**
	 *
	 * Dosya indirmeye yeterli kadar kredisi varmı diye bakılıyor.
	 */
	public function UserImageDownloadCrediValid($Anasayfa_Dokumanlar = '')
	{

		
		if ( Config::User_Kredi() < $Anasayfa_Dokumanlar['DOKUMAN_FIYATI'] ) 
		{
			$Sess = new Sess;
			$Sess->set('DOWNLOAD' ,  array('Alert' =>L10n::T('Bu dökümanı indirmek için yeterli krediniz bulunmamaktadır!') ));
			$pager = new Pager;
			$pager->setPage('LANGUAGE');
			$pager->Values( FALSE );
			$pager->model();
			exit();
		}

	}

	public function LoginValid()
	{
		$Sess = new Sess;
		if( !$Sess->get('USER') ) {
			$Sess->set( array('USER' => array( 'LOGIN' => 'FALSE' )) );
			return 'FALSE';
		}
		$User = $Sess->get('USER');
		return $User['LOGIN'];
	}

	public function LoginControl($value='')
	{
		$R        = new Request;
		$email    = $R->get('email');
		$password = $R->get('password');
		$return   = FALSE;
		if ( !empty($email) AND !empty($password) ) {
			$DB   = new Db;
			$Sess = new Sess;
			$Sess->destroy('USER');
			/*
				Email Valid
			*/
			$Valid = new Valid;
			if ( !$Valid->email($email) ) {
				$USER_SESS['LOGIN']        = 'FALSE';
				$USER_SESS['ALERT']['LOGIN'] =  L10n::T('Geçerli Email Adresi Giriniz');
				$Sess->set('USER',$USER_SESS);
				return false;
			}

			$password = Encrypt::Md5($password);
			$DB->where( array('USER_EMAIL', 'USER_PASSWORD') , array($email , $password) );
			$DB->table('USERS');
			$DB->select();
			$Result_Users = $DB->result();
			if ( empty($Result_Users) ) {
				$USER_SESS['LOGIN']        = 'FALSE';
				$USER_SESS['ALERT']['LOGIN']  = L10n::T('Incorrect email or password');
			} else {
				if ( $Result_Users[0]['STATUS'] == 'FALSE' OR $Result_Users[0]['USER_EMAIL_CHECK'] == 'FALSE' ) {

					if ( $Result_Users[0]['STATUS'] == 'FALSE' ) {
						$USER_SESS['LOGIN']        = 'FALSE';
						$USER_SESS['ALERT']['LOGIN']  = L10n::T('Aktif Olmayan Kullanıcı');
					}
					if ( $Result_Users[0]['USER_EMAIL_CHECK'] == 'FALSE' ) {
						$USER_SESS['LOGIN']        = 'FALSE';
						$USER_SESS['ALERT']['LOGIN']  = L10n::T('Aktifleştirilmemiş mail adresi');
					}

				} else {
					$return = TRUE;
					$USER_SESS['ID']           = $Result_Users[0]['ID'];
					$USER_SESS['NAME']         = $Result_Users[0]['USER_NAME'];
					$USER_SESS['SURNAME']      = $Result_Users[0]['USER_SURNAME'];
					$USER_SESS['EMAIL']        = $Result_Users[0]['USER_EMAIL'];
					$USER_SESS['LOGIN']        = 'TRUE';
					$USER_SESS['CREATION_TIME'] = Now;
					$USER_SESS['END_TIME']      = Config::UserSessEndTime();
				}
			}
			$Sess->set('USER',$USER_SESS);
		}
		return $return;

	}

	public function Logout()
	{
		$Sess = new Sess;
		$Sess->destroy('USER');
		$USER_SESS['LOGIN']        = 'FALSE';
		$Sess->set('USER',$USER_SESS);
		return;
	}

	public function LoginAlert()
	{
		$Sess = new Sess;
		$User = $Sess->get('USER');
		if (isset($User['ALERT']['LOGIN'])) {
			return $User['ALERT']['LOGIN'];
		}
		return 'FALSE';
	}

	public function CreateUser($value='')
	{
		$R    = new Request;
		$Sess = new Sess;
		$Sess->destroy('USER_CREATE');
		$Sess->destroy('USER');
		$email      = $R->get('email');
		$password   = $R->get('password');
		$Name       = $R->get('Name');
		$SurName    = $R->get('SurName');
		$newslatter = $R->get('newslatter');

		$return              = FALSE;
		$ALERT['VALIDATION'] = 'FALSE';
		$ALERT['LOGIN']      = 'FALSE';

		if ( !empty($email) AND !empty($password) AND !empty($Name) AND !empty($SurName) ) {
			/*
				Email Valid
			*/
			$Valid = new Valid;
			if ( !$Valid->email($email) ) {
				$ALERT['ALERT']['CREATE'] =  L10n::T('Geçerli Email Adresi Giriniz');
				$Sess->set('User',$ALERT);
				return false;
			}

			$DB   = new Db;
			$password = Encrypt::Md5($password);

			/*
				Databasede Daha önce kayıtlı email adresine bakılıyor.
			*/
			$DB->where( array('USER_EMAIL') , array($email) );
			$DB->table('USERS');
			$DB->select();
			$Result_Email = $DB->result();

			if ( !empty($Result_Email) ) {
				$ALERT['EMAIL'] =  L10n::T('Kayıtlı Email');
			} else {
				$data['key'][]    = 'USER_NAME';
				$data['key'][]    = 'USER_SURNAME';
				$data['key'][]    = 'USER_EMAIL';
				$data['key'][]    = 'USER_PASSWORD';
				$data['key'][]    = 'USER_EMAIL_CHECK_CODE';
				$data['data'][]   = $Name;
				$data['data'][]   = $SurName;
				$data['data'][]   = $email;
				$data['data'][]   = $password;
				$email_check_code = Encrypt::Hash( $email.Now.uniqid($email.Now) );
				$data['data'][]   = $email_check_code;

				$DB->data($data['key'], $data['data']);
				$DB->table( 'USERS' );
				$DB->insert();
				if ( !empty($newslatter) ) {
					$Insert_Id = $DB->lastInsertId();
					$Bulten = new Site_Bulten;
					$Bulten->Create($email , $Insert_Id);
				}
				$this->User_Email_Check_Code_Email_Send( $email ,$Name , $SurName,  $email_check_code );

				
				$ALERT['MESSAGE']['SUCCESS'] =  L10n::T('Kayıt Basarili');
				$ALERT['VALIDATION']         = 'TRUE';
				$Sess->set('USER',$ALERT);
				return TRUE;

			}

		}

		if ( empty($email)) {
			$ALERT['ALERT']['EMAIL'] =  L10n::T('Required EMAIL');
		}
		if ( empty($password)) {
			$ALERT['ALERT']['PASSWORD'] =  L10n::T('Required Password');
		}
		if ( empty($Name)) {
			$ALERT['ALERT']['NAME'] =  L10n::T('Required Name');
		}
		if ( empty($SurName)) {
			$ALERT['ALERT']['SURNAME'] =  L10n::T('Required Surname');
		}

		$Sess->set('USER_CREATE',$ALERT);
		return $return;
	}

	public function User_Email_Check_Code_Email_Send($user_email , $Name , $SurName , $code)
	{
		$mail             = new Mail;
		$user_email_crypt = Encrypt::Hash( $user_email );
		$dogrulama_linki  = Config::SitePath() . 'email_verification/' . $user_email_crypt . '/'. $code ;
		$html             = 'Hoşgeldiniz ' .  $Name . ' ' . $SurName . '
			email Doğrulama linki   <a href="'.$dogrulama_linki.'" >Doğrula</a>
		';
		$mail->html($html);
		$mail->from_name( Config::SiteEmailName() );
		$mail->from_email( Config::SiteEmail() );
		$mail->subject( L10n::T('Yeni Üyelik Email Doğrulama') );
		$mail->to(
					
						array(
							'email' => $user_email,
			                'name' =>  $Name . ' ' . $SurName,
			                'type' =>'to',

			               	)
					
				);
		$mail->headers(array('Reply-To' => Config::SiteEmailName() ));
		$result = $mail->send();
	}

	public function CreateAlert()
	{
		$Sess = new Sess;
		$User = $Sess->get('USER_CREATE');
		if ( !$User  ) {
			return 'FALSE';
		}
		return $User;
	}
}
