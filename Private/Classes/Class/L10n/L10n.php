<?php 

/**
* 
*/
class L10n
{
	function __construct()
	{
		include_class( get_class() );
	}

	public static function T( $value='' )
	{
		if ( Config::TraslateCache() ) {
			$Cache_Name = 'TRANSLATE_KEYWORDS';
			$keys       = Config::Cache( $Cache_Name , Config::LanguageId() );
			if ( !$keys ) { return $value; }
			if ( isset($keys[$value]) ) { return $keys[$value]; }
			return $value;
		}
		include DR.'/Settings/Languages/'.Config::Language().'.php';
		if (!isset($L10n[$value])) {	
			return $value;
		}
		return $L10n[$value];
	}

}
