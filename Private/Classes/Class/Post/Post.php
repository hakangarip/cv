<?php 

/**
* 
*/
class Post
{
	public $allVariables = array();
	function __construct()
	{
		include_class( get_class() );
		if ( $_POST ) {
			$post = $_POST;
			if ( count($post) > 0) {
				
				foreach ($post as $key => $value) {
					 $return[$key] = $value;
				}
				$this->allVariables = $return;
				global $htmlLog;
				$logClass = str_replace( '[*title*]', 'Posts' , Config::HtmlLog('Request') );
				$logClass = str_replace( '[*Name*]', prpl($return)  , $logClass );
				$htmlLog->Log( $logClass );
			}
			return false;
		}
		
		return false;
	}

	public function allGet()
	{
		return $this->allVariables;
	}

	public function get( $get='' )
	{
		if (isset($this->allVariables[$get])) {
			return $this->allVariables[$get];
		}
		return false;
	}

}
