<?php 
/**
* 
*/
class MultiCurl 
{
	public  $Agent     = 'Googlebot/2.1 (http://www.googlebot.com/bot.html)';
	public  $UserAgent = '';
	public  $Pages     = '';
	public  $Return    = array();
	public  $Data      = '';
	public  $Status    = FALSE;
	function __construct()
	{
		include_class( get_class() );
		$this->UserAgent = $this->Agent;
	}

	/**
	 * userAgent 
	 * Curl yapılırken hangi agent üzerinden okunacağını belirtiyor. 
	 * Default olarak google bot geliyor. 
	 * @param  string $agent [description]
	 * @return [type]        [description]
	 */
	public function userAgent($agent='')
	{
		$this->UserAgent = ($agent=='') ? $this->Agent : $agent;
	}

	/**
	 * [setPages description]
	 * @param string $pages [description]
	 */
	public function setPages($pages='')
	{
		$this->Pages = $pages;
	}

	public function setPost($value='')
	{
		$this->Data = $value;
	}

	public function setStatus( $status=FALSE )
	{
		$this->Status = $status;
	}

	public function getPost($id)
	{
		if ( isset($this->Data[$id]) ) {
			return $this->Data[$id];
		}
		return '';
	}

	/**
	 * [exec description]
	 * @return [type] [description]
	 */
	public function exec()
	{
		$mh            = curl_multi_init(); 
		$curl_array    = array(); 
		
		foreach($this->Pages as $i => $url) { 

            $curl_array[$i]	= curl_init($url); 
            curl_setopt($curl_array[$i], CURLOPT_USERAGENT, $this->UserAgent);
          	curl_setopt($curl_array[$i], CURLOPT_URL,$url);
			curl_setopt($curl_array[$i], CURLOPT_FOLLOWLOCATION, 0);
            curl_setopt($curl_array[$i], CURLOPT_RETURNTRANSFER, true); 
            curl_setopt($curl_array[$i], CURLOPT_ENCODING , "gzip");
            // curl_setopt($curl_array[$i], CURLOPT_SSL_VERIFYPEER , 0); 
            curl_setopt($curl_array[$i], CURLOPT_POST , true); 
			curl_setopt($curl_array[$i], CURLOPT_POSTFIELDS,  $this->getPost($i));
            curl_multi_add_handle($mh, $curl_array[$i]); 
        }

        $running = NULL; 
        do { 
            curl_multi_exec($mh,$running); 
        } while($running > 0); 

        $result = array(); 
        foreach($this->Pages as $i => $url) { 
            
            if ( $this->Status ) {
            	$result['status'][$i]= curl_getinfo($curl_array[$i]);
            }
            
            $result[$i]	= curl_multi_getcontent($curl_array[$i]); 
            curl_multi_remove_handle($mh, $curl_array[$i]); 
        } 
        
        curl_multi_close($mh); 
        $this->Return = $result;
		return $this->Return;
	}

	public function getData()
	{
		return $this->Return;
	}

	public function clear()
	{
			$this->UserAgent = $this->Agent;	
			$this->Pages     = '';
			$this->Return     = array();
	}
	public function returntest()
	{
		return $this;
	}
}
