<?php 

/**
* 
*/

class Db_Drivers_Mysql extends Db
{
	
	public $ServerName       = '';
	public $DatabaseName     = '';
	public $DatabaseUser     = '';
	public $DatabasePassword = '';
	public $DatabasePort     = '';
	public $CharacterSet     = '';
	public $Collation        = '';
	public $Prefix           = '';
	public $SqlConnect       = array();
	public $SqlSelect        = array();
	public $Functions        = array();

	public $TableName = '';

	
	function __construct($config)
	{
		include_class( get_class() );
		// var_dump($config);
		$this->setServerName ($config['ServerName']);
		$this->setDatabaseName ($config['DatabaseName']);
		$this->setDatabaseUser ($config['DatabaseUser']);
		$this->setDatabasePassword ($config['DatabasePassword']);
		$this->setDatabasePort ($config['DatabasePort']);
		$this->setCharacterSet ($config['CharacterSet']);
		$this->setCollation ($config['Collation']);
		$this->setPrefix ($config['Prefix']);
		return $this;

	}

	public  function connection()
	{
		try {
				$port=(($this->getDatabasePort())) ?  ";port:".$this->getDatabasePort()."": '';
				$pdo_conf = "mysql:host=".$this->getServerName().$port.";dbname=".$this->getDatabaseName().";charset=".$this->getCharacterSet()."";
				
				$SqlConnect = new PDO($pdo_conf,$this->getDatabaseUser(),$this->getDatabasePassword());
					if(!$SqlConnect) {

					throw new Exception('Bilgiler Hatalı');
				}

		} catch (Exception $e) {
			prpe($e);
			$this->setError('Database Hatası :'.$e);
			return false;
		}
		$this->setSqlConnect($SqlConnect);
		// $this->setSqlSelect($SqlSelect);
		$this->setMessage('Database Bağlantısı Sağlandı...');
		return $this;
		
	}

/* 					SETS FUNCTIONS 						*/
/* -----------------------------------------------------*/

	private function setSqlConnect($SqlConnect ='')
	{
		$this->SqlConnect = $SqlConnect;
	}

	private function setSqlSelect($SqlSelect='')
	{
		$this->SqlSelect = $SqlSelect;
	}

	private function setServerName($ServerName ='')
	{
		if (empty($ServerName)) {
			$this->setError('Server Adı Hatalı');
			return false;
		}
		$this->ServerName = $ServerName;
		return true;
	}

	private function setDatabaseName($DatabaseName ='')
	{
		if (empty($DatabaseName)) {
			$this->setError('Database Adı Hatalı');
			return false;
		}
		$this->DatabaseName = $DatabaseName;
		return true;
	}

	private function setDatabaseUser($DatabaseUser ='')
	{
		if (empty($DatabaseUser)) {
			$this->setError('Database Kullanıcı Adı Girilmemiş');
			return false;
		}
		$this->DatabaseUser = $DatabaseUser;
		return true;
	}

	private function setDatabasePassword($DatabasePassword ='')
	{
		if (empty($DatabasePassword)) {
			$this->setError('Database Şifresi Girilmemiş');
			return false;
		}
		$this->DatabasePassword = $DatabasePassword;
		return true;
	}

	private function setDatabasePort($DatabasePort = '3306')
	{
		$this->DatabasePort = $DatabasePort;
		return true;
	}

	private function setCharacterSet($CharacterSet = 'utf8')
	{
		$this->CharacterSet = $CharacterSet;
		return true;
	}

	private function setCollation($Collation = 'utf8_unicode_ci')
	{
		$this->Collation = $Collation;
		return true;
	}

	public function setPrefix($Prefix = '')
	{
		$this->Prefix = $Prefix;
		return true;
	}



/* 					GETS FUNCTIONS 						*/
/* -----------------------------------------------------*/

	public function getSqlConnect()
	{
		return $this->SqlConnect;
	}

	private function getSqlSelect()
	{
		return $this->SqlSelect;
	}
	private function getServerName()
	{
		return $this->ServerName;
	}

	private function getDatabaseName()
	{
		return  $this->DatabaseName;
	}

	private function getDatabaseUser()
	{
		return $this->DatabaseUser;
	}

	private function getDatabasePassword()
	{
		return $this->DatabasePassword;
	}

	private function getDatabasePort()
	{
		return $this->DatabasePort;
	}
	
	private function getCharacterSet()
	{
		return $this->CharacterSet;
	}
	private function getCollation()
	{
		return $this->Collation;
	}

	public function getPrefix()
	{
		return $this->Prefix;
	}


}