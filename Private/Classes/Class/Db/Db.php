<?php
/**
 * PDO Database Connection Class
 *
 * @author Hakan Garip | i@hakangarip.com.tr
 *
 */

/*


		$db = new DB;
		$db->table('TEST_TABLE');
		$db->other(' (');
		$db->where('ROW2' , 'GARIP');
		$db->where('ROW22' , '2GARIP');
		$db->other(')');
		$db->orWhere('ROW3' , 'Gar3');
		$db->whereIn('ROW5' , array('asd','fdsa') ,'OR');
		$db->orWhere('ROW4' , 'Gar3');
		$db->where('ROW1' , 'HAKAN' );
		$db->like('ADI' , 'HAAN'  ,'OR');
		$db->between('ADI' , '1' ,'4' , 'OR' );

		$db->whereNotIn('ROW6' , array('asd','fdsa') ,'OR');
		$db->orderby('ADI' , 'DESC');
		$db->limit('2' , 2);
		$db->groupby('ADI');
		$db->select();

		prpe( $db->lastQuery() );


*/
class Db
{
	public $DriverPath    = 'Drivers';
	public $Errors        = '';
	public $Message       = '';
	public $TableName     = '';
	public $SqlQuery      = '';
	public $SqlQueryWhere = '';
	public $whereControl  = 0;
	public $returnData    = array();
	public $executingTime = '';
	public $operator      = ' AND ';
	public $where         = '';
	public $keys          = '';
	public $walues        = '';
	public $orderby       = '';
	public $limit         = '';
	public $join          = '';
	public $groupby       = '';
	public $between       = '';
	public $whereIn       = '';
	public $having        = '';
	public $like          = '';
	public $lastInsertId  = '';
	public $multi         = false;
	public $values        = array();

	function __construct($arraySettings = '')
	{
		include_class( get_class() );
		$this->Config(Config::Db());
		if (!empty($arraySettings)) {

			if (isset($arraySettings)) {
				$this->setConfig($arraySettings);
			}

			foreach ($arraySettings as $key => $value) {
				switch ($key) {
					case 'Prefix':
					case 'Driver':
						$keyFunc = 'set'.$key;
						$this->$keyFunc($value);
						break;


				} // Switch End
			} // Foreach End
		}  // If Empty End

		if(!$this->includeSystem()){
			var_dump($this->getError());
			exit();
		}
		return true;
	}

	private function Config( $db_config ){
		if( !$this->setDriver( Config::DatabaseDriver() ) ){
			return false;
		}
		$this->config = $db_config[Config::DatabaseDriver()];
		$this->setPrefix( $db_config[$this->getDriver()]['Prefix'] );

		return true;
	}

	private function setConfig( $db_config )
	{
		foreach ($db_config as $key => $value) {
			$this->config[$key]  = $value;
		}

		if(!$this->setDriver( $db_config['Driver']) ){
			return false;
		}

		return true;
	}
	public function getConfig()
	{
		$this->config['Prefix'] = $this->getPrefix();
		return $this->config;
	}

	public function includeSystem()
	{

		$includeClassName = __CLASS__.'_'.$this->DriverPath.'_'.$this->getDriver();

		$this->e = new $includeClassName($this->getConfig());
		if ( ! $this->e->connection() ) {
			$this->setError( $this->e->getError() );
			return false;
		}
		$this->setMessage( $this->e->getMessage() );
		return true;

	}

/* 					SETS FUNCTIONS 						*/
/* -----------------------------------------------------*/

	private function setPrefix($Prefix ='')
	{
		$this->Prefix = $Prefix;
		return true;
	}

	public function getPrefix()
	{
		return $this->Prefix;
	}

	public function setDriver( $DriverName ='' )
	{
		if ( empty($DriverName) ) {
			$this->setError('Database Driver Name Error');
			return false;
		}

		$this->Driver = $DriverName;
		return true;
	}

	public function setError( $ErrorValue ='' )
	{
		$this->Errors[] = $ErrorValue;
		return true;
	}

	public function setMessage($Message ='')
	{
		$this->Messages[] = $Message;
		return true;
	}

/* 					GETS FUNCTIONS 						*/
/* -----------------------------------------------------*/

	public function getDriver()
	{
		return $this->Driver;
	}

	public function getError()
	{
		return $this->Errors;
	}

	public function getMessage()
	{
		return $this->Messages;
	}

	public function getSqlConnect()
	{
		return $this->e->SqlConnect;
	}

	public function getMulti()
	{
		return $this->multi;
	}

/* 					OTHER FUNCTIONS 						*/
/* ---------------------------------------------------------*/

	public function multi( $multi='' )
	{
		$this->multi = $multi;
	}

	public function table( $TableName ='' )
	{
		if ( empty($TableName) ) {
			$this->setError('Tablo Adını girmelisiniz.');
			return false;
		}

		$tName = $TableName;

		if ( is_array($TableName) ) {
			$tName = '';
			foreach ($TableName as $key => $value) {
				$tName .= $value . ' , ';
			}
			$tName = substr($tName, 0 , strlen($tName)-2);
		}

		$this->TableName = $tName;

	}

	public function getTable()
	{
		return $this->TableName;

	}

	/**
	 * data
	 * Eklenecek veya Değiştirilecek Bilgiler
	 * Key ve Data
	 *
	 * @param array $data
	 */
	public function data( $keys = '' , $datas = '' )
	{
		if ( ! empty($keys) and ! empty($datas) ) {
			$this->Keys  = $keys;
			$this->Datas = $datas;
		}
	}

	public function _data()
	{
		$kontrol = true;
		$keys    = '';
		if ( is_array($this->Keys ) ) {
			foreach ($this->Keys as $k => $v) {
				$keys .= $v.',';
			}
			$keys = substr($keys, 0 , strlen($keys)-1);
			$keys = '('.$keys.')';
		} else {
			$keys  = '('.$this->Keys.')';
		}
		$datas  = '';
		if ( is_array($this->Datas ) ) {
			foreach ($this->Datas as $k => $v) {
				if (is_array($v)) {
					$kontrol = false;
					$data2   = '';
					foreach ($v as $k2 => $v2) {
						$data2 .=  '"' .$v2.'",';
					}
					$data2 = substr($data2, 0 , strlen($data2)-1);
					$datas .= '('.$data2.'),';
				} else {
					$datas .= '"' . $v.'",';
				}
			}
			$datas = substr($datas, 0 , strlen($datas)-1);
			if ($kontrol == true) {
				$datas = '('.$datas.')';
			}
		} else {
			$datas = '('.$this->Datas.')';
		}
		return $keys . ' VALUES '. $datas;
	}

	public function _dataUpdate()
	{
		$kontrol = true;
		$keys    = '';
		if ( is_array($this->Keys ) ) {
			foreach ($this->Keys as $k => $v) {
				$keys .= $v.'="'.$this->Datas[$k].'" ,';
			}
			$keys = substr($keys, 0 , strlen($keys)-1);
			// $keys = '('.$keys.')';
		} else {
			$keys  = ''.$this->Keys.'="'. $this->Datas. '"';
		}
		return $keys;
	}

	private function _dataMultiUpdate( $case )
	{
		$sql = '';
		foreach ($this->Keys as $k => $v) {

			$sql .= $v . ' = CASE ' . $case;

			foreach ($this->Datas[$v] as $key => $value) {
		        $sql .= ' WHEN ' . $key . " THEN '" . $value . "'";
			}
			$sql .= 'ELSE '. $v .' END,';
		}

		$sql = substr($sql, 0 , strlen($sql)-1);

		return $sql;
	}


	private function setSqlKeys()
	{
		$dataArray = $this->getKeys();
		$sqlKeys = '(';
		foreach ($dataArray as  $value) {
			$sqlKeys .= $value . ',';
		}
		$sqlKeys = substr($sqlKeys, 0 , strlen($sqlKeys)-1);
		$sqlKeys .= ')';
		$this->sql_keys = $sqlKeys;
	}

	public function setSql( $sql )
	{
		$this->SqlQuery = $sql;
	}

	public function setSqlQuery( $Query ='' )
	{
		$where = '';
		if ($this->SqlQueryWhere!='') {
			$where = ' WHERE ';
			$this->SqlQueryWhere = $this->SqlQueryWhere;
		}
		$this->SqlQuery .= 	$Query.
							$this->_innerjoin(). 
							$where . 
							$this->SqlQueryWhere;
		$this->SqlQuery .= ';'; // Multi Query
		return $this->SqlQuery;
	}

	public function other($value='')
	{
		$this->SqlQueryWhere .= $value ;
	}
	public function getSqlQuery()
	{
		return $this->SqlQuery;
	}

	public function setExecutingTime( $time ='' )
	{
		$this->executingTime = $time ;
	}
	public function executingTime( $time ='' )
	{
		return $this->executingTime;
	}

	public function lastQuery()
	{
		return $this->PDO->queryString;
	}

	public function setLastInsertId($lastInsertId='')
	{
		$this->lastInsertId = $lastInsertId;
	}
	public function lastInsertId()
	{

		return $this->lastInsertId;
	}

	public function CountRows()
	{
		return $this->PDO->rowCount();
	}
	public function rollBack()
	{
		$this->getSqlConnect()->rollBack();
	}

	public function Execute( $sql = '')
	{
 		$this->getSqlConnect()->beginTransaction();
		$this->getSqlConnect()->setAttribute( PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION );
		$this->getSqlConnect()->setAttribute( PDO::ATTR_AUTOCOMMIT, FALSE );

		$start = microtime(true);
		
		if ( !empty($sql) ) {
			$this->setSql($sql);
		}
		$this->PDO = $this->getSqlConnect()->prepare( $this->getSqlQuery() );
		$this->bindparam();
		try {

			$this->PDO->execute();
			if ( $this->getSqlConnect()->lastInsertId() != NULL ) {
				$this->setLastInsertId( $this->getSqlConnect()->lastInsertId() );
			}

			$this->getSqlConnect()->commit();
			
	  		$time  = microtime(true) - $start;
	  		$this->setExecutingTime(number_format($time, 4));
	  		
			global $htmlLog;
			$htmlLog->Set_Db( $this->getSqlQuery() . '
Execute Time : ' .$this->executingTime() );

		} catch (PDOException $e) {
				$this->rollBack();
				$this->setError('Sql Error : ' . $e->getMessage() );
				
		}
		
  		$this->Clear();
	}


	public function bindparam()
	{
		if ( empty($this->SqlQueryWhere) ) {
			return;
		}

		if ( is_array( $this->values ) ) {

			foreach ($this->values as $k => &$v) {
				$this->PDO->bindParam(($k+1), $v, $this->getPDOConstantType( $v ));
			}

			return;
		}
		$this->PDO->bindParam(1, $this->values, $this->getPDOConstantType( $this->values ));
	}


// ------------------------------------------------------------------------------------------------
// 			    SELECT , INSERT | MULTIINSERT , REPLACE , UPDATE | MULTIUPDATE , DELETE
// ------------------------------------------------------------------------------------------------

	public function MultiExecuteControl()
	{
		if ( $this->getMulti() === false ) {
			$this->Execute();
			$this->Clear();
		}
	}

	public function select( $Select = '*' )
	{
		$SqlQuery = 'SELECT '.$Select.' FROM '. $this->getTable();
		$this->setSqlQuery($SqlQuery);
		$this->MultiExecuteControl();
	}

	/**
	 * insert
	 * Insert and Multi insert
	 * @return
	 */
	public function insert()
	{
		$SqlQuery = 'INSERT INTO '. $this->getTable() .' '. $this->_data();
		$this->setSqlQuery($SqlQuery);
		$this->Execute();
		$this->Clear();
		// $this->MultiExecuteControl();
	}

	/**
	 * update
	 * @return
	 */
	public function update()
	{
		$SqlQuery = 'UPDATE '. $this->getTable() . ' SET '. $this->_dataUpdate();
		$this->setSqlQuery( $SqlQuery );
		$this->MultiExecuteControl();
	}

	public function multiUpdate($case)
	{
		$SqlQuery = 'UPDATE '. $this->getTable() . ' SET '. $this->_dataMultiUpdate($case);
		return $SqlQuery;
		$this->setSqlQuery( $SqlQuery );
		$this->MultiExecuteControl();
	}

	public function delete()
	{
		$SqlQuery = 'DELETE FROM '. $this->getTable();
		$this->setSqlQuery( $SqlQuery );
		$this->MultiExecuteControl();
	}


	public function replace()
	{
		$SqlQuery = 'REPLACE INTO '. $this->getTable() .' '. $this->_data();
		$this->setSqlQuery( $SqlQuery );
		$this->MultiExecuteControl();
	}

// --------------------------------------------------------------------
// 								JOIN
// --------------------------------------------------------------------

	public function _innerjoin()
	{
		return $this->join;
	}

	public function join($tableName , $value , $action = 'INNER')
	{
		$this->join = $this->join.' '.$action.' JOIN ' .$tableName . ' ON ' . $value;
	}


// --------------------------------------------------------------------
// 								ORDER BY
// --------------------------------------------------------------------

	public function orderby( $id='', $action = 'ASC' )
	{
		$this->SqlQueryWhere .= ' ORDER BY ' . $id . ' ' . strtoupper($action);
	}


// --------------------------------------------------------------------
// 								LIMIT
// --------------------------------------------------------------------

	public function limit( $start = 0 , $stop = '' )
	{
		if ($stop == '') {
			$this->SqlQueryWhere .= ' LIMIT ' . $start ;
		} else {
			$this->SqlQueryWhere .= ' LIMIT ' . $start .' , ' . $stop;
		}
	}


// --------------------------------------------------------------------
// 								GROUP BY
// --------------------------------------------------------------------


	public function groupby( $id='' )
	{
		$this->SqlQueryWhere .= ' GROUP BY ' . $id ;
	}

// --------------------------------------------------------------------
// 								BETWEEN
// --------------------------------------------------------------------
//
	public function between( $field ='' , $first , $second , $action = ' AND ' )
	{
		$first         = (is_int($first)) ? $first  : "'".$first."'";
		$second        = (is_int($second)) ? $second  : "'".$second."'";
		$where =  strtoupper($action).' ( '. $field . ' BETWEEN '. $first .' AND ' . $second .' ) ';
		$this->whereControl($where);
	}

	public function notBetween( $field ='' , $first , $second , $action = '' )
	{
		$first         = ( is_int($first) ) ? $first  : "'".$first."'";
		$second        = ( is_int($second) ) ? $second  : "'".$second."'";
		$where = strtoupper($action).' ('. $field . ' NOT BETWEEN '. $first .' AND ' . $second .' ) ';
		$this->whereControl($where);
	}

// --------------------------------------------------------------------
// 								LIKE
// --------------------------------------------------------------------
//
	
	public function like( $field ='' , $like ='' , $action = ' AND ' )
	{
		$where =  strtoupper($action). $field . ' LIKE "%'. $like .'%" ';
		$this->whereControl($where);					
	}

	public function notLike( $field ='' , $like  , $action = ' AND ' )
	{
		$where =  strtoupper($action). $field . ' NOT LIKE "%'. $like .'%" ';
		$this->whereControl($where);			
	}

 	public function beforeLike( $field ='' , $like  , $action = ' AND ' )
	{
		$where = strtoupper($action). $field . ' LIKE "%'. $like .'" ';
		$this->whereControl($where);
	}

 	public function afterLike( $field ='' , $like  , $action = ' AND ' )
	{
		$where =  strtoupper($action). $field . ' LIKE "'. $like .'%" ';
		$this->whereControl($where);
	}

	public function notBeforeLike( $field ='' , $like  , $action = ' AND ' )
	{
		$where =  strtoupper($action). $field . ' NOT LIKE "%'. $like .'" ';
		$this->whereControl($where);
	}

 	public function notAfterLike( $field ='' , $like  , $action = ' AND ' )
	{
		$where = strtoupper($action). $field . ' NOT LIKE "'. $like .'%" ';
		$this->whereControl($where);
	}

// --------------------------------------------------------------------
// 								HAVING
// --------------------------------------------------------------------

	public function having( $field ='' , $having ='' ,$sign=' = ')
	{
		$this->SqlQueryWhere .= ' HAVING '. $field . ' ' .$sign. ''. $having ;
	}

// --------------------------------------------------------------------
// 								WHERE
// --------------------------------------------------------------------
	/**
	 * setbindparam 
	 * Gelen değerlerin hepsini sırası ile bir dizide topluyoruz.
	 * @param  string OR array $value [description]
	 * @return [type]        [description]
	 */
	public function setbindparam($value='')
	{
		if (!is_array($value)) {
			$this->values[] = $value;	
		} else {

			$this->values   = array_merge($this->values,$value);
		}
	}

	public function _where($key , $operator=' AND ' , $sign=' = ')
	{

		if ( is_array($key) ) {
			$where = '';
			foreach ($key as $k => $v) {
				$where .= $operator.$v . ' '.$sign.' ? ';
			}
		} else {
			$where = $operator.$key . ' '.$sign.' ? ';
		}
		$this->whereControl($where);
		
	}


	public function where($key , $value , $sign = '=' )
	{ 
		$this->setbindparam($value);
		$this->_where($key , ' AND ' , $sign);

	}

	public function orWhere($key , $value , $sign = '=' )
	{
		$this->setbindparam($value);
		$this->_where($key , ' OR ' , $sign);
	}

	public function whereIn( $field = '', $in = array(), $action = 'AND' )
    {
    	if (!empty($field ) and !empty($in)) {
    		$where = $field . ' IN ';
    		$this->setbindparam( $in );
    		if ( is_array($in) ) {
    			$whereIn = '';
    			foreach ($in as $key => $value) {
    				$whereIn .= ' ? ,';
    			}
    			$whereIn = substr($whereIn ,0 , strlen($whereIn) - 2);
    			$where .= '('.$whereIn .')';
    		} else {

    			$where .='( ? )';
    		}
    		$where = strtoupper($action) . ' ('. $where .') ';
    		$this->whereControl($where);
    	}
    }
    public function whereNotIn($field = '', $in = array(), $action = '')
    {
    	if ( ! empty($field ) and  ! empty($in)) {
    		$where = $field . ' NOT IN ';
    		$this->setbindparam($in);
    		if (is_array($in)) {
    			$whereIn = '';
    			foreach ($in as $key => $value) {
    				$whereIn .= ' ? ,';
    			}
    			$whereIn = substr($whereIn ,0 , strlen($whereIn) - 2);
    			$where .= '('.$whereIn .')';
    		} else {

    			$where .='( ? )';
    		}
    		$where = strtoupper($action) . ' ('. $where .') ';
    		$this->whereControl($where);
    	}
    }

// --------------------------------------------------------------------
// 					       WHERE CONTROL
// --------------------------------------------------------------------

    /**
     * whereControl 
     * Sorguya girecek kriterlerin önündekli ilk and or gibi şeylerin ilkini kaldırıyor. 
     * @param  string $where [description]
     * @return [type]        [description]
     */
    public function whereControl( $where = '')
    {
    	if ( $this->whereControl == 0 ) {
    		$where = trim($where);
    		$bosluk_konum = strpos($where, ' ');
    		$where = substr(	$where ,  $bosluk_konum , strlen($where)) ; 
    		$this->SqlQueryWhere .= $where;
    		$this->whereControl = 1;
    	} else {
    		$this->SqlQueryWhere .= $where;
    	}
    }


// --------------------------------------------------------------------
// 					 PDO ESCAPE STRING VS.
// --------------------------------------------------------------------

	public function getPDOConstantType( $var )
	{

	  if( is_int ( $var ) ) return PDO::PARAM_INT;

	  if( is_bool( $var ) ) return PDO::PARAM_BOOL;

	  if( is_null( $var ) ) return PDO::PARAM_NULL;

	  return PDO::PARAM_STR;
	}

	public function escapeStr( $str, $like = false, $side = 'both' )
    {
        if ( is_array($str) ) {
            foreach ($str as $key => $val) {
                $str[$key] = $this->escapeStr($val, $like);
            }
            return $str;
        }

        // escape LIKE condition wildcards
        if ($like === true) {
            $str = str_replace(array('%', '_'), array('\\%', '\\_'), $str);

            switch ($side) {
                case 'before':
                    $str = "%{$str}";
                    break;

                case 'after':
                    $str = "{$str}%";
                    break;

                default:
                    $str = "%{$str}%";
            }
        }

        // make sure is it bind value, if not ...
        if ($this->prepare === true) {
            if (strpos($str, ':') === false) {
                $str = $this->quote($str, PDO::PARAM_STR);
            }
        } else {
            $str = $this->quote($str, PDO::PARAM_STR);
        }

        return $str;
    }

    public function quote( $str, $type = NULL )
    {
        return $this->PDO->quote($str, $type);
    }

	public function escape($str)
    {

    	if( is_int 		( $str ) ) 	return (int) $str;
    	if( is_double 	( $str ) ) 	return (double) $str;
    	if( is_float 	( $str ) ) 	return (float) $str;
    	if( is_bool 	( $str ) )  return ($str === false) ? 0 : 1;
    	if( is_null 	( $str ) )  return ($str === false) ? 0 : 1;

    	return $this->escapeStr($str);
    }

// --------------------------------------------------------------------
// 								RESULT
// --------------------------------------------------------------------
	public function result()
	{
		return $this->getResultArray();
	}

    public function getResultArray()
    {
        return $this->PDO->fetchAll(PDO::FETCH_ASSOC);
    }

    public function getResult()
    {
        return $this->PDO->fetchAll(PDO::FETCH_OBJ);
    }

    public function getRow()
    {
        return $this->PDO->fetch(PDO::FETCH_OBJ);
    }

    public function getRowArray()
    {
        return $this->PDO->fetch(PDO::FETCH_ASSOC);
    }

// --------------------------------------------------------------------
// 								CLEAR
// --------------------------------------------------------------------
	public function Clear()
	{

		$this->orderby       = '';
		$this->limit         = '';
		$this->groupby       = '';
		$this->join          = '';
		$this->between       = '';
		$this->having        = '';
		$this->DriverPath    = 'Drivers';
		$this->Errors        = '';
		$this->Message       = '';
		$this->TableName     = '';
		$this->operator      = ' AND ';
		$this->where         = '';
		$this->keys          = '';
		$this->walues        = '';
		$this->whereIn       = '';
		$this->like          = '';
		$this->multi         = false;
		$this->SqlQuery      = '';
		$this->SqlQueryWhere = '';
		$this->values        = array();
		$this->whereControl  = 0 ;

	}
}