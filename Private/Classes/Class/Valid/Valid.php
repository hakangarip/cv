<?php 
/**
* 
*/
class Valid
{
	
	function __construct()
	{
		include_class( get_class() );
	}
	
	public function email($email='')
	{
		
		if(filter_var($email, FILTER_VALIDATE_EMAIL)){ 
		   return TRUE;
		}else{ 
		    return FALSE;
		} 

	}
}
