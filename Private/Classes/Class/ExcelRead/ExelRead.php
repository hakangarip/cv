<?php 

/**
 * ExelRead
 * Using
 * 
 * $exelRead   = new ExelRead();
 * $exelRead->setFileName('Example_Exel/asd.xlsx');
 * if ( !$exelRead->execute() ) {
 * 	echo $exelRead->getError();
 * 	exit();
 * }
 * 
 * $exelRead->setStart('A_3');
 * $exelRead->setStop('D_5');
 * var_dump($exelRead->read());
 * 
 * @author    Hakan Garip <i@hakangarip.com.tr>
 * 
 */

class ExelRead
{

// ------------------------------  Class $this  ------------------------------

	public $fileName  = '';
	public $error     = '';
	public $data      = array();
	public $start     = '';
	public $stop      = '';
	public $sheetName = '';

//------------------------------------------------------------------
 
	function __construct()
	{
		include 'PHPExcel/IOFactory.php';
	}


/* ------------------------------  SETS ------------------------------	*/ 

	/**
	 * setFileName
	 * @param  string $fileName [description]
	 * @return [type]           [description]
	 */
	public function setFileName($fileName='')
	{
		$this->fileName = $fileName;
	}

	/**
	 * setError
	 * @param string $Error [description]
	 */
	public function setError($Error='')
	{
		$this->error = $Error;
	}

	/**
	 * setData description]
	 * @param string $data [description]
	 */
	public function setData($data='')
	{
		$this->data = $data;
	}

	/**
	 * setStart
	 * @param string $start [description]
	 */
	public function setStart($start='')
	{
		$this->start = $start;
	}

	/**
	 * setStop
	 * @param string $stop [description]
	 */
	public function setStop($stop='')
	{
		$this->stop = $stop;
	}


	public function setSheetName($name)
	{
		$this->sheetName = $name;
	}


/* ------------------------------  GETS ------------------------------	*/ 

	/**
	 * getFileName
	 * 
	 */
	public function getFileName($fileName='')
	{
		return $this->fileName;
	}

	/**
	 * getError
	 * 
	 */
	public function getError()
	{
		return $this->error;
	}

	/**
	 * getData
	 * 
	 */
	public function getData()
	{
		return $this->data;
	}

	/**
	 * getStart
	 * 
	 */
	public function getStart($start='')
	{
		return $this->start;
	}

	/**
	 * getStop
	 * 
	 */
	public function getStop()
	{
		return $this->stop;
	}
	
	public function getSheetName()
	{
		return $this->sheetName;
	}


/* ------------------------------  OTHJER FUNCTIONS  ------------------------------	*/ 
	
	public function execute()
	{
		if (empty($this->getFileName())) {
			$this->setError('Okunacak dosya adı belirtilmemiş.');
			return false;
		}

		if ( !empty( $this->getSheetName() ) ) {
			$objPHPExcel = PHPExcel_IOFactory::createReader($this->getFileName()); 
			$objPHPExcel->setLoadSheetsOnly($this->getSheetName()); 
			$sheetData = $objPHPExcel->load($this->getFileName()); 
		} else {
			$objPHPExcel = PHPExcel_IOFactory::load($this->getFileName());
			$sheetData   = $objPHPExcel->getActiveSheet()->toArray(null,true,true,true);
		}
		$this->setData($sheetData);
		$this->sheetName = '';
		return true;
	}

	public function read()
	{
		$startControl = $stopControl = 0;

		if (!empty($this->getStop())) {
			$stopControl = 1 ;
			$stopExplode = explode('_', $this->getStop());
		}

		if (!empty($this->getStart())) {
			$startExplode = explode('_', $this->getStart());	
			$startControl = 1 ;
		}

		// Başlangıç ve bitiş belirtilmişmi diye kodntol ediliyor. 
		// 
		if ($startControl == 0 and $stopControl == 0) {
			return $this->AllRead();

		} elseif ($startControl == 1 and $stopControl == 0) {
			return $this->StartRead($startExplode);

		} elseif ($startControl == 0 and $stopControl == 1) {
			return $this->StopRead($stopExplode);

		} else {
			return $this->StartStopRead($startExplode, $stopExplode);
		}

	}

	private function AllRead()
	{
		return $this->getData();
	}

	private function StartRead($startExplode)
	{
		
		if (count($startExplode) != 2) {
			return false;
		}
		$returnArray = array();
		$ilkOkumaKontrol = 0;

		foreach ($this->getData() as $satir_k => $satir_v) {
			if ($satir_k >= (int)$startExplode[1]) {
				foreach ($satir_v as $sutun_k => $sutun_v) {
					
					if ( $sutun_k == strtoupper($startExplode[0]) ) {
						$ilkOkumaKontrol = 1;
					}
					if ($ilkOkumaKontrol == 1) {
						$returnArray[$satir_k][$sutun_k] =$sutun_v; 
					}

				}
			}
		}

		return $returnArray;
	}


	private function StopRead($stopExplode)
	{
		
		if (count($stopExplode) != 2) {
			return false;
		}
		$returnArray = array();
		$ilkOkumaKontrol = 1;
		
		foreach ($this->getData() as $satir_k => $satir_v) {
			if ($satir_k <= (int)$stopExplode[1]) {
				foreach ($satir_v as $sutun_k => $sutun_v) {

					if ($sutun_k == strtoupper($stopExplode[0]) and $satir_k == (int)$stopExplode[1] ) {
						$ilkOkumaKontrol = 0;
					}
					if ($ilkOkumaKontrol == 1) {
						$returnArray[$satir_k][$sutun_k] =$sutun_v; 
					}

				}
			}
		}

		return $returnArray;
	}

	private function StartStopRead($startExplode, $stopExplode)
	{
		if (count($startExplode) != 2) {
			return false;
		}
		
		if (count($stopExplode) != 2) {
			return false;
		}

		$returnArray = array();
		$sonOkumaKontrol = $ilkOkumaKontrol = 0;
		
		foreach ($this->getData() as $satir_k => $satir_v) {
			
			if ( $satir_k >= (int)$startExplode[1] and $satir_k <= (int)$stopExplode[1] ) {

				foreach ($satir_v as $sutun_k => $sutun_v) {

					if ( $sutun_k == strtoupper($startExplode[0]) ) {
						$ilkOkumaKontrol = 1;
					}
					if ( $ilkOkumaKontrol == 1 and $sonOkumaKontrol == 0 ) {
						$returnArray[$satir_k][$sutun_k] =$sutun_v; 
					}
					if ( $sutun_k == strtoupper($stopExplode[0]) and $satir_k == (int)$stopExplode[1] ) {
						$sonOkumaKontrol = 1;
					}

				}
			}
		}

		return $returnArray;
	}


}


 ?>