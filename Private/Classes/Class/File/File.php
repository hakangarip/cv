<?php 

/**
*
* 
*/
class File
{
	public  $path;

	function __construct()
	{
		include_class( get_class() );
		$this->setPath();
	}

	public function setPath($path='')
	{

		if (empty($path)) {
			$this->path = DR . SLASH . Config::UploadFolder();
			return;
		}
		$this->path =  $path;
		// echo $this->path;
		if(!file_exists($this->path)){
			return false;
		}
	}

	public function FolderList()
	{
		$arrayDosya = array('.','..');
		$return = array();
		$dizin = $this->path;
		if (is_dir($dizin)) {
			// prp($dizin);
		    if ($dit = opendir($dizin)) {
		        while (($dosya = readdir($dit)) !== false) {
		        	if ( !in_array( $dosya , $arrayDosya ) ) {
		        		$return[] = array(
		        					'FileName' => $dosya,
		        					'FileType' =>(filetype($dizin . $dosya) === "dir") ? "Folder" : "File"
		        			);
			          
		        	}
		        }
		        closedir($dit);
		    }
		}
		return $return;
	}

	public function read($FileName='')
	{
		global $htmlLog;
		$FileName = $this->path.SLASH.$FileName;
		
		if ( ! file_exists($FileName) ) {
			return false;
		}
		$dt   = fopen($FileName , "r");
		$read = fread($dt, filesize($FileName));
		fclose($dt);

		$logClass = str_replace( '[*title*]', 'Okunan Dosya' , Config::HtmlLog('fileread') );
		$logClass = str_replace( '[*Name*]', $FileName  , $logClass );
		$htmlLog->Log( $logClass );

		return $read;
	}

	public function write($FileName = '' , $File , $attr = 'new' )
	{
		if ( empty($FileName) ) {
			return False;
		}

		$FileName = $this->path.SLASH.$FileName;
		$this->mkdir();
		switch ($attr) {
			
			case 'new':
					$dt = fopen($FileName, 'w');
					fwrite($dt, $File);
					fclose($dt);
				break;

			case 'add':
					$read = '';
					if (is_file($FileName)) {
						
						// $dt   = fopen($FileName , "r");
						// $read = fread($dt, filesize($FileName));
						// fclose($dt);
					}

					$File .= $read;
					$dt    = fopen($FileName, 'a');
					fwrite($dt, $File);
					fclose($dt);
				break;
			
		}
		
	}

	public function upload( $Upload_File_Name, $New_File_Name )
	{
		$this->mkdir();
		if(is_uploaded_file($_FILES[$Upload_File_Name]['tmp_name']))
        {
            if(move_uploaded_file($_FILES[$Upload_File_Name]['tmp_name'],$New_File_Name)) {
            	chmod($New_File_Name,0644);
               	return true;
            }
        }
        exit('Dosya Upload Edilemiyor.');
        return false;
	}

	public function mkdir( $path = '')
	{
		if ( empty($path) ) {
			$path = $this->path;
		}
		if(!file_exists($path)){
			if( mkdir($path, 0777) ) {
				
			} else {
				exit('Dosya Oluşturulamıyor.');
			}
		}
		// prpe($path);
	}

	public function getType( $file )
	{

		$mime_types = array(

			'txt'  => 'text/plain',
			'htm'  => 'text/html',
			'html' => 'text/html',
			'php'  => 'text/html',
			'css'  => 'text/css',
			'js'   => 'application/javascript',
			'json' => 'application/json',
			'xml'  => 'application/xml',
			'swf'  => 'application/x-shockwave-flash',
			'flv'  => 'video/x-flv',
			
			// images
			'png'  => 'image/png',
			'jpeg' => 'image/jpeg',
			'gif'  => 'image/gif',
			'bmp'  => 'image/bmp',
			'ico'  => 'image/vnd.microsoft.icon',
			'tiff' => 'image/tiff',
			'svg'  => 'image/svg+xml',
			
			// archives
			'zip'  => 'application/zip',
			'rar'  => 'application/x-rar-compressed',
			'exe'  => 'application/x-msdownload',
			'msi'  => 'application/x-msdownload',
			'cab'  => 'application/vnd.ms-cab-compressed',
			
			// audio/video
			'mp3'  => 'audio/mpeg',
			'qt'   => 'video/quicktime',
			'mov'  => 'video/quicktime',
			
			// adobe
			'pdf'  => 'application/pdf',
			'psd'  => 'image/vnd.adobe.photoshop',
			'ai'   => 'application/postscript',
			'eps'  => 'application/postscript',
			'ps'   => 'application/postscript',
			// 'jpeg'     => 'image/x-ms-bmp',
			// ms office
			'doc'  => 'application/msword',
			'rtf'  => 'application/rtf',
			'xls'  => 'application/vnd.ms-excel',
			'ppt'  => 'application/vnd.ms-powerpoint',
			
			// open office
			'odt'  => 'application/vnd.oasis.opendocument.text',
			'ods'  => 'application/vnd.oasis.opendocument.spreadsheet',
		);
		// $file_type  = mime_content_type($file);
		// $file_type  = strtolower($file_type);
			// echo 'fi:'.$file_type;
		$file_type  = ($file);
		// $mime_types = array_flip($mime_types);
		// prpe($mime_types);
			// prpe($file_type);
		// echo 'f:'.$file;
		if ($file_type == 'image/x-ms-bmp') {
			return 'jpeg';
		}
		if ( array_search($file_type, $mime_types) ) {
            $return = array_search($file_type, $mime_types);
            // echo 'a'.$return;
            return $return;
        }
        return false;
	}


	public function ReName($oldName='' , $newName = '')
	{
		if ( rename($oldName , $newName) ){
			return true;
		}
		return false;
	}
}