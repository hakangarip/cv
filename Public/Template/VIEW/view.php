<!doctype html>
<html>
  <head>
  <meta charset="utf-8">
  <?php 
    $SEO_ARRAY['BASLIK']   = isset($BASLIK)   ? $BASLIK   : '';
    $SEO_ARRAY['KEYWORDS'] = isset($KEYWORDS) ? $KEYWORDS : '';
?>
  <script>
    var sitepath = '<?php echo Config::SitePath() ?>';
  </script>
</head>
<body class="boxed fixed-header hidden-top cm-shop-7">
  <div class="page-box">
    <div class="page-box-content">
      <?php 
        Pager_Site::get($PagerSite);
      ?>
    </div><!-- .page-box-content -->
  </div><!-- .page-box -->
</body>
</html>